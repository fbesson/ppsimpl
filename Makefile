CAMLOPTC ?= ocamlopt -w @Aer-44 -rectypes -annot -c
HASNATDYNLINK = yes
ifdef COMPCERT
INCL=\"-R\" \"$(COMPCERT)/lib\"  \"-as\" \"compcert.lib\" 
COQINCL=-R $(COMPCERT)/lib  compcert.lib
endif

export CAMLOPTC
export HASNATDYNLINK
export COQINCL


all : coq-config.el
	cd src ; make

.PHONY : clean install uninstall

uninstall :
	rm -rf  `$(COQC) -where`/user-contrib/PP


install :
	cd src ; make install

test : 
	$(COQC) examples/PpsimplEx.v

clean : 
	cd src ; make clean


-include src/Makefile.config

coq-config.el : src/Makefile.config
	@echo $(COMPCERT)
	@echo $(INCL)
	@echo \(setq coq-prog-name \"$(COQBIN)/coqtop\"\) > coq-config.el
	@echo \(setq coq-prog-args \(quote \(\"-emacs-U\" \"-R\" \"\.\" \"PP\" $(INCL) \)\)\) >> coq-config.el
	@echo \(setq coq-dependency-analyzer \"$(COQBIN)/coqdep\"\) >> coq-config.el
	@echo \(setq coq-compile-before-require nil\) >> coq-config.el
	@echo \(setq proof-three-window-enable nil\) >> coq-config.el
	@cat coq-config.el
