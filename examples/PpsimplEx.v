Require Import ZArith.
Require Import Psatz.
Require Import PP.PpsimplZ.
Unset Ltac Debug.

Definition pos := positive.


Goal (forall (x:pos) (f : pos -> pos), (x = f x)%positive -> f x = xH -> x = xH)%positive.
Proof.
  intros.
  unfold pos in *.
  ppsimpl.
  lia.
Time Qed.

Goal (forall (x:pos) (P:Prop) (f : pos -> pos), P -> (x = f x)%positive -> f x = xH -> x = xH)%positive.
Proof.
  intros.
  unfold pos in *.
  ppsimpl.
  lia.
Time Qed.


Goal forall x:Z, (1 + x = Z.succ x)%Z.
Proof.
  intro.
  ppsimpl.
  lia.
Time Qed.


Goal forall x y : nat, 0 <= x + y +y.
 intros x y.
 ppsimpl.
 lia.
Time Qed.

Goal forall x y : nat, y <= x -> y -x  = 0. 
Proof.
   intros x y.
   Time ppsimpl.
   lia.
Time Qed.

Require Import Bool.

Goal forall x y : bool, x && y = true -> y = true.
Proof.
   intros x y.
   ppsimpl.
   tauto.
Time Qed.



Goal forall x y: Z, (y < x -> Zmax x y = x)%Z.
Proof.
  intros.
  ppsimpl.
  lia.
Time Qed.

Goal forall x:Z, (0 < x -> Zdiv x  x = 1)%Z.
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.

Goal forall x:Z, (0 < x -> Zdiv x  1 = x)%Z.
Proof.
  intros.
  ppsimpl.
  lia.
Time Qed.

Goal forall x:nat, (x > 0 -> NPeano.sqrt (x * x) = x).
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.

Goal forall x:nat, (x > 1 -> NPeano.sqrt x < x).
Proof.
  intros.
  Time ppsimpl.
  nia.
Time Qed.

Goal forall x:nat, (x > 0 -> NPeano.sqrt x > x -> x = 0).
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.

Goal forall x:Z, (x + 1 = Z.succ x)%Z.
Proof.
  intro.
  ppsimpl.
  reflexivity.
Time Qed.

Goal forall x:Z, Zcompare x x = Eq.
Proof.
  intro.
  ppsimpl.
  intuition lia.
Time Qed.

Goal forall x:comparison,  x = Eq \/ x = Lt \/ x= Gt.
Proof.
  intro.
  ppsimpl.
  intuition.
Time Qed.

Require Import Psatz ZArith.

Open Scope Z_scope.

Goal forall a b c,
0 < a -> Zabs (2 * a * b - 2 * c) <= a -> forall b', Zabs (a * b - c) <= Zabs (a * b' - c).
Proof.
intros a b c _ H b'.
assert (H1:  b = b' \/ b <> b') by lia.

nia.
Qed.

Goal forall a, a <= Zpos (Z.to_pos a).
Proof.
  intros. ppsimpl. lia.
Qed.
