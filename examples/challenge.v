Require Import Coqlib Integers.
Require Import PP.PpsimplCompcert.
Require Import Psatz.

Ltac dand :=
  repeat 
  match goal with
    |H : _ /\ _ |- _ => destruct H
  end.

Ltac only_conj :=
  repeat 
  match goal with
    | H : context[?A\/ ?B] |- _ => clear H
    | H : context[?A ->  ?B] |- _ => clear H
  end.

Ltac intui :=
  repeat 
  match goal with
    | H : _  /\ _ |- _ => destruct H ; subst
    | H : _ \/ _ |- _ => destruct H
  end.

Ltac no_dup :=
repeat 
  match goal with 
    | H : ?A , H' : ?B |- _ => 
      match constr:(A=B) with
        | ?A = ?A => clear H
      end
  end.



Ltac no_impl :=
  match goal with 
    | H : ?A -> ?B |- _ => 
      let f := fresh in 
      assert (f : A) by (only_conj ; lia) ;
        apply H in f ; clear H
  end.

Ltac no_impls :=
  match goal with 
    | H : ?A -> ?B |- _ => 
      let f := fresh in 
      assert (f : A) by lia ;
        apply H in f ; clear H
  end.


Ltac hm :=
  let hm := (eval compute in Int.half_modulus) in 
  let HM := fresh in 
  assert (HM : hm = Int.half_modulus) by reflexivity.

Ltac clean :=
  repeat match goal with 
      H : ?A |- _ => 
      let x := fresh in
      assert (x:A) by (clear H ; only_conj ; lia) ; clear H x
  end.



Lemma rem_correct lo  low  x y :
  lo <= Int.unsigned x < Int.half_modulus ->
  0 < Int.unsigned y < Int.half_modulus -> 
  low <= Int.signed (Int.mods x y).
Proof.
  intros [LO HI].
  ppsimpl.
  intros.
  dand ; subst.
  repeat no_impl.
  dand ; subst.
  hm.
  repeat no_impls.
  dand ; subst.
  destruct H16 ; [| lia].
  dand.
  destruct H17 ; [| lia].
  dand.
  destruct H8 ; [| lia].
  subst.
  clean.
  destruct H9.
  dand. subst.
  assert (e8 = 0) by lia.
  subst.
  ring_simplify in H12.
  intuition.
  ring_simplify in H9.
  assert (e9 = Int.unsigned x - Int.unsigned y * e5) by lia.
  subst.
  clean.
  ring_simplify in H11.
  ring_simplify in H9.
  replace (Z.abs (Int.unsigned y)) with (Int.unsigned y) in * by lia.
  

  dand. subst.
  no_dup.
  destruct H28 ; [| lia].
  dand. subst.
  no_dup.
  destruct H10 ; dand ; subst.
  destruct H11 ;[| lia].
  destruct H9 ;[| lia].
  


  assert (e22 < Int.half_modulus) by lia.

  assert (e22 =  Int.unsigned x - e13 * e18 - 4294967296 * e21).
  lia.
  subst.
  clean.
  
  destruct H11 ; [|lia].
  

  assert (e0 <> 0) by lia.
  apply H29 in H7.


  intui.
  split.
  no_dup.
  assert (e5 = e18) by nia.
  subst.
  assert (e21 = e8) by nia.
  subst.
  assert (e9 = e22) by nia.
  subst.
  no_dup.
  assert (e22 = Int.unsigned x - (Int.unsigned y * e18) - (4294967296 * e8)).
  lia.
  subst.

  
  
Abort.
