Require Import Reals Fcore_FLX Fcore Fcalc_ops QArith.
Require Import List.
Require Import FOLib.
Require Import FOCompil.
Require Import InstanceFlocq.
Set Boolean Equality Schemes.

(** Declared types *)
Inductive btyp_u : Type := bt_bool | bt_R | bt_Z | bt_radix | bt_comparison | bt_nat | bt_Q | bt_positive.

Definition declStypes : list btyp_u := bt_bool :: bt_R :: bt_Z :: bt_radix :: bt_comparison :: bt_nat :: bt_Q :: bt_positive :: nil.

Definition declTypes := (bool:Type):: (R:Type) :: (Z:Type) :: (radix : Type) :: (comparison:Type) :: (nat : Type) :: (Q:Type) :: (positive:Type) :: nil.

Definition max_typ := 2.

Close Scope R_scope.


(* Register using type classes *)

Instance EqBtyp_u : EqTest.t btyp_u.
Proof.
  apply (EqTest.Mk btyp_u_beq).
  destruct t1 ; destruct t2 ; simpl ; intuition congruence.
Defined.

(* Map each type in declTypes to an element in declStypes *)
Lemma reify_btypes : Hlist.t (fun x => @MkName.t Type btyp_u (fst x) (snd x)) (List.combine declTypes declStypes).
Proof. reify_seq. Qed.

(** Register btyp_u *)
Lemma register_btyp : True.
Proof.
  MkName.enum declStypes.
Qed.

(** Bind btyp_u with Type *)
Require Import FOLType.

Definition etyp_u : btyp_u -> Type.
Proof. mk_etyp. Defined.

(** Compute the types required by declared operators *)

Definition styp := (list (option btyp_u) * option btyp_u)%type.

Definition operator_types : list styp.
Proof.
  operator_types btyp_u etyp_u declStypes 3.
Defined.

Lemma reify_operator_types : Hlist.t (fun x => @MkName.t _ _ (eval_my_typ _ etyp_u x) x) operator_types.
Proof. reify_seq. Qed.


Lemma number_types : Hlist.t (fun x => @MkName.t _ _ (fst x) (snd x)) (@MkName.mapi _ _ (fun x p => (x,p)) operator_types).
Proof. reify_seq. Qed.                                                            


Lemma reify_operators : True.
Proof. reify_operators operator_types. Qed.



Definition user_op := (positive * positive)%type.


Definition dump : btyp_u -> positive.
Proof. make_dump. Defined.

Definition parse : positive -> btyp_u * positive.
Proof. make_parse declStypes. Defined.

Lemma parse_dump : forall bt p, parse (concat (dump bt) p) = (bt,p).
Proof. parse_dump. Qed.

Definition inj_typ : btyp_u ->  option btyp_u.
Proof. 
  mk_inj_typ. 
Defined.

Opaque Qeq.

Definition equi_typ_u (bt:btyp_u) : etyp_u bt -> etyp_u bt -> Prop.
Proof.  equi_typ etyp_u bt. Defined.

Definition inj_cst (bt : btyp_u) : etyp_u bt -> Btyp.etyp_prop etyp_u (inj_typ bt).
Proof.
  inj_cst etyp_u bt.
Defined.

Definition dtyp : forall (bt:btyp_u), etyp_u bt.
Proof. intro bt ; dtyp etyp_u bt. Defined.

Instance Btyp : Btyp.t btyp_u.
Proof.
apply Btyp.Mk with (etyp := etyp_u) (inj_typ := inj_typ) (t_beq := btyp_u_beq) (equi_typ := equi_typ_u).
* apply (Parser.Mk dump parse).
  parse_dump.
* exact dtyp.
*  (intros t1 t2 ; destruct t1 ; destruct t2 ; simpl ; congruence).
* abstract (is_equi_typ etyp_u bt).
* exact inj_cst.
Defined.

Instance BtypPropI : BtypProp.t btyp_u := Make Btyp.
Import BtypProp.


Definition xop_args_u : user_op ->  list (option btyp_u).
Proof. mk_op_args_u btyp_u operator_types. Defined.

Definition op_args_u := Eval compute in xop_args_u.

Definition xop_ret_u : user_op ->  option btyp_u.
Proof. mk_op_ret_u btyp_u operator_types. Defined.

Definition op_ret_u := Eval compute in xop_ret_u.

Unset Ltac Debug.            

Transparent block.

Definition xop_eval_u : forall (o:user_op),  eval_typ etyp (op_args_u o) (op_ret_u o).
Proof. mk_op_eval_u btyp_u operator_types.
Defined.

Definition op_eval_u :=  Eval cbv beta iota zeta delta [xop_eval_u] in xop_eval_u.

(** Proving properties *)
Lemma op_is_morph :    forall o, is_morph BtypProp.etyp BtypProp.equi_typ (op_eval_u o).
Proof. make_morph btyp_u operator_types.
Defined.

Require Import FOSimpl.


Instance syntax_arg : TrmWithProp.Arg.t btyp_u (positive*positive):=
@TrmWithProp.Arg.Mk _  (positive*positive) op_args_u op_ret_u.
Require Import FOexpr.
Import TrmSyntax.


Instance syntax : TrmSyntax.t := 
  @TrmWithProp.MakeSyntax btyp_u _   syntax_arg etyp_u.
Canonical Structure syntax.
Import TrmSyntax.


Instance expr_sem : TrmSemantics.t syntax :=
  TrmWithProp.MakeSemantics syntax_arg _ op_eval_u equi_typ_u.
Canonical Structure expr_sem.
Import TrmSemantics.


Definition xinj_u : forall  (o:user_op), Hlist.t (Trm  positive) (List.map inj_typ  (TrmSyntax.op_args  (inr o))) -> Trm positive (inj_typ (op_ret (inr o))).
Proof.
  make_inj_u btyp_u etyp op op_eval_u operator_types.
Defined.


Definition inj_u  := Eval cbv beta iota zeta delta [xinj_u] in xinj_u.


Lemma inj_morph :
  forall (env : Make.Env Btyp syntax_arg positive) 
         (o : op) (l l' : Hlist.t (Trm positive) (List.map inj_typ (op_args o))),
    Hlist.equiv
      (fun (bt : btyp) (e1 e2 : Trm positive bt) =>
         equi_typ bt (interp env e1) (interp env e2)) l l' ->
    equi_typ (inj_typ (op_ret o)) (interp env (Make.inj inj_u o l))
             (interp env (Make.inj inj_u o l')).
Proof.
  make_inj_morph btyp_u operator_types.
Qed.



Definition xtype_cstr : forall (bt:btyp) (x:positive), option (Trm positive None).
Proof. 
  make_type_cstr btyp_u etyp  op. Defined.

Definition type_cstr := Eval cbv beta iota zeta delta [xtype_cstr] in xtype_cstr.



 Lemma type_cstr_lem : 
   forall (bt : btyp) (v : positive) (e : Trm positive None)
          (env : Env (Make.expr_synt Btyp syntax_arg) positive),
     type_cstr bt v = Some e -> interp env e.
 Proof. 
make_type_cstr_lem etyp. Qed.


Lemma inj_vars : 
    forall (o : op)
           (s : Hlist.t (Trm positive) (List.map BtypProp.inj_typ (op_args o)))
           (v : positive),
      In v (vars (Make.inj inj_u o s)) ->
      In v (Hlist.concat_map_list (vars (var:=positive)) s).
  Proof.
    make_inj_vars btyp_u operator_types.
  Qed.


  Lemma inj_cst_lem : 
    forall (env : Make.Env _ syntax_arg positive) 
           (o : op) (vls : Hlist.t etyp (op_args o)),
      equi_typ (BtypProp.inj_typ (op_ret o))
                    (BtypProp.inj_cst (op_ret o) (interp_seq vls (op_eval o)))
                    (interp env
                            (Make.inj inj_u o
                                       (Hlist.mapt (Make.inj_Cst Btyp syntax_arg positive) vls))).
  Proof. make_inj_cst_lem btyp_u etyp operator_types. Qed.


  Instance make_inj : 
    Make.spec inj_u type_cstr op_eval_u.
  Proof.
    constructor.
    apply inj_vars.
    apply inj_cst_lem.
    apply inj_morph.
    apply type_cstr_lem.
  Defined.

Require Import FOReify.



Definition xsimplify_op : forall (o:user_op), Make.simp_rule (inr o).
  Unset Ltac Debug.
  make_simplify btyp_u etyp op operator_types.
Defined.

Definition simplify_op (o:op) : Make.simp_rule o :=
  match o with
    | inl _ => Make.Id _
    | inr o => xsimplify_op o
  end.



Lemma simplify_op_correct : 
   forall o : op,
   match simplify_op o with
   | Make.Unfold f =>
       forall (env : Env (Make.expr_synt Btyp syntax_arg) positive)
         (args : Hlist.t (Trm positive) (op_args o)),
       equi_typ (op_ret o) (interp env (f args)) (interp env (Op o args))
   | Make.Abstract l p =>
       forall (env : Env (Make.expr_synt Btyp syntax_arg) positive)
         (args : Hlist.t (Trm positive) (op_args o))
         (r : Trm positive (op_ret o)),
       equi_typ (op_ret o) (interp env (Op o args)) (interp env r) ->
       exists (args' : Hlist.t etyp l) , interp env (p r args (Hlist.map (fun bt c => Cst bt c) args'))
 | Make.Id => True
   end.
Proof.
  make_simplify_correct btyp_u operator_types.
Defined.




Lemma simplify_var_correct : 
   forall o : op,
   match simplify_op o with
   | Make.Unfold f =>
       forall (args : Hlist.t (Trm positive) (op_args o)) (x : positive),
       In x (vars (f args)) <->
       In x (Hlist.concat_map_list (vars (var:=positive)) args)
   | Make.Abstract l p =>
     forall args res args', forall x, 
       In x (vars  (p res args args')) <-> 
       ((In x (vars  (Op o args))) \/ In x (vars res) \/ In x (Hlist.concat_map_list (@vars _ positive)  args'))
   | Make.Id => True
   end.
Proof.
  make_simplify_var_correct btyp_u operator_types.
Qed.


Lemma simplify_morph : 
     forall o : op,
   match simplify_op o with
   | Make.Unfold _ => True
   | Make.Abstract l p =>
       forall (r : Trm positive (op_ret o))
         (args : Hlist.t (Trm positive) (op_args o))
         (args1 args2 : Hlist.t (Trm positive) l)
         (env : Env (Make.expr_synt Btyp syntax_arg) positive),
       Hlist.equiv
         (fun (l0 : btyp) (v v' : Trm positive l0) =>
          equi_typ l0 (interp env v) (interp env v')) args1 args2 ->
       (interp env (p r args args1) <-> interp env (p r args args2))
   | Make.Id => True
   end.
Proof.
  make_simplify_morph btyp_u operator_types.
Qed.  

Instance simplifier : @Make.Simpl btyp_u _ _ syntax_arg op_eval_u.
  apply Make.MkS with (simplify_op := simplify_op).
  apply op_is_morph.
  apply simplify_op_correct.
  apply simplify_var_correct.
  apply simplify_morph.
Defined.

(** Check that tactics extract the right object *)

  Lemma get_btyp : exists ty, ty = btyp_u.
    let ty := btyp_u_of_inj make_inj 
    in exists ty.
              reflexivity.
  Defined.

  Lemma get_op : exists ty, ty = op.
    let bt := btyp_u_of_inj make_inj in
    let op := op_of_btyp_u bt in
    exists op.
           reflexivity.
  Defined.

  Lemma get_etyp : exists f , f = (@etyp _ ).
  Proof.
    let etyp_u := etyp_u_of_inj make_inj in
    let etyp := etyp_of_etyp_u etyp_u in
    exists etyp ; reflexivity.
  Qed.


  Lemma get_dtyp : exists f , f = dtyp.
    let f := dtyp_of_inj make_inj in
    exists f.
           reflexivity.
  Qed.

(*Global Opaque Z.pow_pos Pos.of_succ_nat Pos.of_nat Pos.to_nat Pos.succ Pplus Pminus Pmult. *)
(*Global Opaque Pmax Ppred beq_nat NPeano.leb NPeano.ltb Zeq_bool Z.leb Z.ltb Z.geb Z.gtb. *)
Global Opaque  NPeano.pow. (*  NPeano.modulo minus  ge plus mult max NPeano.div *)