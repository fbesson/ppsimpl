(** Generic tactics for compiling a specification of operators into a tactic. *)
Require Import FOLib.
Require Import ZArith.
Require Import List.
Require Import FOReify.
Declare ML  Module "g_ltac_instance". (* Define the tactic Existing Instance *)

Open Scope list_scope.

Require Import FSets.FMapPositive.

Module FType.
  Require Import ZArith.
  (** We encode any finite type of cardinal [card_typ] by a positive of sufficient length. *)

  Section S.
    Variable card_typ : nat.
    
    Definition nb_bits := Pos.size_nat (Pos.of_nat card_typ).
    
    Fixpoint pad (n:nat) : positive :=
      match n with
        | O   => xH
        | S n => xO (pad n)
      end.

    Definition bits := pad nb_bits.
  End S.
End FType.

  

Module MkName.
  Require Import ZArith.
  (* For reification purpose, we need to provide a function fo type [Type -> positive].
     - This is impossible in Coq.
     - This is possible in Ltac (but a bit clumsy)
     - This is possible by abusing type classes.
       Using Ltac, we generate and register instances programmatically.
   *)



  Section S.
    Variable Dom   : Type.
    Variable CoDom : Type.

    Class t   {dom : Dom} {codom : CoDom} :=  Mk{idx : unit}.

  End S.

  Ltac vimage Dom CoDom D := 
    let x := constr:(@MkName.idx Dom CoDom D _ _) in
    match x with
    | @MkName.idx _ _ _ ?C _ => C
  end.

  Ltac vpre_image Dom CoDom  C :=
    let x := constr:(@MkName.idx Dom CoDom _ C _) in
    match x with
    | @MkName.idx _ _ ?D _ _ => D
  end.

  

  Ltac image CoDom D :=
    (** index_of_typ : forall (Dom:Type) (CoDom : Type) (D:Dom), (C:CoDom) *)
    let Dom := type of D in
    let x := constr:(@MkName.idx Dom CoDom D _ _) in
    match x with
    | @MkName.idx _ _ _ ?C _ => C
  end.

  Ltac pre_image Dom  C :=
    (** typ_of_index : forall (Dom:Type) (CoDom : Type) (C:CoDom), (D:Dom) *)
    let CoDom := type of C in
    let x := constr:(@MkName.idx Dom CoDom _ C _) in
    match x with
    | @MkName.idx _ _ ?D _ _ => D
  end.

  Ltac any_binding Dom CoDom :=
    let x := constr:(@MkName.idx Dom CoDom _ _ _) in
    match x with
    | @MkName.idx _ _ ?D ?C _ => constr:(D,C)
  end.
    
  
  Ltac simage D := 
    let x := constr:(@MkName.idx _ _ D _ _) in
    match x with
    | @MkName.idx _ _ _ ?C _ => C
  end.

  Ltac spre_image  C :=
    (** typ_of_index : forall (Dom:Type) (CoDom : Type) (C:CoDom), (D:Dom) *)
    let x := constr:(@MkName.idx _ _ _ C _) in
    match x with
    | @MkName.idx _ _ ?D _ _ => D
  end.

  Ltac register_tc:=
    (** Construct and register and instance for the current goal.
     *)
    let inst := fresh "inst" in 
    match goal with
      | |- ?X => 
        assert(X) by   abstract (apply Mk ; exact tt) using inst
        ; (Existing Instance inst); assumption
    end.

  Ltac register_mapping X Y :=
    first[
        (let H := fresh in 
         assert (H : @t _ _ X Y) by register_tc ; 
         clear H) | fail -1 "register_mapping: failure for " X Y].

  Ltac xenum T L P :=
    match L with
      | nil        => idtac
      | cons ?E ?L => 
        register_mapping E P;
          let P := (eval compute in (Psucc P)) in 
          xenum T L P
    end.

  Ltac enum L :=
    (* enum : (L:list _) , _
       generate instances for all the elements of L 
      and generate a proof of True *)
    let L := (eval compute in L) in 
    let P := (eval compute in (FType.bits (List.length L))) in
    match type of L with
      | list ?TY => xenum TY L P ; exact I
      |   ?T      => fail -1 "type error: enum expects a list but was given " T
    end.

  Section Map.
    Variables A B : Type.
    Variable F : A -> positive -> B.

    Fixpoint xmapi (p:positive) (l:list A) : list B :=
      match l with
        | nil => nil
        | cons e l => (F e p) :: (xmapi (Psucc p) l)
      end.

    Definition mapi (l:list A) : list B :=
      xmapi xH l.
    End Map.


End  MkName.


Ltac reify_seq := 
  (** Take a Hlist.t in the goal and create an MkName.t instance for each of the elements *)
  compute ; repeat apply Hlist.scons ; simpl; try MkName.register_tc ; apply Hlist.snil.


  (** One role of the tactics is to "compile" a function of type operator -> something
      where the number of operators is not fixed.
      To get an efficient implementation, operator is coded by a positive and
      the function is a sufficiently deep pattern-matching.
      To get a pattern matching of depth [n], 
      we execute the function [split] below for a given [n]
      and eliminate the generated match by the tactic [dest_positive] *)

Fixpoint split (n:nat) (p:positive) : positive * positive :=
  match n with
    | O => (xH,p)
    | S n => 
      match p with
        | xH => (xH,xH)
        | xI p => let (pl,pr) := split n p in
                  (xI pl,pr)
        | xO p => let (pl,pr) := split n p in
                  (xO pl,pr)
      end
  end.

Ltac dest_positive := 
  match goal with
    | |- context[match ?X with
                   | xH   => _
                   | xO _ => _
                   | xI _ => _
                 end] => destruct X
  end.


Ltac etyp BTYP := 
(** etyp (BTYP:_), Type *)
let ty := type of BTYP in 
MkName.pre_image Type BTYP.

Ltac reify_btyp_option BT T :=
  (** reify_btyp_option : forall (BT:Type) (T : Type), BT *)
  let BT := (eval compute in BT) in 
  match T with
    | Prop   => eval compute in (@None BT)
    | ?A   => let bt := MkName.image  BT (T:Type) in
              constr:(Some bt)
  end.

Ltac reify_type BT T :=
  let CoDom := (eval compute in (list (option BT) * (option BT))) in 
  MkName.image  CoDom T.


Ltac mk_etyp := 
  intro bt ; 
  set (bt' := bt) ; revert bt';
  destruct bt;
  match goal with
    | |- let _ := ?X in _ => 
      let res := MkName.pre_image Type X 
      in exact res
    | |- let _ := ?X in _  => fail -1 "mk_etyp error for" X
  end.

(* Definition etyp : btyp_u -> Type.
Proof. mk_etyp. Defined. *)


Section ArrowType.
(** Construction of arrow types *)
  Variable T : Type.

  Definition extend_arg (arg : list T) (res : list (list T)) :=
    List.map (fun a_r => (fst a_r) :: (snd a_r)) (List.list_prod arg res).

  Fixpoint xenum_types  (n : nat) (args: list T) (acc : list (list T)) :=
    match n with
      | O => acc
      | S n =>  xenum_types n args (nil::(extend_arg args acc))
  end.

Definition enum_types (n:nat) (l:list T) := 
  List.list_prod (xenum_types n l nil) l.


End ArrowType.

Require Import FOLType.



Definition eval_my_typ (btyp_u : Type) (etyp : btyp_u -> Type) (ty : list (option btyp_u) * option btyp_u) : Type :=
  eval_typ (Btyp.etyp_prop etyp) (fst ty) (snd ty).


Ltac filter_types BT ETYP L :=
(** filter_types : forall (BT: Type) (ETYP : BT -> Type) (L : list (list (option BT) * option BT)), 
    returns a lists only containing the used types. *)
  match L with
    | nil => L
    | cons ?E ?L => 
      let t := eval compute in (@eval_my_typ BT ETYP E) in
      match t with
        |  _  => let x := constr:(@DeclOperator.operators t _) in
                 let rest := filter_types BT ETYP L in
                 constr:(cons E rest)
        |  _  => filter_types BT ETYP L 
      end
  end.

Ltac operator_types BT ETYP BTYPS NAT :=
  (* operator_types : forall (BT: Type) (BTYPS : list BT) (NAT : nat) *)
  let NAT := constr:(S NAT) in
  let potential_ops := (eval compute in (enum_types _ NAT (None :: (List.map (@Some BT) BTYPS)))) in 
  let filtered := filter_types BT ETYP potential_ops in
  exact filtered.


Ltac reify_ops_of_type ST OPS IDX :=
  match OPS with
    | nil => idtac
    | cons ?E ?OPS => 
      let IDX_OP := constr:(ST,IDX) in 
      MkName.register_mapping E IDX_OP ; 
(*        idtac "register_op: " E IDX_OP ; *)
        let IDX := (eval compute in (Psucc IDX)) in 
        reify_ops_of_type ST OPS IDX
  end.

Ltac get_operators_of_type T := 
  let OPS := constr:(@DeclOperator.operators T _) in 
  match OPS  with
    | @DeclOperator.operators _ ?X => 
      eval unfold X, DeclOperator.operators in OPS
  end.

Ltac xreify_operators P OPS_TYPES :=
  match OPS_TYPES with
    | cons ?ST ?OPS_TYPES => 
      let ty := MkName.pre_image Type ST in
      let ops := get_operators_of_type ty in
      reify_ops_of_type P ops xH ; 
        let P' := (eval compute in (Psucc P)) in 
        xreify_operators P' OPS_TYPES
    | nil => idtac
  end.
                                                            
Ltac reify_operators OPS_TYPES :=
  let ops := (eval compute in OPS_TYPES) in 
  xreify_operators xH ops;
  exact I.
  

(*Lemma reify_operators : True. 
Proof reify_operators operator_types. Qed. *)

Ltac elim_car := 
  match goal with
    | |- context[@Hlist.car ?A ?B ?C ?D ?X] => 
      let v := fresh "a0"  in
      generalize (@Hlist.car A B C D X) ; intro v
  end.


Ltac prove_inj :=
  intros args ; 
  rewrite (Hlist.make_seq_id args);
  simpl ; try reflexivity ; repeat elim_car.

Ltac prove_morph:=
  intros args args';
  rewrite (Hlist.make_seq_id args) ;
  rewrite (Hlist.make_seq_id args') ;
  simpl ; unfold eq_rect_r ; simpl; try intuition congruence ; 
  repeat elim_car.


Ltac make_inj T :=
  refine (Inj.Mk T _ _ ) ; [prove_inj | prove_morph].


Ltac make_dump := 
  intro bt ; 
  set (bt' := bt) ; revert bt';
  destruct bt; 
    match goal with
      | |- let bt' := ?Y in  _ => 
        let res := MkName.simage Y in
        exact res
      | |- let bt' := ?Y in  _ => fail -1 "make_dump: cannot find index for " Y
    end.



(* Definition dump (bt : btyp) : positive. make_dump. Defined. *)

Ltac elt_of_list L :=
  match type of L with
    | list ?X => X
    |  _      => fail -1 "elt_of_list: not a list " L
  end.


Ltac make_parse L := 
  intro p ; 
  let n  := (eval compute in (FType.nb_bits (List.length L))) in 
  let ty := elt_of_list L in
  set (x := split n  p);
    simpl in x;
    revert x;
    repeat dest_positive; 
    match goal with
      | |- let _ := (?Y,?P) in _ => 
        let bt := MkName.pre_image ty Y in
        exact (bt,P)
      | |- _ => 
        match MkName.any_binding ty positive with
                    |(?X,_) => exact (X,xH)
                end
    end.

(* 
Definition xparse (p : positive) : btyp * positive. make_parse p.
Defined.

Definition parse  := Eval compute in xparse.
*)

Ltac parse_dump := intro bt ; destruct bt ; try reflexivity.


Ltac mk_inj_typ := 
  intro bt ; 
  let ty := type of bt in 
  set (bt' := bt); revert bt';
  destruct bt;
  match goal with
    | |- let _ := ?BT in _ => 
      let typ := MkName.pre_image Type  BT in
      let inj := constr:(@DeclInj.inj typ _ _ _ _) in
      match  inj  with
        | @DeclInj.inj _ _ ?TY' _ _ => 
          let bt := reify_btyp_option ty TY' in
          exact bt
      end
    |  |- let _ := ?BT in _ => fail -1 "Cannot find injection for " BT
  end.

(*Definition inj_typ : btyp_u ->  option btyp_u. 
Proof. mk_inj_typ. Defined. *)


Ltac equi_typ etyp bt := 
  set (bt' := bt);
  revert bt';
  destruct bt;
  match goal with
    | |- let bt := ?Y in _ => 
         let equi := (eval simpl in (@TypeDecl.equi (etyp Y) _)) in 
         exact equi
  end.

(* Definition equi_typ (bt:btyp) : etyp bt -> etyp bt -> Prop.
    Proof equi_typ equi_typ bt. Defined. *)


Ltac inj_cst etyp bt :=
  set (bt' := bt) ; revert bt' ;
  destruct bt;
  match goal with
    | |- let bt' := ?Y in _ =>
      let ty := (eval compute in (etyp Y)) in 
      let val := constr:(@DeclInj.inj ty _ _ _ _)  in
      match val  with
        | @DeclInj.inj _ _ _ _ ?X  => 
          let t' := (eval unfold X, DeclInj.inj in val) in 
          exact t'
      end
  end.

(* Definition inj_cst (bt : btyp) : etyp bt -> Btyp.etyp_prop etyp (inj_typ bt).
   inj_cst etyp bt *)


Ltac dtyp etyp bt := 
  set (bt' := bt);
  revert bt';
  destruct bt;
  match goal with
    | |- let bt := ?Y in _ => 
         let dflt := (eval cbv in (@TypeDecl.dtyp (etyp Y) _)) in 
         exact dflt
  end.

(* Definition dtyp (bt:btyp) : etyp bt. dtyp ety bt. Defined. *)

Ltac is_equi_typ etyp bt := 
  intro;
  set (bt' := bt);
  revert bt';
  destruct bt;
  match goal with
    | |- let bt' := ?Y in _ =>
      let val := constr:(@TypeDecl.is_equi (etyp Y) _) in 
      match val with
        | @TypeDecl.is_equi (etyp Y) ?X => 
          let t' := (eval unfold X, TypeDecl.is_equi in val) in 
          exact t'
      end
  end.

(* Instance is_equi_typ : forall bt : btyp, Equivalence (equi_typ bt).
Proof. is_equi_typ etyp bt Qed. *)

Ltac mk_op_args_u BT OPS :=
  intro o; 
  let styp := constr:(list (option BT) * option BT)%type in 
  destruct o as [ty o'];
  let n := (eval compute in (pred (FType.nb_bits (List.length OPS)))) in 
  set (x := split n ty);
    simpl in x;
    revert x ; repeat dest_positive;
    match goal with
      | |- let _ := (?T,_) in _ => 
        let st := MkName.pre_image styp  T in 
        let arg  := (eval compute in (fst st)) in 
        exact  arg
      | |- _ => exact nil
    end.


Ltac mk_op_ret_u BT OPS :=
  intro o; 
  let styp := constr:(list (option BT) * option BT)%type in 
  destruct o as [ty o'];
  let n := (eval compute in (pred (FType.nb_bits (List.length OPS)))) in 
  set (x := split n ty);
    simpl in x;
    revert x ; repeat dest_positive;
    match goal with
      | |- let _ := (?T,_) in _ => 
        let st := MkName.pre_image styp  T in 
        let arg  := (eval compute in (snd st)) in 
        exact  arg
      | |- _ => exact None
    end.


Ltac forall_ops T O CT TAC  :=
  let opn := (eval compute in 
                 (pred (FType.nb_bits 
                          (List.length (@DeclOperator.operators CT _))))) in 
  set (y := split opn O) ; 
    simpl in y; 
    revert y ; repeat dest_positive;
    match goal with
      | |- let _ := (?OP,_) in _ => 
        TAC CT (T,OP)
      | |- let _ := (?OP,_) in _ => 
        TAC CT (T,xH) 
    end.



Ltac forall_types BT TYPES O TAC := 
  let styp := constr:(list (option BT) * option BT)%type in 
  let n := (eval compute in (pred (FType.nb_bits (List.length TYPES)))) in 
  destruct O as [ty o'];
  set (x := split n ty);
    simpl in x;
    revert x ; repeat dest_positive;
    match goal with
      | |- let _ := (?T,_) in _ => 
        let ST := MkName.pre_image styp T  in
        let CT := MkName.pre_image Type ST in
        intro x ; forall_ops T o' CT TAC 
      | |-  _ => idtac
    end.
          
Ltac op_eval CT OP := 
  let OP := MkName.pre_image CT OP in 
  exact (block  OP).

Ltac mk_op_eval_u BT OPS :=
  intro o ; forall_types BT OPS o op_eval ; exact True.


(*
Definition xop_eval_u : forall (o:user_op),  eval_typ etyp (op_args_u o) (op_ret_u o).
Proof.
  mk_op_eval_u btyp_u operator_types.
Defined.


Definition op_eval_u :=  Eval cbv beta iota zeta delta [xop_eval_u] in xop_eval_u.
*)


Ltac morph_tac X Y := unfold is_morph ; intros ; Hlist.equiv_tac ; subst ; 
                    simpl in * ; unfold block ;  
                    intuition try congruence ; try typeclasses eauto;
                    repeat match goal with
                      | H : ?F ?X ?Y |- _ => rewrite H in *
                    end ; auto.



Ltac make_morph BT OPS := 
  intro o ; forall_types BT OPS o morph_tac ; morph_tac Prop Prop.

(*Lemma op_is_morph :    forall o, is_morph BtypProp.etyp BtypProp.equi_typ (op_eval_u o).
Proof. make_morph btyp_u operator_types.
Defined.
*)

(** Reification of expressions *)


Ltac extract_btyp BTYP :=
  (** extract_btyp : BTYP as option btyp_u), btyp_u *)
  let BTYP := (eval compute in BTYP) in
  match  BTYP with
    | option ?T => T
    | _         => fail -1 "extract_btyp :" BTYP " should be an option type "
  end.

Ltac btyp_of_bt BT := 
  match type of BT with
    | option ?T => T
    |   _       => fail -1 "btyp_of_bt error :" BT "should have type option ?X"
  end.

Require Import FOSimpl.

Ltac reify_prop op BT F X Y :=
  match BT with
    | None => 
      match type of F with
        | ?T -> ?T -> Prop =>
          let equi := constr:(@TypeDecl.equi T _) in
          let equi :=
              match equi with
                | @TypeDecl.equi ?X ?Y => (eval unfold TypeDecl.equi, Y in equi)
              end in
          match constr:(F = equi) with
            | (?F = ?F) => 
              let BTYP := btyp_of_bt BT in
              let bt := reify_btyp_option BTYP T in
              constr:(@Some op (inl (TrmWithProp.Prop_Eq bt)))
            |   _       => constr:(@None op)
          end
        | _   =>  constr:(@None op)
      end
    | _ => constr:(@None op)
  end.


(*
      let tx := type of X in
      let ty := type of Y in
      match constr:(tx = ty) with
               | (?T = ?T) => 
                 let equi := (eval cbv in (@TypeDecl.equi tx _)) in
                 match constr:(F = equi) with
                   | (?F = ?F) => 
                     let BTYP := btyp_of_bt BT in
                     let bt := reify_btyp_option BTYP tx in
                     constr:(@Some op (inl (TrmWithProp.Prop_Eq bt)))
                   |   _       => constr:(@None op)
                 end
               | _   =>  constr:(@None op)
             end
           | _ => constr:(@None op)
         end.
    Unset Ltac Debug.
*)
Module DPair.

  Section S.
  Variable T : Type.
  Variable etyp : T -> Type.

  Definition make (x:T) (e:etyp x) : {x : T & etyp x} := existT _ x e.

  Definition dest1 (X : {x : T & etyp x}) : T :=
    match X with
      | existT bt _ => bt
    end.

  Definition dest2 (X : {x : T & etyp x}) : etyp (dest1 X):=
    match X with
      | existT bt v => v
    end.

  End S.

  Ltac make ETYP X E := 
    eval unfold make in (@make _ ETYP X E).

  Ltac dest1 X :=
    (eval cbv beta iota delta [dest1]  in (@dest1 _ _ X)).

  Ltac dest2 X :=
    (eval cbv beta iota delta [dest2]  in (@dest2 _ _ X)).

End DPair.

Ltac mkSig BTYP ETYP X :=
  let tyX := type of X in
  let btX := reify_btyp_option BTYP tyX in
  DPair.make  ETYP btX  X .

Ltac eq_dpair BT X Y :=
  let TY := DPair.dest1 Y in
  match constr:(TY = BT) with
    |( ?V = ?V)  => 
     let ELT := DPair.dest2 Y in
     match constr:(ELT = X) with
       | (?X = ?X) => true
       |   _       => false
     end
    | _ => false
  end.


Ltac add_variable ETYP VARS n BT X :=
(** 
add_variable : list {x : btyp & etyp x} -> nat -> forall (T:btyp) (X:etyp T), list {x : btyp & etyp x} * idx
Return the position of X in the return sequence.
Should never fail
 **)
  match VARS with
    | nil => 
      let idx := (eval compute in (Pos.of_nat (S n))) in
      let elt := DPair.make ETYP BT X in
      constr:(cons elt nil,idx)
    | cons ?e ?le => 
      match eq_dpair BT X e with
        | true => 
          let idx := (eval compute in (Pos.of_nat (S n))) in 
          constr:(VARS,idx)
        | false =>           
          let res := add_variable ETYP le (S n) BT X in
          match res with
            | (?L,?IDX) => constr:(cons e L, IDX)
          end
        |  _    => fail -1 "add_variable : eq_pair should return a boolean"
      end
    | _ => fail -1 "add_variable : " VARS n BT X
  end.

Ltac app  E ARGS :=
  match ARGS with
    | nil         => E
    | cons ?A ?AL => let A := DPair.dest2 A in
                     let E := constr:(E A) in 
                     app E AL
    |   _         => fail -1 "app failure :" E ARGS
  end.
      
Require Import FOexpr.



Ltac reify_expr BTYP ETYP TYOP VARS E ARGS :=
  (** reify_expr (VARS: list ({x : btyp & etyp x})) (E : etyp _) (ARGS : list ({x : btyp & etyp x})) *)
  match E with
    | ?A -> ?B => 
      let OP := constr:((inl (TrmWithProp.Prop_Impl BTYP)):TYOP) in
      reify_binary BTYP ETYP TYOP VARS OP A B


    | iff ?A ?B =>  
      let OP := constr:((inl (TrmWithProp.Prop_Eq None)):TYOP) in
      reify_binary BTYP ETYP TYOP VARS OP A B


    | ?A /\ ?B => 
      let OP := constr:((inl (TrmWithProp.Prop_And BTYP)):TYOP) in
      reify_binary BTYP ETYP TYOP VARS OP A B


    | ?F ?X ?Y => 

      (* E might be an equivalence relation *)
      let ty := type of E in
      let BT := reify_btyp_option BTYP ty in (* iff should be handled here *)
      match reify_prop TYOP BT F X Y with
        | Some ?O => 
          reify_binary BTYP ETYP TYOP VARS O X Y
      end

    | ?F ?X => 

      (* E is an application *)
      let X  := mkSig BTYP ETYP X in 
      reify_expr BTYP ETYP TYOP VARS F (cons X ARGS)

    | ?E    => 

      (* E is a known operator *)
      let TY := type of E in
      let O := MkName.vimage TY (positive*positive)%type E in
      match reify_args BTYP ETYP TYOP VARS ARGS with
        | (?VARS, ?ARGS) => constr:(VARS, Op ((inr O):TYOP) ARGS)
      end

    |   ?E  => 
        let F  := app E ARGS in
        let ty := type of F in
        let BT := reify_btyp_option BTYP ty in        
        let res := add_variable ETYP VARS O BT F in
          match res with
            | (?VARS , ?IDX) => constr:(VARS , Var BT IDX)
          end
    |   _  => fail -1 "reify_expr :" BTYP VARS E ARGS
  end
with reify_binary BTYP ETYP TYOP VARS OP X Y := 
  let X := mkSig BTYP ETYP X in
  let Y := mkSig BTYP ETYP Y in
  let ARGS := constr:(cons X (cons Y nil)) in
  match reify_args BTYP ETYP TYOP VARS ARGS with
    | (?VARS , ?ARGS) => 
      constr:(VARS,Op OP ARGS)
  end


with reify_args := fun BTYP ETYP TYOP VARS ARGS => 
  match ARGS with
    | nil => constr:(VARS, @Hlist.snil (option BTYP) (Trm positive))
    | cons ?e ?le => 
      let BT := DPair.dest1 e in 
      let E  := DPair.dest2 e in 
      let TY := type of e  in
      let A  := reify_expr BTYP ETYP TYOP VARS E (@nil TY) in
      match A with
        | (?VARS, ?E) => 
          let RL := reify_args BTYP ETYP TYOP VARS le in
          match RL with
            | (?VARS,?RL) => 
                  constr:(VARS, Hlist.scons E RL)
          end
      end
    | _   => fail -1 "reify_args: " VARS ARGS
  end.
Unset Ltac Debug.

Section mkEnv.
  Variable T : Type.
  Variable etyp : T -> Type.
  Variable dtyp : forall (x:T), etyp x.
  Variable deflt : T.

  Variable EqT : EqTest.t T.
  Existing Instance EqT.

  Fixpoint collect_vars (l : list {x : T &  etyp x}) (n:positive) (acc : list (T * positive)) : list (T * positive) :=
    match l with
      | nil => acc
      | cons (existT bt _)  l => collect_vars l (Psucc n) ((bt,n)::acc)
    end.


  Definition dmap := PositiveMap.t {x : T & etyp x}.

  Fixpoint xmakeMap (l : list {x : T & etyp x}) (p:positive) (m: dmap) : dmap :=
    match l with
      | nil => m
      | e::l => xmakeMap l (Psucc p) (PositiveMap.add p e m)
    end.

  Definition makeMap (l : list {x : T & etyp x})  : dmap :=
    xmakeMap l xH (PositiveMap.empty {x : T & etyp x}).
                  
  Definition mkEnv (m:dmap) : forall (vr : positive) (bt : T) , etyp bt :=
    fun vr => 
      match PositiveMap.find vr m with
        | None => dtyp
        | Some (existT bt' r) => fun bt => 
          match EqTest.eq_dec bt bt' with
           | left Heq => eq_rect_r _ (block r) Heq
           | right _  => dtyp bt
          end
      end.

  Arguments mkEnv / m vr bt.

(* obsolete 

  Definition mkEnv  (l : list {x : T & etyp x}) : forall (vr:positive) (bt : T), etyp bt.
    refine(
        fun vr => 
          match List.nth (pred (Pos.to_nat vr)) l (existT _ deflt (dtyp deflt)) with
            | existT bt' e => fun bt =>
                                if EqTest.eq_dec bt' bt 
                                then  _ e 
                                else dtyp _
          end
      ).
  rewrite _H.
  auto.
Defined.
*)
End  mkEnv.

Ltac btyp_u_of_inj MK_INJ := 
  match type of MK_INJ with
    | @Make.spec ?TY _ _ _ _ _ _ => TY
  end.

Ltac op_of_btyp_u  BTYP :=
  constr:(TrmWithProp.op_prop BTYP + positive * positive)%type.

Ltac etyp_of_btyp BTYP :=
  constr:(@FOLType.Btyp.etyp BTYP _).

Ltac etyp_u_of_inj MK_INJ := 
  match type of MK_INJ with
    | @Make.spec _ ?BTYP _ _ _ _ _ => constr: (@FOLType.Btyp.etyp _ BTYP)
  end.

Ltac etyp_of_etyp_u ETYP := 
  constr:(@TrmWithProp.etyp _ ETYP).

Ltac dtyp_of_inj MK_INJ := 
  match type of MK_INJ with
    | @Make.spec _ ?BTYP _ _ _ _ _ => constr: (@FOLType.dtyp _ BTYP)
  end.

Ltac reify_goal BTYP TYOP :=
  let my_etyp   := constr:(@TrmSyntax.etyp  _) in
  let my_dtyp   := constr:(@BtypProp.dtyp BTYP _) in

  let my_nil := constr:(@nil ({x : option BTYP & my_etyp x})) in
  
  match goal with
    | |- ?G => let e := reify_expr BTYP my_etyp TYOP my_nil G  my_nil in
               match e with 
                 | (?VARS ,  ?E) => 
                   let map := (eval unfold makeMap in (@makeMap _ my_etyp VARS)) in
                   let smap := (eval simpl in map) in
                   set (xx := E) ; 
                     set (env := @mkEnv _ my_etyp my_dtyp (@option_beq_test _ _)  smap) ;
                     set (val := @interp _ positive _  _ env xx);
                     try change val
               end
  end.

Ltac reify_fun BTYP ETYP TAC F ARGS T  :=
  match T with
    | ?T1 -> ?T2 => 
      let hyp := fresh "H" in
      let arg := fresh "x" in
      assert (hyp := exist (fun _ : T1 => True) TypeDecl.dtyp I);
        destruct hyp as [arg _] ; 
        let TY := reify_btyp_option BTYP T1 in
        let ARG := DPair.make  ETYP TY arg in
        let ARGS' := constr:(cons ARG ARGS) in
        let F'    := (eval cbv beta in  (F arg)) in 
        reify_fun BTYP ETYP TAC F' ARGS' T2
    | _ => 
      let A := (eval simpl in (List.rev ARGS)) in 
      (*idtac "Reify1";*)
      TAC A F
  end.
Unset Ltac Debug.





(** Simplification of expressions *)
Require Import FOexpr.

Section S.
  Variable expr_synt : TrmSyntax.t.
  Existing Instance expr_synt.
  Import TrmSyntax.

  Variable EqTestBtyp : EqTest.t btyp.
  Existing Instance EqTestBtyp.



  Inductive LKind :=
  | Null
  | Inter (p q : positive)
  | Other.
  

  Definition is_var (r : btyp) (e : Trm positive r) : option positive :=
    match e with
      | Var _ p => Some p
      | _           => None
    end.

  Fixpoint range (l:list btyp ) (s : Hlist.t (Trm positive) l) : LKind :=
    match s with
      | Hlist.snil => Null
      | Hlist.scons x e lx le => 
        match is_var _ e with
          | Some p => 
            match range _ le with
              | Null => Inter p p 
              | Inter q r => if Pos.eqb (Psucc p) q then Inter p r else Other
              | Other     => Other
            end
          | None  => Other
        end
    end.

  Definition full_range (l : list btyp) (s : Hlist.t (Trm positive) l) : bool :=
    match range _ s with
      | Inter p q => match p with
                       | xH => true
                       |  _  => false
                     end
      | _ => false
    end.



  Section S.
    Variable fsimpl : forall (r:btyp), Trm positive r -> Trm positive r.
    Variable l : list btyp.
    Variable args : Hlist.t (Trm positive) l.

    Fixpoint simplify_seq 
             (l' : list btyp) (s : Hlist.t (Trm positive) l') : Hlist.t (Trm positive) l'.
    Proof.
      refine (
          match nat_compare  (List.length l) (List.length l') with
          | Lt => (* Do a recursive call *)
            match s with
              | Hlist.snil => Hlist.snil
              | Hlist.scons x xe xl xle => Hlist.scons (fsimpl _ xe) (simplify_seq  _ xle)
            end
          | Gt => (* Can't do anything *)
            Hlist.map fsimpl s
          | Eq => 
            match list_eq_dec EqTest.eq_dec l l' with
              | left e0 =>
                if full_range l' s
                then  _ args
                else Hlist.map fsimpl s
              | right _ => Hlist.map fsimpl s
            end
        end).
    * rewrite e0.
      exact (fun x => x).
  Defined.

  End S.


  Fixpoint simplify_expr  (l : list btyp) (s : Hlist.t (Trm positive) l) (r : btyp) (e: Trm positive r) {struct e}: Trm positive r.
  refine(
      match e in (Trm _ b) return (Trm positive b) with
        | Cst bt v => Cst bt v
        | Var bt v => 
          match EqTest.eq_dec bt (nth (pred (Pos.to_nat v)) l r) with
            | left e0 =>
                (eq_rect_r (fun b : btyp => Trm positive b) (Hlist.nth (pred (Pos.to_nat v)) s e) e0)
            | right _ => Var bt v
      end
        | Op o args => 
      Op o (simplify_seq (simplify_expr l s) l s _ args)
      end).
  Defined.

  (** Could also simplify *)

  Definition subst_var (rty : btyp) (rt : Trm positive rty) (l:list btyp) (s : Hlist.t (Trm positive) l) (bt : btyp) (v : positive) : Trm positive bt :=
    match v with
      | xH => 
        match EqTest.eq_dec bt rty with
          | left e0 =>  (eq_rect_r (fun b : btyp => Trm positive b) rt e0)
          | right _ => Var bt v (* Should not happen *)
        end
      | _ => let idx := (pred (pred (Pos.to_nat v))) in
        match EqTest.eq_dec bt (nth idx l bt) with
               | left e0 => (eq_rect_r (fun b : btyp => Trm positive b) (Hlist.nth idx s (Var bt v)) e0)
               | right  _ => Var bt v
        end
    end.


  Fixpoint simplify_abstract (rty : btyp) (rt : Trm positive rty) (l : list btyp) (s : Hlist.t (Trm positive) l) (r : btyp) (e: Trm positive r) : Trm positive r :=
    match e in (Trm _ b) return (Trm positive b) with
      | Cst bt v => Cst bt v
      | Var bt v => subst_var rty rt l s bt v
      | Op o l' => Op o (Hlist.map (@simplify_abstract rty rt l s) l')
    end.


End S.

Ltac make_inj_u BTYP ETYP OPT op_eval_u  operator_types:= 
  intros o args;
  match goal with
  | FARGS : Hlist.t _ _ |- _ => 
    let  get_op CT OP := 
           let F := (eval simpl in (op_eval_u OP)) in
           (*idtac "get_op" OP F;*)
             match F with
               |  _   => 
                  let NF := constr:(@Inj.inj_op _ F _) in
                  match NF with
                    | @Inj.inj_op _ _ ?X => 
                      let NF' := (eval unfold X,Inj.inj_op in  NF) in 
                      let T := type of NF' in 
                      let ARGS := constr:(@nil {x:option BTYP & ETYP x}) in
                      let tac A E := 
                          ((*idtac "T1";*)
                           let ARGS := constr:(@nil {x: option BTYP & ETYP x}) in
                           let e := reify_expr BTYP ETYP OPT A E ARGS in
                           (*idtac "non-simpl: " e;*)
                           let e' := (eval compute in 
                                         (@simplify_expr _ _ _ FARGS _ (snd e))) in
                           (*idtac "simpl: " e';*)
                           try exact e' ; idtac "exact failed"
                          ) 
                      in
                      reify_fun BTYP ETYP tac NF' ARGS T
                  end
               | True => (*idtac "True " ; *) apply (@Make.Prop_True BTYP _ _ _ _) ; auto ; fail -1 F
               |  ?X   => (*idtac "default " X ;*) apply (@Op _ _ ((inr OP):OPT) FARGS)
             end in
    forall_types BTYP operator_types o get_op
end  ;   try refine (@Make.Prop_True BTYP _ _ _ _).




Ltac make_inj_morph  BTYP  OPS := 
  let morph_tac H ENV L L' CT OP := 
        let OP := MkName.pre_image CT OP in
        let NF := constr:(@Inj.inj_morph _ OP _ ) in
        first[
        exact (NF (@Make.eval_seq _ _ _ _ _ ENV _ L) (@Make.eval_seq _ _ _ _ _ ENV _ L') H)
          |
          set (HH := NF)
          ]
  in 

        (Existing Instance is_equi ) ;
    intros env o l l' H;
        Hlist.equiv_tac ;
      destruct o as [o | o] ;[
        destruct o as [ | | | bt] ; simpl in * ; try tauto;
        unfold eq_rect_r in H ; simpl in H;
        destruct H as [HH1 [HH2 HH3]];
        destruct bt as [bt|]; [destruct bt |] ; simpl in * ; rewrite HH1 ; rewrite HH2 ; try tauto
        | 
        revert H;
        intros;
        let f CT OP := try (morph_tac H env l l' CT OP) in
        forall_types BTYP OPS o f ; simpl;
          simpl in H;  unfold eq_rect_r in H ; simpl in H ; 
          try intuition congruence
      ].




Ltac type_cstr BTYP ETYP OPT :=
  match goal with
    | P : positive |- let _ := ?Typ in _ => 
       let ty := (eval compute in (ETYP Typ)) in
       let TC := constr:(@TypeDecl.type_cstr ty _ ) in
       match TC with
         | @TypeDecl.type_cstr ty ?TD  => 
           let TC' := (eval unfold TD, TypeDecl.type_cstr in TC) in
           match TC' with
             | None => exact None
             | Some ?TC => 
               let T := type of TC in 
               let ARGS := constr:(@nil {x:option BTYP & ETYP x}) in
               let tac A E := 
                   let e' := reify_expr BTYP ETYP OPT A E ARGS in
                   let e' := (eval compute in (FOexpr.map (fun r v => Var r P) (snd e'))) in 
                   try exact (Some e') ; 
                     (let x := fresh "DEBUG" in  
                     set (x :=  e')) in
               reify_fun BTYP ETYP tac TC ARGS T
           end
       end
  end.


Ltac make_type_cstr BTYP ETYP OPT :=
  intros bt x ; 
  set (bt':= bt);
  revert bt';
  destruct bt as [b|];
  [(destruct b;
   type_cstr BTYP ETYP OPT)
   |
  exact None].

Ltac make_type_cstr_lem ETYP := 
    intros bt v e env H;
    set (bt' := bt);
    destruct bt as [bt|];
    [
      intros until 0;
      simpl;
      destruct bt ; simpl ; try discriminate;
      inv H;
      simpl;
      let var := (eval unfold bt' in  (env v bt')) in 
      generalize var;
      let et := (eval compute  in (ETYP bt')) in 
      (apply (@TypeDecl.is_type_cstr et _) ; try reflexivity)
    | simpl;
      discriminate].



Ltac make_inj_vars BTYP OPS := 
    intro o;
    destruct o as [o | o];
    [ destruct o ; simpl ; auto
    |
    intro s ; 
      rewrite (Hlist.make_seq_id s);
      let tac A B := (simpl ; repeat rewrite <- app_nil_end ; 
                      repeat rewrite in_app ; simpl ; try tauto ) in
      forall_types BTYP OPS o tac ; intro ; simpl ; repeat rewrite in_app ; try tauto].

  Ltac inj_1_cst VLS A C := 
    let op := MkName.pre_image A C in
    let op := (eval unfold block in op) in 
    exact ((@Inj.inj_spec _ op _ ) (@Hlist.make_seq' _ _ _ VLS)).


  Ltac make_inj_cst_lem BTYP ETYP OPS := 
      intros env o vls; destruct o as [o | o];
      [
        revert vls;
        destruct  o as [ | | | bt]; simpl in * ; intros; rewrite (Hlist.make_seq_id vls) ; simpl; try tauto;
        set (bt' := bt) ; revert bt';
        destruct bt as [bt|];[
          destruct bt;
          match goal with
            | |- let _ := ?T in _ => 
              let typ := (eval compute in (ETYP  T)) in 
              set (prf :=  (@DeclInj.inj_morph_inj typ _ _ _ _)) ; 
                apply prf
          end |
          simpl; tauto] |
        rewrite (Hlist.make_seq_id vls);
          let f  CT OP := (inj_1_cst vls CT OP
                                     || 
                                     (simpl ; tauto)
                         ) in
          forall_types BTYP OPS o f ; simpl ; try tauto].

Ltac nb_arrows TY :=
  match TY with
    | ?A -> ?B => let x := nb_arrows B in
                  constr:(S x)
    | ?A       => constr:O
  end.

Ltac remove_arrows n TY :=
  match n with
    | O => TY
    | S ?n => match TY with
               | ?A -> ?B => remove_arrows n B
               |  ?A      => A
             end
  end.

Ltac list_args TY :=
  match TY with
    | ?A -> ?B => let x := list_args B in
                  constr:(cons (A:Type) x)
    |    _     => constr:(nil: list Type)
  end.


Ltac extract_ex_type TY TRM :=
  let n := nb_arrows TY in
  let ty := type of TRM in
  let ty := remove_arrows (S n) ty in
  list_args ty.


Ltac mk_abstract TRM :=
  match goal with
    | |-  @Abstract.t ?T _ => 
      let ex := extract_ex_type T TRM in
      refine (@Abstract.Mk _ _ _ ex _ _);
        instantiate (1:= TRM);
        intros r args ; rewrite (Hlist.make_seq_id args) ; simpl in * ; repeat elim_car
  end.

Ltac mk_unfold TRM :=
  refine (@Unfold.Mk _ _ _ _ _);
  instantiate (1:= TRM);
  intros args ; rewrite (Hlist.make_seq_id args) ; simpl in * ; repeat elim_car.

Ltac show_goal :=
  match goal with
    | |- ?G => idtac "Goal" G
  end.

Ltac reduce_simplify_abstract_rec T :=
  let T' := (eval hnf in T) in
  let T' := (eval simpl in T) in
  match constr:(T = T') with
    | ?X = ?X => T
    |   _     => reduce_simplify_abstract_rec T'
  end.

Ltac reduce_simplify_abstract T :=
  let T' := (eval unfold simplify_abstract,  full_range,is_var,eq_rect_r in T) in
  let T' := reduce_simplify_abstract_rec T' in
  match constr:(T = T') with
    | ?X = ?X => T
    |   _     => reduce_simplify_abstract T'
  end.




Ltac reify_btyp_list BTYP l :=
  match l with
    | nil => constr: (nil (A:=option BTYP))
    | cons ?A ?L => 
      let l' := reify_btyp_list BTYP L in
      let bt := reify_btyp_option BTYP A in
      constr:(cons bt l')
  end.

Ltac abstract_op BTYP ETYP OPT COP := 
  let PRED := (eval hnf in (@Abstract.pred _ COP _)) in
  idtac "Make.Abstract" PRED;
    let ARGS := constr:(@nil {x: option BTYP & ETYP x}) in
    let tac_fun A F := 
        let e := reify_expr BTYP ETYP OPT A F ARGS in
        let LPRED :=  (eval hnf in (@Abstract.ex_ty _ COP _)) in
        let lbt := reify_btyp_list BTYP LPRED in
        apply Make.Abstract with (l:= lbt) ; 
          intros r args exc; 
          match e with
            | (?A,?E) => 
              let e' := constr:(@simplify_expr _ _ _ (Hlist.scons r  (Hlist.app (Hlist.make_seq args) (Hlist.make_seq exc))) _ E) in 
              (*let e' := constr:(@simplify_expr _ _ _ (Hlist.scons r  args) _ E) in *)
              let e' := reduce_simplify_abstract e' in
              let te := type of e' in
              let te := (eval compute in te) in
              (*                  idtac  e' ":" te;*)
              try exact e'
          end 
    in 
    let TY := type of PRED in
    reify_fun BTYP ETYP tac_fun PRED ARGS TY.

Ltac unfold_op BTYP ETYP OPT COP :=
  let FUN := (eval hnf in (@Unfold.op' _ COP _ )) in
  idtac "Make.Unfold" FUN ; 
    let ARGS := constr:(@nil {x: option BTYP & ETYP x}) in
    let tac_fun A F := 
        let e := reify_expr BTYP ETYP OPT A F ARGS in
        apply Make.Unfold ; 
          intros args ; 
          match e with
            | (?A,?E) => 
              let e' := constr:(@simplify_expr _ _ _ (Hlist.make_seq args) _ E) in 
              let e' := (eval compute in  e') in
              (*                  idtac  e' ":" te;*)
              idtac "UNFOLD_EXACT";
              try exact e' ; fail -1 
          end in
    let TY := type of FUN in
    reify_fun BTYP ETYP tac_fun FUN ARGS TY.



Ltac make_simplify BTYP ETYP OPT OPS :=
  let tac CT OP := 
      let COP := MkName.pre_image CT OP in 
      first [
          unfold_op BTYP ETYP OPT COP   |
          abstract_op BTYP ETYP OPT COP | 
             apply Make.Id] in

  intro o ; forall_types BTYP  OPS o  tac ; apply Make.Id.


Ltac better_exact T :=  
  generalize T ; simpl ; exact (fun X => X).



Ltac my_exact T :=
  (exact T || (idtac "Warning : exact failed" ; better_exact T) ).

Ltac find_ex LTY H S :=
  match LTY with
    | nil => 
      let L := constr:(Hlist.rev S) in
      (exists L)
    | cons ?BT ?BTL => 
      let  x := fresh  in
      destruct H as [x H] ;
        let S := constr:(@Hlist.scons _ _ BT x _ S) in
        find_ex BTL H S
  end.


Ltac make_simplify_correct BTYP OPS := 
  let  tac CT OP := 
       let ROP := MkName.pre_image CT OP in
       first [ 
           exact I 
         |
         (
           unfold Make.simplify_op ;
           intros env args r ; 
           rewrite (Hlist.make_seq_id args) ; 
           idtac "Abstract" OP ROP;
             let PRF := (eval simpl in ((@Abstract.pred_is_thm _ ROP _) (interp env r) (Hlist.make_seq' (Hlist.map (fun bt => interp env) (Hlist.make_seq args))))) in 
             generalize PRF;
             intros Prf Heq;
             apply Prf in Heq;
             match goal with
               | |- @ex (Hlist.t ?ET ?T) _ => 
                 find_ex T Heq (Hlist.snil (A:= option BTYP) (typ := ET))
             end;
             my_exact Heq
         )
         | (
           unfold Make.simplify_op ;
           intros env args ; 
           rewrite (Hlist.make_seq_id args) ; 
           idtac "Unfold" OP ROP;
             let PRF := (eval simpl in ((@Unfold.op_op' _ ROP _)  (Hlist.make_seq' (Hlist.map (fun bt => interp env) (Hlist.make_seq args))))) in 
             my_exact PRF 
         )
         ] in
  intro o;
    destruct o as [o |o]; 
    [(simpl ; exact I) 
    | 
    forall_types BTYP OPS o tac ; try exact I].

Ltac make_simplify_var_correct BTYP OPS :=
  unfold Make.vars_of_pred ;
  let ktac := 
      solve[exact I |
            simpl;
              intro args;
              try (intros res args' ; rewrite (Hlist.make_seq_id args'));
              intros until 0;
              rewrite (Hlist.make_seq_id args) ; 
              simpl;
              repeat rewrite <- app_nil_end;
              repeat rewrite in_app ; try tauto] in

  let tac CT OP := try ktac in
  intro o;
    destruct o as [o |o]; 
    [(simpl ; exact I) | 
     forall_types BTYP OPS o tac ; try ktac
    ].





Ltac make_simplify_morph BTYP OPS :=
  let ktac :=  solve[exact I 
                    | 
                    simpl;
                      intros r args args1 args2 env Heq;
                      rewrite (Hlist.make_seq_id args1) in Heq;
                      rewrite (Hlist.make_seq_id args2) in Heq;
                      apply Hlist.equiv_fix in Heq;
                      unfold block in * ;
                      simpl in  * ; 
                      unfold eq_rect_r in Heq ; simpl in Heq ; intuition congruence
                    ] 
  in

  let tac CT OP := try ktac in
  intro o;
    destruct o as [o |o]; 
    [(simpl ; exact I) | 
     forall_types BTYP OPS o tac ; try ktac
    ].

