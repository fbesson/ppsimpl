Require Import FOLib.
Require Import FOReify.
Require Import ZArith.
Require Import BigZ.
Require Import Utf8.
Require Import List.
Require Import FOCompil.

(** Type instances *)
Require Import InstanceZ.

(** Declared operators *)

(* Constants *)
Instance Tcomparison : DeclOperator.t := @DeclOperator.Mk comparison (Eq :: Lt :: Gt :: nil).

Instance Tpositive : DeclOperator.t := @DeclOperator.Mk positive (xH :: nil).

Instance Tbool : DeclOperator.t := @DeclOperator.Mk bool (true :: false :: nil).

Instance Tnat : DeclOperator.t := @DeclOperator.Mk nat (O :: nil).

Instance Tprop : DeclOperator.t := @DeclOperator.Mk Prop (True :: False :: nil).

Instance TZ  : DeclOperator.t := @DeclOperator.Mk _ (Z0 :: nil).

Instance TN  : DeclOperator.t := @DeclOperator.Mk _ (N0 :: nil).

Instance TbigZ  : DeclOperator.t := @DeclOperator.Mk bigZ (BigZ.zero :: nil).

(* Unary *)

Instance Tnat_nat : DeclOperator.t := @DeclOperator.Mk _ (S :: NPeano.sqrt :: pred :: nil).

Instance Tnat_Z   : DeclOperator.t := @DeclOperator.Mk _ (Z.of_nat :: nil).

Instance Tnat_pos : DeclOperator.t := @DeclOperator.Mk _ (Pos.of_succ_nat :: Pos.of_nat :: nil).

Instance Tpos_nat : DeclOperator.t := @DeclOperator.Mk _ (Pos.to_nat :: nil).

Instance TZ_nat   : DeclOperator.t := @DeclOperator.Mk _ (Z.to_nat :: nil).

Instance TZ_pos   : DeclOperator.t := @DeclOperator.Mk _ (Z.to_pos :: nil).

Instance Tbool_bool : DeclOperator.t := @DeclOperator.Mk _ (negb :: nil).

Instance Tbool_bool_bool : DeclOperator.t := @DeclOperator.Mk _ (andb :: orb :: nil).

Instance Tbool_prop : DeclOperator.t := @DeclOperator.Mk _ (Bool.Is_true :: nil).

Instance Tprop_prop : DeclOperator.t := @DeclOperator.Mk _ (not :: nil).

Instance Tpos_Z : DeclOperator.t := @DeclOperator.Mk _ (Z.pos :: Z.neg :: nil).

Instance Tpos_N : DeclOperator.t := @DeclOperator.Mk _ (N.pos :: nil).

Instance Tpos_pos : DeclOperator.t := @DeclOperator.Mk _ (Pos.succ :: Pos.pred :: xI :: xO :: nil).

Instance TZ_Z : DeclOperator.t := 
  @DeclOperator.Mk _ (Z.opp :: Z.succ :: Z.sqrt :: Z.sgn :: Z.abs :: Zpos_of_nat :: nil ).

Instance TN_N : DeclOperator.t := 
  @DeclOperator.Mk _ (N.succ :: nil ).

Instance TbigZ_Z : DeclOperator.t :=
  @DeclOperator.Mk (bigZ → Z) (BigZ.to_Z :: nil).

Instance TZ_bigZ : DeclOperator.t :=
  @DeclOperator.Mk (Z → bigZ) (BigZ.of_Z :: nil).

Instance TbigZ_bigZ : DeclOperator.t :=
  @DeclOperator.Mk (bigZ → bigZ) (BigZ.opp :: nil).

(* Binary *)
Instance Tnat_nat_nat : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (NPeano.pow :: plus :: minus :: max :: mult :: NPeano.div :: NPeano.modulo :: nil).

Instance TZ_Z_Z : DeclOperator.t := 
  @DeclOperator.Mk _
                   (Z.pow :: Zplus :: Zminus :: Zmult :: Zmax :: Zmin
                          :: Zdiv :: Zmod :: Z.quot :: Z.rem :: nil ).

Instance TN_N_N : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (N.pow :: N.add :: N.sub :: N.mul :: N.div :: nil).

Instance TZ_pos_Z : DeclOperator.t := @DeclOperator.Mk _ (Z.pow_pos :: nil).  

Instance TZ_Z_prop : DeclOperator.t :=  @DeclOperator.Mk _ (Zle :: Zlt :: Zge :: Zgt :: nil).

Instance TZ_Z_bool : DeclOperator.t :=  @DeclOperator.Mk _ (Zeq_bool :: Z.eqb :: Z.leb :: Z.ltb :: Z.geb :: Z.gtb :: nil).

Instance TZ_Z_cmp : DeclOperator.t :=  @DeclOperator.Mk _ (Zcompare :: nil).

Instance Tnat_nat_bool : DeclOperator.t :=  
  @DeclOperator.Mk _ 
                   (beq_nat :: NPeano.leb :: NPeano.ltb :: nil).

Instance Tpos_pos_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (Ple :: Plt :: Pge :: Pgt ::nil).

Instance Tpos_pos_pos : DeclOperator.t :=
  @DeclOperator.Mk _ (Pplus :: Pos.add_carry :: Pminus :: Pmult :: Pmax :: Pos.min :: nil).


Instance Tnat_nat_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (ge :: le :: lt :: gt :: nil).

Instance TN_N_prop : DeclOperator.t :=  @DeclOperator.Mk _ (N.le :: nil).

Instance Tprop_prop_prop : DeclOperator.t := @DeclOperator.Mk _ (or :: nil).

Instance TbigZ_bigZ_prop : DeclOperator.t :=
  @DeclOperator.Mk (bigZ → bigZ → Prop) (BigZ.eq :: BigZ.lt :: nil).

Instance TbigZ_bigZ_bool : DeclOperator.t :=
  @DeclOperator.Mk (bigZ → bigZ → bool) (BigZ.eqb :: BigZ.leb :: BigZ.ltb :: nil).

Instance TbigZ_bigZ_bigZ : DeclOperator.t :=
  @DeclOperator.Mk (bigZ → bigZ → bigZ)
    (BigZ.add :: BigZ.sub :: BigZ.mul :: BigZ.div :: BigZ.min :: BigZ.max :: nil).

Existing Instances ReifyType.MakeProp ReifyType.MakeBtype ReifyType.MakeArrow.





