(** Instanciation of FOSimpl for arithmetic *)
Require Import ZArith.
Require Import List.
Require Import FOLib.
Require Import FOLType.
Require Import FOSimpl.
Set Boolean Equality Schemes.
Set Implicit Arguments.

Module ArithType.

  Inductive typ :=
  | bt_Z | bt_nat | bt_N  | bt_pos.


  Definition dump  (x:typ) : positive :=
    match x with
      | bt_Z    => xO (xO (xO xH))
      | bt_nat  => xO (xO (xI xH))
      | bt_N    => xO (xI (xO xH))
      | bt_pos    => xO (xI (xI xH))
    end.

  Definition parse (p:positive) : typ * positive :=
    match p with
      | xO (xO (xO p)) => (bt_Z,p)
      | xO (xO (xI p)) => (bt_nat,p)
      | xO (xI (xO p)) => (bt_N,p)
      | xO (xI (xI p)) => (bt_pos,p)
      |   _            => (bt_Z,xH)
    end.

  Lemma parse_dump : forall bt p, parse (concat (dump bt) p) = (bt,p).
  Proof.
    destruct bt ; reflexivity.
  Qed.

  Instance parser : Parser.t typ := Parser.Mk  dump parse parse_dump.

  Definition etyp : typ -> Type :=
    fun bt => match bt with
                | bt_Z => Z
                | bt_nat => nat
                | bt_N   => N
                | bt_pos   => positive
              end.


  Lemma typ_beq_sc : forall p1 p2, if typ_beq p1 p2 then p1 = p2 else p1 <> p2.
  Proof.
    induction p1 ; destruct p2 ; simpl ; intuition (try congruence).
  Defined.

  (* For the moment, this is only pure equality *)
  Definition equi_typ (bt : typ) := 
    match bt as bt' return (etyp bt' -> etyp bt' -> Prop) with
      | x => @eq (etyp x)
    end.

  Notation " X == Y" := (equi_typ _ X Y) (at level 90).

  Lemma equi_typ_refl : forall (bt:typ) (x: etyp bt), x == x.
  Proof.
    destruct bt ; reflexivity.
  Qed.

  Lemma equi_typ_sym : forall (bt:typ) (x y: etyp bt), x == y -> y == x.
  Proof.
    destruct bt ; simpl ; intros ; subst ; congruence.
  Qed.

  Lemma equi_typ_trans : forall (bt:typ) (x y z: etyp bt), x == y -> y == z -> x == z.
  Proof.
    destruct bt ; simpl ; intros ; subst ; congruence.
  Qed.
  
  Require Import Setoid.

  Instance is_equi (x:typ) : @Equivalence (etyp x) (@equi_typ x) :=
  {|
  Equivalence_Reflexive := equi_typ_refl x;
  Equivalence_Symmetric := @equi_typ_sym x;
 Equivalence_Transitive := @equi_typ_trans x
 |}.

  Definition norm_typ (bt:typ) := bt_Z.

  Definition norm_cst (bt:typ) : etyp bt -> etyp (norm_typ bt) :=
    match bt with
      | bt_Z => fun x => x
      | bt_nat => Z_of_nat
      | bt_N   => Z_of_N
      | bt_pos => Zpos
    end.

  Definition dtyp (bt:typ) : etyp bt :=
    match bt with
      | bt_Z => Z0
      | bt_nat => O
      | bt_N   => N0
      | bt_pos => xH
    end.


  Instance t : Btyp.t ArithType.typ := Btyp.Mk  parser norm_typ etyp  dtyp typ_beq typ_beq_sc equi_typ is_equi norm_cst.

End ArithType.

Require Import Zmax.
Import ArithType.

Instance pre_arith : Btyp.t typ := ArithType.t.


Instance arith : Btyp.t (BtypProp.btyp typ) := BtypProp.t pre_arith.

Module SName.

Definition btyp := BtypProp.btyp typ.
Definition etyp := @Btyp.etyp _ arith.
Definition equi_typ := @Btyp.equi_typ _ arith.
Definition norm_typ := @Btyp.norm_typ _ arith.

End SName.


Import SName.


Inductive op_u :=
(* Z -> Z -> Z *)
| Z_Zmax | Z_Zplus | Z_Zmult | Z_Zminus | Z_Zdiv | Z_Zmod
(* Z -> Z -> Prop *)
| Z_Zge  | Z_Zle | Z_Zgt | Z_Zeq | Z_Zlt
(* N -> N -> Prop *)
| N_eq 
(* positive -> positive -> Prop *)
| Pos_eq
(* nat -> nat -> nat *)
| nat_plus | nat_minus 
(* nat -> nat *)
| nat_S
(* nat -> nat -> Prop *)
| nat_le | nat_ge | nat_eq
(* Prop -> Prop -> Prop *)
| Prop_impl | Prop_and | Prop_or | Prop_iff.


Notation bt_Prop := (@None ArithType.typ).


Module Spec.

  Record t := 
    Mk{
        args : list btyp ; 
        ret  : btyp ;
        eval : eval_typ etyp args ret
      }.

End Spec.

Definition op_args_u (o:op_u) := 
  match o with
    (* Z -> Z -> Z *)
    | Z_Zmax => Some bt_Z::Some bt_Z::nil
    | Z_Zplus => Some bt_Z::Some bt_Z::nil
    | Z_Zmult => Some bt_Z::Some bt_Z::nil
    | Z_Zminus => Some bt_Z::Some bt_Z::nil
    | Z_Zdiv    => Some bt_Z::Some bt_Z::nil
    | Z_Zmod    => Some bt_Z::Some bt_Z::nil
    (* Z -> Z -> Prop *)
    | Z_Zge  =>  (Some bt_Z::Some bt_Z::nil)
    | Z_Zle  =>  (Some bt_Z::Some bt_Z::nil)
    | Z_Zgt  =>  (Some bt_Z::Some bt_Z::nil)
    | Z_Zlt  =>  (Some bt_Z::Some bt_Z::nil)
    | Z_Zeq  =>  (Some bt_Z::Some bt_Z::nil)
     (* N -> N -> Prop *)
    | N_eq   =>  (Some bt_N::Some bt_N::nil)
      (* positive -> positive -> Prop *)
    | Pos_eq =>  (Some bt_pos::Some bt_pos::nil)
    (* nat -> nat -> nat *)
    | nat_plus =>  (Some bt_nat::Some bt_nat::nil) 
    | nat_minus =>  (Some bt_nat::Some bt_nat::nil)
    (* nat -> nat *)
    | nat_S    =>  (Some bt_nat ::nil) 
   (* nat -> nat -> Prop *)
    | nat_ge  =>  (Some bt_nat::Some bt_nat::nil) 
    | nat_le  =>  (Some bt_nat::Some bt_nat::nil) 
    | nat_eq  =>  (Some bt_nat::Some bt_nat::nil) 
   (* Prop -> Prop -> Prop *)
    | Prop_impl =>  (bt_Prop :: bt_Prop :: nil) 
    | Prop_and  =>  (bt_Prop :: bt_Prop :: nil) 
    | Prop_or  =>  (bt_Prop :: bt_Prop :: nil) 
    | Prop_iff =>  (bt_Prop :: bt_Prop :: nil) 
  end.

Definition op_ret_u (o:op_u) :=
  match o with
    (* Z -> Z -> Z *)
    | Z_Zmax => (Some bt_Z)
    | Z_Zplus => (Some bt_Z)
    | Z_Zmult => (Some bt_Z)
    | Z_Zminus => (Some bt_Z)
    | Z_Zdiv    => (Some bt_Z)
    | Z_Zmod    =>  (Some bt_Z)
    (* Z -> Z -> Prop *)
    | Z_Zge  =>  bt_Prop
    | Z_Zle  => bt_Prop 
    | Z_Zgt  => bt_Prop 
    | Z_Zlt  => bt_Prop 
    | Z_Zeq  => bt_Prop 
     (* N -> N -> Prop *)
    | N_eq   => bt_Prop 
      (* positive -> positive -> Prop *)
    | Pos_eq =>  bt_Prop 
    (* nat -> nat -> nat *)
    | nat_plus =>  (Some bt_nat) 
    | nat_minus =>  (Some bt_nat)
    (* nat -> nat *)
    | nat_S    => (Some bt_nat) 
   (* nat -> nat -> Prop *)
    | nat_ge  =>  bt_Prop 
    | nat_le  =>  bt_Prop
    | nat_eq  =>  bt_Prop
   (* Prop -> Prop -> Prop *)
    | Prop_impl =>  bt_Prop
    | Prop_and  =>  bt_Prop
    | Prop_or  =>  bt_Prop
    | Prop_iff =>  bt_Prop
  end.



Definition op_eval_u (o:op_u) : eval_typ etyp (op_args_u o) (op_ret_u o) :=
  match o with
    (* Z -> Z -> Z *)
    | Z_Zmax =>  Zmax
    | Z_Zplus => Zplus
    | Z_Zmult => Zmult
    | Z_Zminus => Zminus
    | Z_Zdiv    => Zdiv
    | Z_Zmod    => Zmod
    (* Z -> Z -> Prop *)
    | Z_Zge  => Zge
    | Z_Zle  => Zle
    | Z_Zgt  => Zgt
    | Z_Zlt  => Zlt
    | Z_Zeq  => (@eq Z)
     (* N -> N -> Prop *)
    | N_eq   => (@eq N)
      (* positive -> positive -> Prop *)
    | Pos_eq =>  (@eq positive)
    (* nat -> nat -> nat *)
    | nat_plus =>  plus
    | nat_minus => minus
    (* nat -> nat *)
    | nat_S    => S
   (* nat -> nat -> Prop *)
    | nat_ge  => ge
    | nat_le  => le
    | nat_eq  =>  (@eq nat)
   (* Prop -> Prop -> Prop *)
    | Prop_impl => (fun x y => x -> y)
    | Prop_and  => (fun x y => x /\ y)
    | Prop_or  => (fun x y => x \/ y)
    | Prop_iff => iff
  end.


Require Import FOexpr.


Lemma zarith_is_morph : 
   forall o : op_u, is_morph Btyp.etyp Btyp.equi_typ (op_eval_u o).
Proof.
  intros; unfold is_morph ; destruct o; intros; 
  Seq.equiv_tac ; subst ; simpl in *;
  unfold ArithType.equi_typ in * ; subst ; 
  try reflexivity ; intuition.
Qed.

(*
Instance ZArithSpec : FoSpec.t  arith op.
Proof.
  apply (FoSpec.Mk  _ 
            (fun o => Spec.args (eval o))
            (fun o => Spec.ret (eval o))
            (fun o => Spec.eval (eval o))).
  apply zarith_is_morph.
Defined.
*)

Import SName.

Instance syntax_arg : ExprWithProp.Arg.t typ op_u:=
@ExprWithProp.Arg.Mk typ op_u op_args_u op_ret_u.
Import ExprSyntax.


Instance syntax : ExprSyntax.t := 
  @ExprWithProp.MakeSyntax ArithType.typ op_u   syntax_arg ArithType.etyp.

Instance expr_sem : ExprSemantics.t syntax :=
  ExprWithProp.MakeSemantics syntax_arg _ op_eval_u (fun bt => @eq (ArithType.etyp bt)).
Import ExprSemantics.




  Definition norm_u : forall  (o:op_u), Seq.t (Expr  positive) (List.map norm_typ  (ExprSyntax.op_args  (inr o))) -> Expr positive (norm_typ (op_ret (inr o))):=
    fun o => 
    match o with
      | Z_Zmax =>  fun args => Op (inr Z_Zmax) args
      | Z_Zplus =>  fun args => Op (inr Z_Zplus) args
      | Z_Zmult =>  fun args => Op (inr Z_Zmult) args
      | Z_Zminus => fun args => Op (inr Z_Zminus) args
      | Z_Zdiv => fun args => Op (inr Z_Zdiv) args
      | Z_Zmod => fun args => Op (inr Z_Zmod) args
      | Z_Zge    => fun args => Op (inr Z_Zge) args
      | Z_Zle    => fun args => Op (inr Z_Zle) args
      | Z_Zgt    => fun args => Op (inr Z_Zgt) args
      | Z_Zeq    => fun args => Op (inr Z_Zeq) args
      | Z_Zlt    => fun args => Op (inr Z_Zlt) args
      | nat_plus  => fun args => Op (inr Z_Zplus) args
      | nat_minus  => fun args => Op (inr Z_Zmax) (Seq.scons (Cst  (Some bt_Z) Z0) (Seq.scons (Op (inr Z_Zminus) args) Seq.snil))
  (* nat -> nat *)
      | nat_S      => fun args => Op (inr Z_Zplus) (Seq.scons (Cst  (Some bt_Z) (Zpos xH)) args)
 (* nat -> nat -> Prop *)
      | nat_ge     => fun args => Op (inr Z_Zge) args
      | nat_le     => fun args => Op (inr Z_Zle) args
      | nat_eq     => fun args => Op (inr Z_Zeq) args
      | Prop_impl  => fun args => Op (inr Prop_impl) args
      | Prop_and   => fun args => Op (inr Prop_and) args
      | Prop_or    => fun args => Op (inr Prop_or) args
      | Prop_iff   => fun args => Op (inr Prop_iff) args
  (* N -> N -> Prop *)
      | N_eq       => fun args => Op (inr Z_Zeq) args
  (* positive -> positive -> Prop *)
      | Pos_eq     => fun args => Op (inr Z_Zeq) args
    end.

(*  Definition norm_fun (o:op)  : FunExpr _ _  :=
    match o   with
      |  Z_Zmax =>  FunOp Z_Zmax
      | Z_Zplus =>  FunOp Z_Zplus
      | Z_Zmult =>  FunOp Z_Zmult
      | Z_Zminus => FunOp Z_Zminus 
      | Z_Zdiv => FunOp Z_Zdiv 
      | Z_Zmod => FunOp Z_Zmod 
      | Z_Zge    => FunOp Z_Zge
      | Z_Zle    => FunOp Z_Zle
      | Z_Zgt    => FunOp Z_Zgt
      | Z_Zeq    => FunOp Z_Zeq
      | Z_Zlt    => FunOp Z_Zlt
      | nat_plus  => FunOp Z_Zplus
      | nat_minus  => FunList   Z_Zmax  ( (FunCst (Some bt_Z) Z0) :: (FunOp Z_Zminus) :: nil)
  (* nat -> nat *)
      | nat_S      => FunList Z_Zplus   ((FunCst (Some bt_Z) (Zpos xH)) :: (FunArg  O) :: nil)
 (* nat -> nat -> Prop *)
      | nat_ge     => FunOp Z_Zge
      | nat_le     => FunOp Z_Zle
      | nat_eq     => FunOp Z_Zeq
      | Prop_impl  => FunOp Prop_impl
      | Prop_and   => FunOp Prop_and
      | Prop_or    => FunOp Prop_or
      | Prop_iff   => FunOp Prop_iff
  (* N -> N -> Prop *)
      | N_eq       => FunOp Z_Zeq
  (* positive -> positive -> Prop *)
      | Pos_eq     => FunOp Z_Zeq
    end.
*)


  Definition type_cstr (bt : btyp) (x:positive) : option (Expr positive bt_Prop) :=
    match bt with
      | Some bt_nat => Some (Op (inr nat_le) (Seq.scons (Cst (Some bt_nat) O) (Seq.scons (Var (Some bt_nat) x) Seq.snil)))
      | Some bt_N =>  None
      | _      => None
    end.

  Lemma norm_vars : 
    forall (o : op)
           (s : Seq.t (Expr positive) (List.map Btyp.norm_typ (op_args o)))
           (v : positive),
      In v (vars (Make.norm norm_u o s)) ->
      In v (Seq.concat_map_list (vars (var:=positive)) s).
  Proof.
    destruct o ; simpl; auto.
    * destruct o ; simpl ; auto.
    * destruct o ; simpl ; auto;
    intros; rewrite <- app_nil_end in H; auto.
  Qed.

  Lemma arith_norm_cst : 
   forall (env : Make.Env pre_arith syntax_arg positive) 
     (o : op) (vls : Seq.t etyp (op_args o)),
   Btyp.equi_typ (Btyp.norm_typ (op_ret o))
     (Btyp.norm_cst (op_ret o) (interp_seq vls (op_eval o)))
     (interp env
        (Make.norm norm_u o
           (Seq.mapt (Make.norm_Cst pre_arith syntax_arg positive) vls))).
    Proof.
      intros; destruct o.
      *
        destruct o ; simpl in * ; intros; Seq.dest_seq ; Seq.dest_seq_nil ; simpl ; try tauto.
        destruct bt ; simpl ; intuition try congruence.
        apply Nat2Z.inj ; auto.
        apply N2Z.inj ; auto.
      * 
        destruct o ; intros; Seq.dest_seq ; Seq.dest_seq_nil ; try reflexivity.
        - 
          simpl.
          split. congruence.
          apply N2Z.inj.
        - simpl.
          intuition congruence.
        - simpl.
          exact (inj_plus _ _).
        -  exact (inj_minus _ _).
        - 
          assert (HH :   Z_of_nat (S H0) = (1 + Z_of_nat H0)%Z).
          rewrite inj_S. rewrite Zplus_comm. reflexivity.
          exact HH.
        - split ; [exact (inj_le _ _) | exact (inj_le_rev _ _)].
        - split ; [exact (inj_ge _ _)| exact (inj_ge_rev _ _)].
        - split ; [ congruence | exact (inj_eq_rev _ _)].
    Qed.
      
    Ltac interp_morph := 
      repeat 
        match goal with
          | H : ?X == ?Y |- _ => rewrite H in * ; clear H
        end.

    Lemma norm_morph : 
   forall (env : Make.Env pre_arith syntax_arg positive) 
     (o : op) (l l' : Seq.t (Expr positive) (List.map Btyp.norm_typ (op_args o))),
   Seq.equiv
     (fun (bt : btyp) (e1 e2 : Expr positive bt) =>
      Btyp.equi_typ bt (interp env e1) (interp env e2)) l l' ->
   Btyp.equi_typ (Btyp.norm_typ (op_ret o))
     (interp env (Make.norm norm_u o l)) (interp env (Make.norm norm_u o l')).
    Proof.
      intros; destruct o.
      *
        destruct o ; simpl in * ; intros; Seq.equiv_tac ; subst ; simpl in * ; try tauto.
        rewrite H_r_r_r_r_r_l in *.
        rewrite H_r_r_l in *. tauto.
      * 
        destruct o ; simpl in * ; intros; Seq.equiv_tac ; subst ; simpl in * ; interp_morph ; try reflexivity; try tauto.
    Qed.

    
    Lemma type_spec :
      forall (bt : btyp) (v : positive) (e : Expr positive bt_Prop)
             (env : Env (Make.expr_synt pre_arith syntax_arg) positive),
        type_cstr bt v = Some e -> interp env e.
    Proof.
      destruct bt as [bt | ]. 
      destruct bt ; simpl ; try discriminate; intros until 0 ; 
      intros H ; inv H ; simpl.
      mauto. discriminate.
    Qed.


  Lemma arith_norm : 
    Make.spec norm_u type_cstr op_eval_u.
  Proof.
    constructor.
    apply norm_vars.
    apply arith_norm_cst.
    apply norm_morph.
    apply type_spec.
  Qed.


Definition binop (var : Type) (bt : btyp) (e1: Expr var bt) (e2 : Expr var bt) : Seq.t (Expr var) (bt::bt::nil) :=
  Seq.scons e1 (Seq.scons e2 Seq.snil).


Definition abstract_Z_Zmax:  Seq.t (Expr positive) (op_args (inr Z_Zmax)) ->  Expr positive (op_ret (inr Z_Zmax)) -> Expr positive bt_Prop :=
  fun args r => 
    let '(exist (e1,rargs) prf1) := Seq.car_cdr args in
      let '(exist (e2,_) prf2) := Seq.car_cdr rargs in
        Op (inr Prop_or) (binop 
          (Op (inr Prop_and) (binop (Op (inr Z_Zge) args) (Op (inr Z_Zeq) (binop r e1))))
          (Op (inr Prop_and) (binop (Op (inr Z_Zlt) args) (Op (inr Z_Zeq) (binop r e2))))).


(*Ltac norm_tac :=
  change arith_fo_spec with (@fo_spec ArithType.t arithSpec);
    change (@FoSpec.btyp (@fo_spec ArithType.t arithSpec)) with (option typ).
*)
Check Make.simp_rule.

  Definition simplify_op  (o:op) : Make.simp_rule   o :=
    match o as o' return  Make.simp_rule   o' with
      | inr Z_Zmax => Make.Abstract (inr Z_Zmax) (@abstract_Z_Zmax )
      | o'     => Make.Id    o'
    end.

(*  Definition binop_fun (l:list btyp) (r1 r2 : btyp) (arg1 : FunExpr) (arg2 : FunExpr l  r2) : Seq.t (FunExpr l) (r1::r2::nil) :=
    Seq.scons arg1 (Seq.scons arg2 Seq.snil).
*)




(*  Definition simplify_op_fun  (o:op) : @simp_rule_fun pre_arith arithSpec  o :=
    match o as o' return @simp_rule_fun pre_arith arithSpec  o' with
      |  Z_Zmax =>  AbstractFun  Z_Zmax (abstract_Z_Zmax_fun )
      |  o'     =>  NoFun _ _   
    end.
*)
(*
Lemma simplify_1 : 
   forall o : op,
   match simplify_op o with
   | Unfold f =>
       forall (env : FOexpr.Env (btyp_spec pre_arith) positive)
         (args : Seq.t (Expr positive) (op_args o)),
       Btyp.equi_typ (op_ret o) (interp env (f args))
         (interp env (Op o args))
   | Abstract p =>
       (forall (env : FOexpr.Env (btyp_spec pre_arith) positive)
          (args : Seq.t (Expr positive) (op_args o))
          (r : Expr positive (op_ret o)),
        Btyp.equi_typ (op_ret o) (interp env (Op o args)) (interp env r) ->
        interp env (p args r)) /\
       (forall
          args
           args' : Seq.t (fun bt : Expr.btyp typ => Btyp.etyp bt) (op_args o),
        Seq.equiv
          (fun (bt : Expr.btyp typ) (e1 e2 : Btyp.etyp bt) =>
           Btyp.equi_typ bt e1 e2) args args' ->
        Btyp.equi_typ (op_ret o) (interp_seq args (op_eval o))
          (interp_seq args' (op_eval o)))
   | Id => True
   end.
Proof.
  destruct o ; constructor ; intros; simpl in *.
  *
    Seq.dest_seq.  
    simpl in *.
    rewrite (Seq.nil_snil H4) in *.
    simpl in H.
    rewrite <- H.
    refine (Zmax_spec _ _).
  *  simpl in *.
     equiv_tac.
     subst.
     rewrite H_r_r_r_r_r_l.
     rewrite H_r_r_l.
     reflexivity.
Qed.

Lemma simplify_vars : 
   forall o : op,
   match simplify_op o with
   | Unfold f =>
       forall (args : Seq.t (Expr positive) (op_args o)) (x : positive),
       In x (vars (Expr.btyp typ) op (btyp_spec pre_arith) (f args)) <->
       In x
         (Seq.concat_map_list
            (vars (Expr.btyp typ) op (btyp_spec pre_arith) (var:=positive))
            args)
   | Abstract p =>
       forall (args : Seq.t (Expr positive) (op_args o))
         (res : Expr positive (op_ret o)) (x : positive),
       In x (vars (option typ) op (btyp_spec pre_arith) (p args res)) <->
       In x (vars (Expr.btyp typ) op (btyp_spec pre_arith) (Op o args)) \/
       In x (vars (Expr.btyp typ) op (btyp_spec pre_arith) res)
   | Id => True
   end.
Proof.
  destruct o ; simpl ; auto.
  unfold abstract_Z_Zmax.
  intros.
  Seq.dest_seq.
  simpl.
  repeat rewrite <- app_nil_end. in_app. tauto.  
Qed.

*)

Lemma simplify_1 : 
   forall o : op,
   match simplify_op o with
   | Make.Unfold f =>
       forall (env : Env (Make.expr_synt pre_arith syntax_arg) positive)
         (args : Seq.t (Expr positive) (op_args o)),
       Btyp.equi_typ (op_ret o) (interp env (f args))
         (interp env (Op o args))
   | Make.Abstract p =>
       (forall (env : Env (Make.expr_synt pre_arith syntax_arg) positive)
          (args : Seq.t (Expr positive) (op_args o))
          (r : Expr positive (op_ret o)),
        Btyp.equi_typ (op_ret o) (interp env (Op o args)) (interp env r) ->
        interp env (p args r))
   | Make.Id => True
   end.
Proof.
  destruct o ; simpl ; auto.
  *
    destruct o ; simpl ; auto.
    + intros.
      unfold abstract_Z_Zmax.
      Seq.dest_seq.  
      simpl in *.
      rewrite <- H.
      refine (Zmax_spec _ _).
Qed.

 Lemma simplify_vars : forall o : op,
 match simplify_op o with
 | Make.Unfold f =>
     forall (args : Seq.t (Expr positive) (op_args o)) (x : positive),
     In x (vars (f args)) <->
     In x (Seq.concat_map_list (vars (var:=positive)) args)
 | Make.Abstract p =>
     forall (args : Seq.t (Expr positive) (op_args o))
       (res : Expr positive (op_ret o)) (x : positive),
     In x (vars (p args res)) <-> In x (vars (Op o args)) \/ In x (vars res)
 | Make.Id => True
 end.
Proof.
  destruct o ; simpl ; auto.
  destruct o ; simpl ; auto.
  unfold abstract_Z_Zmax.
  intros.
  Seq.dest_seq.
  simpl.
  repeat rewrite <- app_nil_end. in_app. tauto.  
Qed.



Definition arith_simpl : Make.Simpl _ syntax_arg op_eval_u.
Proof.
  apply Make.MkS with (simplify_op := simplify_op).
  * apply simplify_1.
  * apply simplify_vars.
Defined.

(*Extraction  "simplify.ml" simplify.*)


(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
