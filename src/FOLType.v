Require Import ZArith.
Require Import FOLib.
Require Import Hlist.
Require Import Setoid.
Set Implicit Arguments.

Section EvalType.
  Variable btyp : Type.
  Variable etyp : btyp -> Type.

  Fixpoint eval_typ (l:list btyp) (r:btyp) : Type :=
    match l with
      | nil => etyp r
      | cons e l => etyp e -> (eval_typ l r)
    end.

  Variable equi_typ : forall (x:btyp), etyp x -> etyp x -> Prop.

  Notation " X == Y" := (@equi_typ _ X Y) (at level 90).

  Fixpoint interp_seq (l : list btyp) (args : Hlist.t  etyp l) (r:btyp): eval_typ  l r -> etyp r :=
    match args  with
      | Hlist.snil => fun eval => eval
      | Hlist.scons x0 e lx le' =>
        fun eval => interp_seq  le' r (eval e)
    end.

  Lemma interp_seq_rew : forall (l : list btyp) (args : Hlist.t  etyp l) (r:btyp) f,
    @interp_seq l args r f = 
    match args in (Hlist.t _  l1) return (eval_typ  l1 r -> etyp r) with
      | Hlist.snil => fun eval => eval
      | Hlist.scons x0 e lx le' =>
        fun eval => interp_seq  le' r (eval e)
    end f.
  Proof.
    destruct args ; reflexivity.
  Qed.


  Definition is_morph (r:btyp) (l:list btyp) (f: eval_typ  l r) :=
    forall (args args': Hlist.t etyp l),
      Hlist.equiv  (fun bt (e1 e2: etyp bt) => @equi_typ _ e1 e2)  args args' -> 
      interp_seq args r f == interp_seq args' r f.


    Lemma interp_seq_morph : forall lt (args args':Hlist.t etyp lt) r f, 
      is_morph r lt f -> 
      Hlist.equiv (fun bt (e1 e2:etyp bt) => e1 == e2) args args' -> 
      interp_seq args r f == interp_seq  args' r f.
    Proof.
      intros.
      apply H; auto.
    Qed.



End EvalType.
Implicit Arguments is_morph [btyp r l].


Module Btyp.

    Definition etyp_prop (typ : Type) (etyp : typ -> Type) (x: option typ) : Type :=
      match x with
        | None => Prop
        | Some bt => etyp bt
      end.


  Class t (typ:Type) := Mk {
    parser : Parser.t typ ;  (* Parser *)
    inj_typ : typ -> option typ ; 
    etyp : typ -> Type ; (* Interpretation of [typ] *)
    dtyp : forall (bt:typ), etyp bt;
    t_beq : typ -> typ -> bool ; (* Boolean equality test *)
    t_beq_sc : forall t1 t2, if t_beq t1 t2 then t1 = t2 else t1 <> t2 ; (* Correctness of boolean equality *)
    equi_typ  : forall (bt:typ), etyp bt -> etyp bt -> Prop ; (* An equivalence relation for each typ *)
    is_equi  : forall (bt:typ), @Equivalence (etyp bt) (@equi_typ  bt);
    inj_cst : forall (bt : typ) , etyp bt -> etyp_prop etyp (inj_typ bt)
  }.

End Btyp.

Module BtypProp.

  Class t (typ:Type) := Mk {
    parser : Parser.t (option typ) ;  (* Parser *)
    inj_typ : option typ -> option typ ; 
    etyp : option typ -> Type ; (* Interpretation of [typ] *)
    dtyp : forall (bt:option typ), etyp  bt;
    t_beq : option typ -> option typ -> bool ; (* Boolean equality test *)
    t_beq_sc : forall t1 t2, if t_beq t1 t2 then t1 = t2 else t1 <> t2 ; (* Correctness of boolean equality *)
    equi_typ  : forall (bt:option typ), etyp bt -> etyp bt -> Prop ; (* An equivalence relation for each typ *)
    is_equi  : forall (bt:option typ), @Equivalence (etyp bt) (@equi_typ  bt);
    inj_cst : forall (bt : option typ) , etyp bt -> etyp (inj_typ bt)
  }.

End BtypProp.

Section S.
  
  Variable btyp_u : Type.
  Variable btyp_u_spec : Btyp.t btyp_u.
    
    Definition btyp := (option btyp_u).

    Definition parser := OptionParser.t Btyp.parser.

    Definition inj_typ (bt:btyp) := 
      match bt with 
        | None => None
        | Some bt => (Btyp.inj_typ bt)
      end.

    Definition etyp (bt : btyp) : Type :=
      match bt with
        | None => Prop
        | Some bt => Btyp.etyp bt
      end.

    Definition option_beq (A:Type) (f : A -> A -> bool) (o o': option A) :=
      match o , o' with
        | None , None => true
        | Some e1 , Some e2 => f e1 e2
        |  _ , _   => false
      end.
    
    Definition t_beq : btyp -> btyp ->  bool := option_beq Btyp.t_beq.

    Lemma t_beq_sc : forall t1 t2, if t_beq t1 t2 then t1 = t2 else t1 <> t2.
    Proof.
      destruct t1 as [t1|]; destruct t2 as [t2|] ; simpl ; try congruence.
      generalize (Btyp.t_beq_sc t1 t2).
      elim (Btyp.t_beq t1 t2) ; congruence.
    Defined.

    Definition equi_typ (bt:btyp) : etyp bt -> etyp bt -> Prop :=
      match bt with
        | None => iff
        | Some bt => Btyp.equi_typ bt
      end.

    Instance is_equi (bt:btyp) :  @Equivalence (etyp bt) (@equi_typ bt).
    Proof.
      intros.
      destruct bt ; simpl.
      apply Btyp.is_equi.
      constructor; 
        unfold Reflexive, Symmetric, Transitive ; 
          intros ; tauto.
    Qed.

    Definition inj_cst (bt:btyp) : etyp bt -> etyp (inj_typ bt) :=
      match bt with
        | None => fun v => v
        | Some bt => Btyp.inj_cst bt 
      end.
    
    Definition dtyp (bt : btyp) : etyp bt :=
      match bt with
        | None => True
        | Some bt => Btyp.dtyp bt 
      end.

    Instance Make  : BtypProp.t btyp_u := BtypProp.Mk       parser inj_typ _  dtyp t_beq t_beq_sc
      equi_typ is_equi inj_cst.

    End S.

