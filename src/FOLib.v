(** A basic set of tactics. *)
Require Import ZArith.
Require Import List.
Set Implicit Arguments.

Ltac inv H := inversion H ; try subst ; try clear H.
Ltac add_eq expr val := set (temp := expr) ; generalize (refl_equal temp) ; unfold temp at 1 ; generalize temp ; intro val ; clear temp.
Ltac add_weak_eq expr val := 
  set (temp := expr) ; generalize (refl_equal temp) ; unfold temp at 1 ; intro tmp ; unfold temp ; revert tmp ; generalize temp ; intro val ; clear temp.

Definition block (A:Type) (x:A) : A :=  x.

Arguments FOLib.block _ x : simpl never.


Ltac dest_match :=
  match goal with
    | |- context [match ?X with
                    | left _ => _
                    | right _ => _
                  end ]  => destruct X
    | |- context[match ?X with pair _ _ =>_ end] => destruct X
  end.

Ltac case_eq_match :=
  match goal with
    | |- context [match ?X with
                    | left _ => _
                    | right _ => _
                  end ]  => case_eq X
    | |- context[match ?X with pair _ _ =>_ end] => case_eq X
  end.



Ltac sapply X := 
  first [refine X 
    | refine (X _ ) 
    | refine (X _ _) 
    | refine (X _ _ _) 
    | refine (X _ _ _ _)
    | refine (X _ _ _ _ _)  
    | refine (X _ _ _ _ _ _)  
].


(** 
   A [Case] tactic similar to Pierce's implementation with  the improvements that:
   * The goal is not polluted with proof terms
   * The proof level is an argument
   * Error messages 
**)

Module CaseT.
  Inductive t (n:nat) : Type := H.
End CaseT.
Notation Case := CaseT.t.

Ltac move_to_top x :=
  match reverse goal with
  | H : _ |- _ => try move x after H
  end.

Ltac lt_case X N M :=
  let t := constr:(leb (S N) M) in
  match eval compute in t with
    | true => idtac
    | false => fail -1 ": case [" X "] is at higher level [" N "]"
  end.

Tactic Notation "assert_error" ident(x) constr(level) :=
  first [
    assert (x:= CaseT.H level) ; move_to_top x 
    | fail -1 ": case[" x "] already exists"].


Tactic Notation "Case_level" ident(x) constr(level) :=
  match reverse goal with
    | H : Case level |- _ => 
      match H with 
        | x => idtac (* The occur check succeeds *)
        | _ => fail -1 ": case [" H "] is already  at level [" level "]" 
      end
    | H : Case ?N |- _ =>  
      match H with 
        | x => fail -1 ": case [" x "] is already at level [" N "]" 
        | _ => lt_case H N level ; assert_error x level
      end
    |             |- _ =>  assert_error x  level
  end.


(** [Case x n] adds the hypothesis x : Case n.
   It fails if there is already a case [y : Case m] such that
   either n <= m
   or n = m /\ x <> y.
   (It also fails if [x] is an existing hypothesis.)
*)

Tactic Notation "Case" ident(x) constr(level):=
  let t := type of level in 
    lazymatch t with
      | nat => Case_level x level
      | Z  => 
        let level := eval compute in (Zabs_nat level) in
          Case_level x level
      | _  => fail -1 "argument [" level "] should be an integer"
    end.

Tactic Notation "Case" ident(x) := Case x 0.

(** [Ecase x by tac] solves current goal tagged with [x : Case n] for some n by tactic [tac].
   NB: There migth still be sibling goals tagged with [x : Case n] *)
Tactic Notation "ECase" ident(x) "by" tactic(tac) :=
  match reverse goal with
    | H : Case ?N |- _ => 
      match H with
        | x => try tac ; fail -1 ": case [" x N "] is still open"
      end
    |   |- _    => fail 2 ": case [" x "] does not exist"
  end.

(* [Ecase x] checks that the current goal is not tagged with [x : Case n] for some n *)
Tactic Notation "ECase" ident(x) :=
  match reverse goal with
    | H : Case ?N |- _ => 
      match H with
        | x => fail 2 ": case [" x N "] is still open"
      end
    |    _    => (* none of the cases is [x] *) idtac
  end.

(** [flatten] eliminates  conjunctions (and existentials) in hypotheses *)

Ltac  flatten :=
  repeat 
    match goal with
      | H : ?A /\ ?B |- _ => 
        let h1 := fresh H "_l" in 
          let h2 := fresh H "_r" in 
            destruct H as [h1 h2]
      | H : @ex ?T ?P |- _ =>  
        elim H ; intro ; clear H ; intro  H
    end.

(** flip *)
Lemma flip : forall A B, A /\ B -> B /\ A.
Proof.
  tauto.
Qed.

Ltac flip := apply flip.


(** [fo] is an attempts at doing a little bit of first-order reasoning *)


Definition tagged (x:Type) := x.

Ltac saturate :=
  repeat 
    (match goal with
      | V : ?T ,  H' : ?U |- _ => 
        match T with
          | tagged _ => fail 1
          |    _      => 
            let h := fresh in 
              assert (h := H' V) ; change T with (tagged T) in V
        end
    end) ; unfold tagged in *.



Ltac pre_fo tac :=
  try tac ; flatten ; 
  repeat 
    (match goal with 
      | H : ?A , H' : ?A -> ?B |- _ => apply H' in H ; try tac ; flatten
      | V : ?T ,  H' : ?U |- _ => 
        match T with
          | tagged _ => fail 1
          |    _      => 
            let h := fresh in 
              assert (h := H' V) ; change T with (tagged T) in V
        end
    end) 
    ; try (unfold tagged in * ; tauto) ; 
      match goal with
        | H : ?A |- ?G => apply H ; pre_fo tac
      end.



Tactic Notation "fo"  := pre_fo idtac.
Tactic Notation "fo" tactic(tac) := pre_fo tac.

(** [mauto] does a minimal zify *)
Ltac mauto :=
  repeat flatten ; 
    repeat 
      match goal with
      (* positive -> Z *)
        | H :context[Plt ?X ?Y] |- _ => change (Plt X Y) with  (Zlt (Zpos X) (Zpos Y)) in H
      | H :context[Ple ?X ?Y] |- _ => change (Ple X Y) with (Zle (Zpos X) (Zpos Y)) in H
        | |- context[Plt ?X ?Y] => change (Plt X Y) with (Zlt (Zpos X) (Zpos Y))
      | |- context[Ple ?X ?Y] => change (Ple X Y) with (Zle (Zpos X) (Zpos Y))
      | |- context[Pcompare ?X ?Y Eq = Lt] => change (Pcompare X Y Eq = Lt) with (Zlt (Zpos X) (Zpos Y))
      | |- context[Pcompare ?X ?Y Eq = Gt] => change (Pcompare X Y Eq = Gt) with (Zgt (Zpos X) (Zpos Y))
      | |- context[Pcompare ?X ?Y Eq = Eq] => rewrite (Pcompare_eq_iff X Y)
      | |- context[Zpos (Psucc ?X)] => rewrite (Zpos_succ_morphism X)
      | H : context[Zpos (Psucc ?X)] |- _ => rewrite (Zpos_succ_morphism X) in H
      (* inversion of equalities between positive *)
      | H : (?X~0)%positive = (?Y~0)%positive |- _ => inv H
      | H : (?X~1)%positive = (?Y~1)%positive |- _ => inv H
      | H : (?X~0)%positive = (?Y~1)%positive |- _ => discriminate  H
      | H : (?X~1)%positive = (?Y~0)%positive |- _ => discriminate  H
      | H : xH = (?Y~0)%positive |- _ => discriminate  H
      | H : xH = (?Y~1)%positive |- _ => discriminate  H
      | H : (?Y~0)%positive = xH |- _ => discriminate  H
      | H : (?Y~1)%positive = xH |- _ => discriminate  H
      (* inversion of pairs *)
      | H :  (?X1,?Y1) = (?X2,?Y2)  |- _ => inv H
      (* inversion of existT *)
    end ; auto with zarith.

(** A type-class for generating decidable tests. *)
Module EqTest.

  Class t (T:Type)  : Type := Mk {
    eqb : T -> T -> bool;
    eqb_sc : forall (t1 t2 : T), if eqb t1 t2 then t1 = t2 else t1 <> t2
  }.


  Lemma mk_eqb_true : forall {T:Type} {e:EqTest.t T}, forall p p' : T, EqTest.eqb p p' = true -> p = p'.
  Proof.
    intros.
    generalize (EqTest.eqb_sc p p').
    rewrite H.
    auto.
  Defined.
  
  Lemma mk_eqb_false : forall {T:Type} {e:EqTest.t T}, forall p p' : T, EqTest.eqb p p' = false -> p <> p'.
  Proof.
    intros.
    generalize (EqTest.eqb_sc p p').
    rewrite H.
    auto.
  Defined.

  Lemma eq_dec : forall {T:Type} {e:EqTest.t T}, forall p p' : T, {p = p'} + {p <> p'}.
  Proof.
    refine (fun T e p p' => 
      let b := EqTest.eqb  p p' in
        match b as  b' return b = b' -> {p = p'} + {p <> p'} with
          | true =>  fun H => left _ _ 
          | false => fun H => right _ _ 
        end (refl_equal b)).
    apply (mk_eqb_true ). apply H.
    apply (mk_eqb_false). apply H.
  Defined.

End  EqTest.


Lemma positive_beq_sc : forall p1 p2, if Peqb p1 p2 then p1 = p2 else p1 <> p2.
Proof.
  intros.
  generalize (Peqb_eq p1 p2).
  destruct (Peqb p1 p2) ; intuition congruence.
Defined.

Instance positive_beq_test : EqTest.t positive  := EqTest.Mk  Peqb positive_beq_sc.

Definition option_eqb (A:Type) (f : A -> A -> bool) (x x' : option A) : bool :=
  match x , x' with
    | None , None => true
    | Some x , Some x' => f x x'
    |  _     , _       => false
  end.

Instance option_beq_test (A : Type) {eq : EqTest.t A} : EqTest.t (option A).
Proof.
  apply (EqTest.Mk (option_eqb EqTest.eqb)).
  intros.
  destruct eq.
  destruct t1 ; destruct t2 ; simpl ; try congruence.
  generalize (eqb_sc a a0).
  destruct (eqb a a0); congruence.
Defined.

Lemma list_eq_dec :
  forall (A:Type) (eq_dec : forall (x y:A), {x = y}+{x<>y}) (l l':list A) , {l = l'} + {l <> l'}.
Proof.
  induction l as [| x l IHl]; destruct l' as [| y l'].
  left; trivial.
  right; apply nil_cons.
    right; unfold not; intro HF; apply (nil_cons (sym_eq HF)).
    destruct (eq_dec x y) as [xeqy|xneqy]; destruct (IHl l') as [leql'|lneql'];
      try (right; unfold not; intro HF; injection HF; intros; contradiction).
    rewrite xeqy; rewrite leql'; left; trivial.
  Defined.



Lemma in_app : forall (A:Type) (l m : list A) (x:A),
  In x (l ++ m) <-> In x l \/ In x m.
Proof.
  intros.
  split; intro ; [apply in_app_or in H | apply in_or_app] ; tauto.
Qed.

Ltac in_app :=
  repeat 
    match goal with
      | |- context[In ?X (app ?U ?V)] => rewrite (in_app U V X)
      | H : context[In ?X (app ?U ?V)] |- _ => rewrite (in_app U V X) in H
    end.



Fixpoint concat (p p':positive) : positive :=
  match p with
    | xH => p'
    | xO p => xO (concat p p')
    | xI p => xI (concat p p')
  end.

Lemma concat_assoc : forall p1 p2 p3, concat (concat p1 p2) p3 = concat p1 (concat p2 p3).
Proof.
  induction p1 ; simpl ; intros ; auto ; rewrite IHp1 ; reflexivity.
Qed.
  


(** Parser combinators *)

Module Parser.
  (* To parse/dump data-structures *)
  Class t (T:Type) : Type := Mk {
    dump : T -> positive ;
    parse : positive -> T * positive ; 
    prf   : forall bt p, parse (concat (dump bt) p) = (bt,p)
  }.

End Parser.


Module PairParser.
  Section S.
 
    Variable A : Type.
    Variable Pa : Parser.t A.
    
    Variable B : Type.
    Variable Pb : Parser.t B.
    
    Definition dump (x : A * B) : positive :=
      let (l,r) := x in
        concat (Parser.dump l) (Parser.dump r).

    
    Definition parse  (p:positive) : A * B * positive :=
      let (l,p) := Parser.parse p in
        let (r,p) := Parser.parse p in
          (l,r,p).

    Lemma prf : forall lr p, parse (concat (dump lr) p) = (lr,p).
    Proof.
      unfold parse.
      destruct lr ; simpl.
      intros.
      case_eq (@Parser.parse A Pa (concat (concat (@Parser.dump A Pa a) (@Parser.dump B Pb b)) p)).
      intros.
      case_eq (@Parser.parse B Pb p0).
      intros.
      rewrite concat_assoc in H.
      unfold Parser.dump, Parser.parse in *.
      destruct Pa ; destruct Pb.
      rewrite prf in H.
      inv H.
      rewrite prf0 in H0.
      congruence.
    Qed.
  
  End S. 

  Definition mk (A:Type) (Pa:Parser.t A) (B:Type) (Pb : Parser.t B): Parser.t (A * B) :=
    Parser.Mk (dump Pa Pb) (parse Pa Pb) (prf  Pa Pb).

  Instance t (A:Type) (Pa : Parser.t A) (B:Type) (Pb : Parser.t B) : Parser.t (A * B) := @mk A Pa B Pb.

End PairParser.

Module OptionParser.

  Section S.
    Variable T : Type.
    Variable P : Parser.t T.

    Definition dump  (x : option T) : positive :=
      match x with
        | None => xO xH
        | Some x => xI (Parser.dump x)
      end.
    
    Definition parse  (p:positive) : option T * positive :=
      match p with
        | xO p => (None,p)
        | xI p => let (e,p) := Parser.parse p in
          (Some e,p)
        | xH   => (None,p)
      end.

    Lemma prf : forall bt p, parse (concat (dump bt) p) = (bt,p).
    Proof.
      destruct bt ; simpl.
      intros. rewrite Parser.prf. reflexivity.
      reflexivity.
    Qed.
  
  End S. 

  Definition mk (T:Type) (P:Parser.t T) : Parser.t (option T) :=
    Parser.Mk (dump  P) (parse  P) (prf  P).

  Instance t (T:Type) (P : Parser.t T) : Parser.t (option T) := @mk T P.

End OptionParser.


(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
