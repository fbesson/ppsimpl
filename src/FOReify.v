(** Type classes for declaring types and operators *)
Require Import Setoid.
Require Import List.
Require Import FOLType.
Require Import FOLib.
Require Import Psatz.
Require Import Utf8.
Set Boolean Equality Schemes.


Module TypeDecl.
  (** The properties needed for a type.  *)

  Class t (typ:Type) : Type :=
    Mk {
        dtyp         : typ;
        equi         : typ -> typ -> Prop;
        type_cstr    : option (typ -> Prop);
        is_equi      : Equivalence equi;
        is_type_cstr : ∀ p,  type_cstr = Some p -> forall t, p t
      }.

  Arguments Mk {typ} dtyp equi type_cstr [is_equi] [is_type_cstr].

End TypeDecl.

Instance PropType : @TypeDecl.t Prop.
Proof.
  apply TypeDecl.Mk with (equi := iff) (type_cstr := None).
  * exact True.
  * typeclasses eauto.
  * intros ; auto.
    discriminate.
Defined.

Module DeclInj.
  (** One role of the tactic is to normalise formulae 
      by mapping e.g., numeric types such as nat, N, positive to Z.
   *)

  Class t (ty : Type)  {TY : TypeDecl.t ty} (ty': Type) {TY' : TypeDecl.t ty'} : Type := 
    Mk {
        inj : ty -> ty';
        inj_morph_inj : forall (x y:ty), TypeDecl.equi x y <-> TypeDecl.equi (inj x) (inj y)
      }.

  Arguments Mk ty {TY} ty' {TY'} inj inj_morph_inj.

End DeclInj.

Require Import FOSimpl.

Module DeclOperator.
  (** For a given type ty, 
      the list of operators needs to be declared. *)

  Class t {ty : Type} : Type :=
    Mk { operators : list ty }.

End DeclOperator.

Module BuildLem.

  Class t (ty : Type) (ty':Type): Type :=
    Mk {

        lty  : list Type ;
        rty  : Type ; 

        lty' : list Type ; 
        rty' : Type ; 

        inj_lty : Hlist.t (fun x => x) lty -> Hlist.t (fun x => x) lty' ; 
        inj_rty : rty -> rty';


        eq_lty'  : Hlist.t (fun x => x) lty' -> Hlist.t (fun x => x) lty' -> Prop ;
        eq_rty' : rty' ->  rty' -> Prop ; 

        cast_ty : forall (o:ty), eval_typ (fun x => x) lty  rty ;
        cast_ty': forall (o:ty'), eval_typ (fun x =>  x) lty'  rty' ; 

        inj_lem :=
          fun (op:ty) (op':ty') => 
            forall (args : Hlist.t (fun x => x) lty),
              eq_rty'
                (inj_rty (interp_seq (etyp := fun x => x) (r:= rty) args
                                              (cast_ty  op)))
                (interp_seq (etyp := fun x => x) (r:= rty') (inj_lty args)
                                           (cast_ty' op')) ;

        morph_lem := fun (op' : ty') => forall (args args': Hlist.t (fun x => x) lty'),
              eq_lty' args args' -> 
              eq_rty' (interp_seq (etyp := fun x => x) (r:= rty') args
                                     (cast_ty' op'))
                         (interp_seq (etyp := fun x => x) (r:= rty') args'
                                     (cast_ty' op'))

      }.

  Instance MakeDecl (ty : Type) {TD : TypeDecl.t ty} (ty' : Type) {TD' : TypeDecl.t ty'} {N : @DeclInj.t ty TD ty' TD'} : t ty ty'.
  Proof.
    eapply Mk with
    (lty := nil) 
      (rty := ty) 
      (lty' := nil) 
      (rty' := ty')
      (eq_lty' := (fun s s' => True))
      (inj_lty := (fun x => Hlist.snil))
      (inj_rty := DeclInj.inj)
      (eq_rty' := TypeDecl.equi)
      (cast_ty  := (fun x => x))
      (cast_ty' := (fun x => x))
    .
  Defined.

  Instance MakeArrowDecl 
           (ty : Type) {TD : TypeDecl.t ty} (ty':Type) {TD' : TypeDecl.t ty'} {N : @DeclInj.t ty TD ty' TD'} 
           (typ typ': Type) {RN : t typ typ'} : t (ty  -> typ) (ty' -> typ').
  Proof.
    destruct RN.
    eapply Mk with (lty := ty::lty0) (rty := rty0) (lty' := ty' :: lty'0) (rty' := rty'0).
    Grab Existential Variables.
    +
      simpl.
      exact (fun F x => cast_ty'0 (F x)).
    +
      simpl.
      exact (fun F x => cast_ty0 (F x)).
    +
      apply eq_rty'0.
    +
      remember (ty' :: lty'0).
      intros s.
      destruct s.
      exfalso ; apply Hlist.list_discr in Heql ; auto.
      rewrite (Hlist.inj_car Heql) in e.
      rewrite (Hlist.inj_cdr Heql) in s.      
      rewrite Heql. clear Heql.
      remember (ty' :: lty'0).
      intro s' ;  destruct s'.
      exfalso ; discriminate.
      rewrite (Hlist.inj_car Heql) in e0.
      rewrite (Hlist.inj_cdr Heql) in s'.
      apply ((TypeDecl.equi e e0 /\ eq_lty'0 s s')).
    +
      apply inj_rty0.
    +
      remember (ty :: lty0).
      intro s.
      destruct s.
      exfalso ; apply Hlist.list_discr in Heql ; auto.
      rewrite (Hlist.inj_car Heql) in e.
      rewrite (Hlist.inj_cdr Heql) in s.
      apply (Hlist.scons (DeclInj.inj e) (inj_lty0 s)).
  Defined.

End BuildLem.

Module Inj.

  Class t {Ty : Type} (op : Ty) :=
    Mk {
        ty' : Type;
        inj_op :ty';
        BL : BuildLem.t Ty ty';

        inj_spec : @BuildLem.inj_lem _ _ BL op inj_op;
        
        inj_morph : @BuildLem.morph_lem _ _ BL inj_op
      }.

  Arguments Mk  [Ty] [op]  [ty'] inj_op [BL]  inj_spec inj_morph.

End Inj.


Module ReifyType.

  Class t (ty : Type) := 
    Mk {
        args   : list Type;
        res    : Type;
        eq_res : res -> res -> Prop; 
        cast   : forall (o:ty),eval_typ (fun x => x) args res
      }.

  Instance MakeProp : @t  Prop.
  Proof.
    eapply (@Mk _ nil Prop ).
    exact iff.
    exact (fun x => x).
  Defined.

  Instance MakeBtype (ty:Type) {TD : TypeDecl.t ty} : @t ty.
  Proof.
    apply (@Mk _ nil ty).
    apply TypeDecl.equi.
    * exact (fun x => x).
  Defined.
  
  Instance MakeArrow  (A:Type) {TD : TypeDecl.t A} (ty : Type) {RT : @t ty}  : @t (A -> ty).
  Proof.
    destruct RT.
    apply (@Mk _ (A::args0) res0); auto.
    * simpl.
      intros.
      exact (cast0 (o X)).
  Defined.
  
End ReifyType.

Module Unfold.

  Class t {ty : Type} (op : ty)  :=
    Mk {
        op'       : ty;
        RT : ReifyType.t ty ;
        cast_ty   := @ReifyType.cast _ RT ;
        args_ty   := @ReifyType.args _ RT ;
        ret_ty    := @ReifyType.res  _ RT;
        eq_ret_ty := @ReifyType.eq_res _ RT ;

        op_op' : forall (args : Hlist.t (fun x => x) args_ty),
                   eq_ret_ty
                     (interp_seq (etyp := fun x => x) (r:= ret_ty) args (cast_ty op))
                     (interp_seq (etyp := fun x => x) (r:= ret_ty) args (cast_ty op'))
      }.

End Unfold.

Module Abstract.

  Fixpoint gen_ex (l : list Type)  : eval_typ (fun x => x) l Prop ->  Prop :=
    match l with
      | nil => fun P => P
      | ty::l => fun P => exists x:ty , gen_ex l (P x)
    end.

  Class t {ty : Type} (op : ty) :=
    Mk {
        RT : ReifyType.t ty;
        cast_ty   := @ReifyType.cast _ RT ;
        args_ty   := @ReifyType.args _ RT ;
        ret_ty    := @ReifyType.res  _ RT;
        eq_ret_ty := @ReifyType.eq_res _ RT ;
        ex_ty     : list Type ;
        pred      : ret_ty -> (eval_typ (fun x => x) args_ty (eval_typ (fun x => x) ex_ty Prop));
        pred_is_thm : forall r (args : Hlist.t (fun x => x) args_ty),
                        eq_ret_ty
                          (interp_seq (etyp := fun x => x) (r :=ret_ty) args (cast_ty op))
                          r -> 
                        gen_ex ex_ty
                               (interp_seq (etyp := fun x => x) (r:= (eval_typ (fun x => x) ex_ty Prop)) args (pred r))
      }.

End Abstract.

