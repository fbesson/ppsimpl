Require Import ZArith.
Require Import Psatz.
Require Import Ppform.
Require Import BuildPpsimpl.
Require Import FOCompil.
Require Import List.
Unset Ltac Debug.

Definition pos := positive.


Goal (forall (x:pos) (f : pos -> pos), (x = f x)%positive -> f x = xH -> x = xH)%positive.
Proof.
  intros.
  unfold pos in *.
  ppsimpl.
  lia.
Time Qed.

Goal (forall (x:pos) (P:Prop) (f : pos -> pos), P -> (x = f x)%positive -> f x = xH -> x = xH)%positive.
Proof.
  intros.
  unfold pos in *.
  ppsimpl.
  lia.
Time Qed.


Goal forall x:Z, (1 + x = Z.succ x)%Z.
Proof.
  intro.
  ppsimpl.
  lia.
Time Qed.


Goal forall x y : nat, 0 <= x + y +y.
 intros x y.
 ppsimpl.
 lia.
Time Qed.

Goal forall x y : nat, y <= x -> y -x  = 0. 
Proof.
   intros x y.
   Time ppsimpl.
   lia.
Time Qed.

Require Import Bool.

Goal forall x y : bool, x && y = true -> y = true.
Proof.
   intros x y.
   ppsimpl.
   tauto.
Time Qed.



Goal forall x y: Z, (y < x -> Zmax x y = x)%Z.
Proof.
  intros.
  ppsimpl.
  lia.
Time Qed.

Goal forall x:Z, (0 < x -> Zdiv x  x = 1)%Z.
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.

Goal forall x:nat, (x > 0 -> NPeano.sqrt (x * x) = x).
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.



Goal forall x:nat, (x > 1 -> NPeano.sqrt x < x).
Proof.
  intros.
  Time ppsimpl.
  nia.
Time Qed.

Goal forall x:nat, (x > 0 -> NPeano.sqrt x > x -> x = 0).
Proof.
  intros.
  ppsimpl.
  nia.
Time Qed.

Goal forall x:Z, (x + 1 = Z.succ x)%Z.
Proof.
  intro.
  ppsimpl.
  reflexivity.
Time Qed.

