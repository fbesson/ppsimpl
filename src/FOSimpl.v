Require Import List.
Require Import ZArith.
Require Import FOLib.
Require Import FOLType.
Require Import Setoid.
Require Import FOexpr.
Require Import Psatz.
Set Implicit Arguments.
Set Boolean Equality Schemes.

Module TrmWithProp.

  Module Arg.
    Section S.
      Variable btyp_u : Type.
      Variable op_u   : Type.

      Definition btyp := option btyp_u.

      Class t :=
        Mk {
            op_args_u : op_u -> list btyp;
            op_ret_u  : op_u -> btyp
        }.
    
      End S.
  End Arg.


    Open Scope type_scope.
  
    Section S.
      Variable btyp_u : Type.
      Variable op_u   : Type.

      Variable args : Arg.t btyp_u op_u.
      Import Arg.

      Definition btyp := option btyp_u.

      Inductive op_prop : Type :=
      | Prop_True 
      | Prop_And
      | Prop_Impl
      | Prop_Eq (bt :btyp).


      Definition op_prop_args (o:op_prop) : list btyp :=
        match o with
          | Prop_True => nil
          | Prop_And  => None :: None :: nil
          | Prop_Impl => None :: None :: nil
          | Prop_Eq bt =>  bt ::  bt :: nil
        end.

      Variable etyp_u : btyp_u -> Type.

      Definition etyp (bt : btyp) : Type :=
        match bt with
          | None => Prop
          | Some bt => etyp_u bt
        end.
      
      Definition op := op_prop + op_u.
      
      Definition op_args (o:op) : list btyp :=
        match o with
          | inl o => op_prop_args  o
          | inr o => op_args_u o
        end.

      Definition op_ret (o:op) : btyp :=
        match o with
          | inl o => None
          | inr o => op_ret_u o
        end.

      Instance MakeSyntax : TrmSyntax.t := 
        TrmSyntax.Mk etyp op_args op_ret.
      Canonical Structure MakeSyntax.

      Variable op_eval_u : forall (o:op_u), eval_typ etyp (op_args_u o) (op_ret_u o).
      Variable equi_typ_u : forall (bt:btyp_u), etyp_u bt -> etyp_u bt -> Prop.

      Definition op_eval_prop (o: op_prop) : 
        eval_typ etyp (op_args (inl o)) (op_ret (inl o)):=
        match
          o as o' return (eval_typ etyp (op_args (inl o')) (op_ret (inl o')))
        with
          | Prop_True => True
          | Prop_And => and
          | Prop_Impl => fun x y : etyp None => x -> y
          | Prop_Eq bt =>
            match bt as bt'      return
              (eval_typ etyp (op_args (inl (Prop_Eq bt')))
                        (op_ret (inl (Prop_Eq bt'))))
            with
              | Some b => equi_typ_u (bt:=b)
              | None => iff
            end
        end.


      Definition op_eval (o:op) : eval_typ etyp (op_args o) (op_ret o) :=
        match o as o' return eval_typ etyp (op_args o') (op_ret o') with
          | inr opr => op_eval_u opr
          | inl opl => op_eval_prop opl
        end.

      Instance MakeSemantics : TrmSemantics.t MakeSyntax :=
        TrmSemantics.Mk MakeSyntax op_eval.
      Canonical Structure MakeSemantics.


      End S.

End TrmWithProp.


Module Make.
  (* Functor [Btyp.t] -> [OperatorSpec.t] ... -> t *)
  Section S.

    Variable btyp_u : Type.
    Variable btyp_u_spec : Btyp.t btyp_u.
    Import Btyp.

    Instance is_equi_btyp_u (x:btyp_u) : @Equivalence (etyp x) (equi_typ x) := @Btyp.is_equi _ btyp_u_spec x.

    Definition typ := (option btyp_u).
    

    Instance btyp_spec : BtypProp.t btyp_u := Make btyp_u_spec.
    Import BtypProp.

    Notation bt_Prop := (@None btyp_u).
    
    
    Instance EqTest : EqTest.t typ := EqTest.Mk  (@BtypProp.t_beq _ btyp_spec) (@BtypProp.t_beq_sc _ btyp_spec).

    Definition parse_var (v:positive) : typ * positive :=
      @Parser.parse _ BtypProp.parser  v.

    Definition dump_var (v : typ * positive) : positive :=
      let (bt , p) := v in  xI (concat (@Parser.dump _ (@BtypProp.parser _ btyp_spec) bt) p).


    Instance equi_typ_eq (x:typ) : @Equivalence (etyp x) (equi_typ x) := @BtypProp.is_equi _ btyp_spec x.

    Require Import FOexpr.

    Variable op_u : Type.

    Variable syntax_arg : @TrmWithProp.Arg.t btyp_u op_u.
    Import TrmWithProp.Arg.

    Instance expr_synt : TrmSyntax.t := TrmWithProp.MakeSyntax  syntax_arg Btyp.etyp .
    Canonical Structure expr_synt.
    Import TrmSyntax.


    Definition and_expr (var : Type) (e1 e2 : Trm var bt_Prop) :=
      Op (inl (TrmWithProp.Prop_And _)) (Hlist.scons e1 (Hlist.scons e2 Hlist.snil)).

    Definition impl_expr (var : Type) (e1 e2 : Trm var bt_Prop) :=
      Op (inl (TrmWithProp.Prop_Impl _)) (Hlist.scons e1 (Hlist.scons e2 Hlist.snil)).

    Definition mk_conj (var:Type) (e1 : option (Trm var bt_Prop)) (e2 : option (Trm var bt_Prop))  : option (Trm var bt_Prop) :=
      match e1 , e2 with
        | None , None => None
        | Some e , None | None , Some e => Some e
        | Some e1 , Some e2 => Some (and_expr e1 e2)
      end.

    Definition Prop_True (var : Type) : Trm var bt_Prop :=
      Op (inl (TrmWithProp.Prop_True _)) Hlist.snil.      

    Definition option_prop (var : Type) (e: option (Trm var bt_Prop)) :=
      match e with
        | None => Prop_True var
        | Some e => e
      end.

      Variable inj_u : 
        forall  (o:op_u), Hlist.t (Trm positive) (List.map inj_typ (op_args (inr o))) -> Trm positive (inj_typ (op_ret (inr o))).

      Definition inj_prop (o:TrmWithProp.op_prop _) : Hlist.t (Trm positive) (List.map inj_typ (op_args (inl o))) -> Trm positive (inj_typ (op_ret (inl o))):=
        match o with
            | TrmWithProp.Prop_True => Op (inl (TrmWithProp.Prop_True _))
            | TrmWithProp.Prop_And  => Op (inl (TrmWithProp.Prop_And _))
            | TrmWithProp.Prop_Impl => Op (inl (TrmWithProp.Prop_Impl _))
            | TrmWithProp.Prop_Eq bt => Op (inl (TrmWithProp.Prop_Eq (inj_typ bt)))
        end.


      Definition inj (o:op) : Hlist.t (Trm positive) (List.map inj_typ (op_args o)) -> Trm positive (inj_typ (op_ret o)) :=
        match o with
          | inl opl => inj_prop opl
          | inr opr => inj_u opr
        end.

    Fixpoint inj_trm  (r:btyp) (e : Trm positive r) : Trm positive (inj_typ r) :=
      match e  with
        | Cst bt v => Cst _ (inj_cst bt v) 
        | Var bt x => Var (inj_typ bt) (dump_var (bt,x))
        | Op o args => inj o (Hlist.mapt inj_trm args) 
      end.

    Lemma inj_trm_rew : forall  (r:btyp) (e : Trm positive r) ,
      inj_trm  e =
      match e in (Trm _ t0) return (Trm positive (inj_typ t0)) with
        | Cst bt v => Cst _ (inj_cst bt v)
        | Var bt x => Var (inj_typ bt) (dump_var (bt,x))
        | Op o args => inj o (@Hlist.mapt _ (Trm positive)  (Trm positive) inj_typ inj_trm  _ args)
      end.
    Proof.
      destruct e ; reflexivity.
    Qed.

    Variable type_cstr : forall  (bt : btyp) (x:positive), option (Trm positive bt_Prop).

    Definition cstr_of_var (var : btyp * positive) : option (Trm positive bt_Prop) :=
      let (bt , v) := var in option_map (@inj_trm bt_Prop) (type_cstr bt v).


    Definition xtyp_cstr_of_vars (l : list (btyp * positive)) : option (Trm positive bt_Prop) :=
      List.fold_right (fun e acc => mk_conj (cstr_of_var e) acc)  None l.

    Definition typ_cstr_of_vars (l : list (btyp * positive)) :  (Trm positive bt_Prop) :=
      option_prop  (xtyp_cstr_of_vars l).

    Fixpoint xcollect_cstr  (r:btyp) (e : Trm positive r) : option (Trm positive bt_Prop) :=
      match e with
        | Cst _ _  => None
        | Var bt v => match type_cstr bt v with
                        | None => None
                        | Some e => Some (inj_trm e)
                      end
        | Op o args => Hlist.fold_right  (fun bt (e: Trm positive bt) a => mk_conj  (xcollect_cstr  e) a) args None
      end.

    Lemma xcollect_cstr_rew : forall r e, @xcollect_cstr r e =       match e with
        | Cst _ _  => None
        | Var bt v => match type_cstr bt v with
                        | None => None
                        | Some e => Some (inj_trm e)
                      end
        | Op o args => Hlist.fold_right  (fun bt (e: Trm positive bt) a => mk_conj  (xcollect_cstr  e) a) args None
      end.
    Proof.
      destruct e ; reflexivity.
    Qed.

    Definition collect_cstr  (r:btyp) (e : Trm positive r) :  (Trm positive bt_Prop) :=
      match xcollect_cstr e with
        | None => Prop_True  positive
        | Some f => f
      end.
    Notation " X == Y" := (@BtypProp.equi_typ _ btyp_spec _ X Y) (at level 90).

    Variable op_eval_u : forall (o:op_u), eval_typ etyp (op_args_u o) (op_ret_u o).

    Instance expr_sem : TrmSemantics.t expr_synt := 
      TrmWithProp.MakeSemantics syntax_arg _ op_eval_u Btyp.equi_typ.
    Canonical Structure expr_sem.
    Import TrmSemantics.


    Definition Env := @Env expr_synt.



  Fixpoint eval_seq (env : Env  positive) (l : list btyp) :  Hlist.t (Trm positive) l ->  Hlist.t (fun x => x) (List.map etyp l) :=
    match l as l' return Hlist.t (Trm positive) l' ->  Hlist.t (fun x => x) (List.map etyp l') with
      | nil => fun _ => Hlist.snil (typ := fun x => x)
      | cons e l => fun s => Hlist.scons (typ:= fun x => x) (interp env (Hlist.car s)) (eval_seq env  (Hlist.cdr s))
    end.


    Definition inj_Cst (var : Type) (bt : typ) (c: etyp bt) : Trm var (inj_typ bt) :=
      Cst _ (inj_cst _ c).


    Class spec := 
      Mk {
          inj_vars : forall (o:op) s v, In v (vars  (inj o s)) -> In v (Hlist.concat_map_list  (@vars expr_synt _)   s);

          inj_spec : forall  (env: Env positive) (o : op) (vls : Hlist.t etyp (op_args o)),
                        (inj_cst (op_ret o) (interp_seq  vls  (op_eval o)))
                        ==
                        (interp env (inj o (@Hlist.mapt btyp etyp (Trm _) inj_typ (@inj_Cst _ ) _ vls))); 

          inj_morph : forall (env:Env positive) o (l l': Hlist.t (Trm positive) (List.map inj_typ (op_args o))),
      Hlist.equiv  (typ := Trm _) (typ' :=Trm _) (fun bt e1 e2 => interp env e1 == interp env e2)  l l' ->
        (interp env (inj o l)
          ==
          (interp env (inj o l')));





          type_cstr_spec : forall  (bt : btyp) (v:positive) e env,
                             type_cstr  bt v = Some e ->  
                             interp  env e
        }.

    Variable inj_spec : spec.
    Instance iinj_spec : spec := inj_spec.


    Definition inj_env (env : Env positive) : Env positive :=
      fun  x => 
        match x with
          | xI x => 
            let (bt',p) := parse_var x in
              fun bt => 
                match EqTest.eq_dec bt (inj_typ bt') with
                  | left H => eq_rect_r _  (inj_cst _ (env p bt')) H
                  |   _     => env p bt
                end
          | xO x => fun bt => env x bt
          | xH   => fun bt => env xH bt
        end.


    Require Import Eqdep_dec.

    (* black magic *)

    Lemma equi_typ_eq_rect_r : forall (bt:btyp) (e : inj_typ bt = inj_typ bt) (ty : etyp (inj_typ bt)), 
      equi_typ (inj_typ bt) ty (eq_rect_r etyp ty e).
    Proof.
      intros.
      assert (HH: e  = refl_equal (inj_typ bt)).
      {     apply  UIP_dec.
            intros. apply EqTest.eq_dec.
      }
      rewrite HH.
      reflexivity.
    Qed.



Lemma interp_cst : forall (var:Type) bt (env:Env var) v, interp env  (Cst _ (inj_cst bt v)) ==   inj_cst bt v.
    Proof.
      reflexivity.
    Qed.


    Lemma inj_trm_correct : forall  (env: positive -> forall (bt:btyp), etyp bt) (r:btyp) (e: Trm positive r)  , 
      (inj_cst _ (interp  env  e)) == (interp  (inj_env  env)  (inj_trm e)).
    Proof.
      induction e using expr_ind ; [Case CCst | Case CVar | Case COp].
      Case CCst.
      simpl.  reflexivity.
      Case CVar.
      unfold inj_trm. unfold interp.
      unfold inj_env.
      unfold dump_var. unfold parse_var. rewrite Parser.prf.
      dest_match. 
      apply equi_typ_eq_rect_r.
      tauto.
      Case COp.
      rewrite inj_trm_rew.
      simpl (interp env (Op o args)).
      rewrite (inj_morph (inj_env env)).
      apply inj_spec.
      rewrite Hlist.mapt_map.
      apply Hlist.in_mapt_equi.
      intros.
      unfold inj_Cst.
      rewrite interp_cst. 
      symmetry. apply H ; auto.
    Qed.


    Lemma inj_trm_prop : forall  (env : positive -> forall (bt:btyp), etyp bt)  (e: Trm positive bt_Prop),
      interp (inj_env  env) (inj_trm  e) <-> 
      interp env  e.
    Proof.
      intros. rewrite <- inj_trm_correct.
      split; auto.
    Qed.

    Lemma interp_mk_conj : forall var (env:Env var) o o',
      interp env (option_prop o) -> 
      interp env (option_prop o') -> 
      interp env (option_prop (mk_conj o o')).
    Proof.
      destruct o ; destruct o'; simpl ; intros; try auto.
    Qed.
    
    Lemma interp_mk_conj' : forall var (env:Env var) o o',
      interp env (option_prop (mk_conj o o')) -> 
      interp env (option_prop o) /\ interp env (option_prop o').
    Proof.
      destruct o ; destruct o'; simpl ; intros; try auto.
    Qed.

    Lemma interp_True : forall (var:Type) (env:Env var), interp env (Prop_True var).
    Proof.
      intros.
      exact I.
    Qed.

    Hint Resolve interp_True.

    Lemma interp_None : forall (var:Type) (env:Env var), interp env (option_prop None).
    Proof.
      exact interp_True.
    Qed.

    Hint Resolve interp_None.

    Ltac inj_btyp :=
      change (option btyp_u) with btyp in *;
      change (TrmWithProp.btyp btyp_u) with btyp in *.


    Ltac inj_typ o :=
      try inj_btyp;
      try change (TrmWithProp.op_ret syntax_arg o) with (op_ret o) in *;
      try change (@TrmWithProp.op_args btyp_u op_u syntax_arg o) with (op_args o) in *;
      try change (TrmWithProp.op_eval syntax_arg Btyp.etyp op_eval_u Btyp.equi_typ o)  with (op_eval o) in *.

    Lemma cstr_of_var_correct : 
      forall env a e, 
        cstr_of_var a = Some e -> 
        interp (inj_env env) e.
    Proof.
      destruct a ; simpl.
      intros e.
      case_eq (type_cstr b p).
      simpl. intros.
      inv H0.
      apply type_cstr_spec with (env := env) in H.
      rewrite inj_trm_prop. auto.
      discriminate.
    Qed.
      
    Lemma typ_cstr_of_vars_correct : forall env  l, interp (inj_env env) (typ_cstr_of_vars l).
    Proof.
      unfold typ_cstr_of_vars.
      intros.
      case_eq (xtyp_cstr_of_vars l).
      *
      intros.
      change (interp (inj_env env) (option_prop (Some t0))).
      rewrite <- H. clear H. clear t0.
      unfold xtyp_cstr_of_vars.
      induction l ; simpl.
      - auto.
      - apply interp_mk_conj.
        case_eq (cstr_of_var a); simpl.
        apply cstr_of_var_correct.
        auto.
        auto.
      * auto.
    Qed.




    Lemma collect_cstr_correct : forall env  bt (e: Trm positive bt), interp (inj_env env) (collect_cstr e).
    Proof.
      unfold collect_cstr.
      intros.
      case_eq (xcollect_cstr e).
      *
      intros.
      change (interp (inj_env env) (option_prop (Some t0))).
      rewrite <- H. clear H. clear t0.
      induction e using expr_ind ; [Case CCst | Case CVar | Case COp].
      - Case CCst.
        simpl. auto.
      - Case CVar.
        simpl.
      case_eq (type_cstr bt v).
      simpl. 
      intros.
      apply type_cstr_spec with (env:= env) in H.
      change ((inj_cst bt_Prop (interp  env  t0))) in H.
      rewrite inj_trm_correct in H. auto.
      auto.
      - Case COp .
        rewrite xcollect_cstr_rew.
        revert H. revert args.
        generalize (op_args o).
        induction args ; simpl; auto.
        intros. 
        apply interp_mk_conj. apply H;auto. 
        constructor.
        apply IHargs.
        intros ; apply H.
      constructor ; auto. 
        *
      auto.
    Qed.

    Definition inj_trmt  (r: btyp) (e : Trm positive bt_Prop) : Trm positive bt_Prop :=
      impl_expr (collect_cstr e) (inj_trm e).

    Lemma inj_trmt_correct : forall  (e: Trm positive bt_Prop), (forall env, interp   env  (inj_trmt bt_Prop e)) -> 
      forall env, interp env  e.
    Proof.
      intros.
      apply inj_trm_prop.
      unfold inj_trmt in H.
      assert (HH := H (inj_env env)).
      simpl in HH.
      apply HH.
      apply collect_cstr_correct.
    Qed.

    Lemma inj_trmt_sharing_correct : 
      forall  (e: Trm positive bt_Prop) (l:list (btyp * positive)), 
        (forall env, 
           interp  env (typ_cstr_of_vars l) -> 
           interp   env  (inj_trm  e)) -> 
        forall env, interp env  e.
    Proof.
      intros.
      apply inj_trm_prop.
      assert (HH := H (inj_env env)).
      apply HH.
      apply typ_cstr_of_vars_correct.
    Qed.

    Lemma inj_trmt_sharing_correct_keep_env : 
      forall  (e: Trm positive bt_Prop) (l:list (btyp * positive)), 
        forall env, 
          (interp  (inj_env env) (typ_cstr_of_vars l) -> 
           interp   (inj_env env)  (inj_trm  e)) -> 
           interp env  e.
    Proof.
      intros.
      apply inj_trm_prop.
      apply H.
      apply typ_cstr_of_vars_correct.
    Qed.


(** Once the expression is inj_trmd, it is time to simplify *)

  (** There are 3 ways of simplifying a binary expression.
     [Unfold] provides an equivalent expression
     [Abstract] provides an property of the two arguments and the result
     [Id] does nothing *)

    Inductive simp_rule  (o : op) :=
    | Unfold (f : Hlist.t (Trm positive) (op_args o) ->  Trm positive (op_ret o))
    | Abstract 
        (l : list btyp )
        (p :  Trm positive (op_ret o) -> Hlist.t (Trm positive) (op_args o)  -> Hlist.t (Trm positive) l -> Trm positive bt_Prop)
    | Id.

    Implicit Arguments Id [o].
    Implicit Arguments Abstract [o].
    Implicit Arguments Unfold [o].

(*
    Inductive simp_rule_fun   (o : op) :=
    | AbstractFun
      (p :  @FunTrm btyp op btyp_t)
    | NoFun.


    Inductive simp_rule_eq  (o:op) : simp_rule  o -> simp_rule_fun o -> Prop :=
    | EqId : simp_rule_eq   Id (NoFun _)
    | EqUnfold : forall f, simp_rule_eq  (Unfold f) (NoFun  _ )
    | EqAbstract : forall f f' , (forall args ret, f args ret = interp_fun  f' _ (Hlist.scons ret args)) -> 
      simp_rule_eq  (Abstract  f) (AbstractFun  o f').
*)

    Definition same_vars (var :Type) (r r': btyp) (e : Trm var r) (e' : Trm var r'):= forall x, In x (vars  e) <-> In x (vars  e').

    Definition vars_of_pred (o : op) (l : list btyp)
               (p : Trm positive (op_ret o) -> Hlist.t (Trm positive) (op_args o) -> Hlist.t (Trm positive) l -> Trm positive bt_Prop) :=
      forall args res ex x, In x (vars (p res args ex)) <->
                            ((In x (vars  (Op o args))) \/ In x (vars res) \/ In x (Hlist.concat_map_list (@vars _ positive)  ex)).
      

    Class Simpl := MkS {
           op_morph_u :  forall (o:op_u) , is_morph etyp equi_typ  (op_eval_u o);

            simplify_op : forall (o:op), simp_rule  o ; 

                  simplify_correct : forall  o ,
                    match simplify_op  o with
                      | Id => True
                      | Unfold f => forall env args,
                        interp  env  (f args) == interp env (Op o args)
                      | Abstract l p => (forall env args r, 
                                        interp env (Op o args) == interp env r -> 
                                        exists (args' : Hlist.t etyp l) , interp env (p r args (Hlist.map (fun bt c => Cst bt c) args')))
                    end ; 

                    simplify_same_vars : forall  o ,
                      match simplify_op  o with
                        | Id   => True
                        | Unfold f => forall args, forall x, In x (vars  (f args)) <-> (In x (Hlist.concat_map_list (@vars _ positive)  args))
                        | Abstract l p => vars_of_pred o  p
                      end  ;

                    simplify_morph : 
                      forall o,
                        match simplify_op o with
                          | Id => True
                          | Unfold _ => True
                          | Abstract l p => 
                            forall r args  (args1 args2:Hlist.t (Trm positive) l) env, 
                              Hlist.equiv (fun l v v' => interp env v == interp env v') args1 args2 -> 
                              (interp env (p r args args1) <-> interp env (p r args args2))
                        end
    }.

    Variable simpl_spec : Simpl.

    Require Import Setoid.



    Lemma op_morph : forall (o:op), is_morph etyp equi_typ  (op_eval o).
    Proof.
      destruct o.
      *
        unfold is_morph.
        destruct o ; simpl.
        - (* True *)
        intros.
        Hlist.equiv_tac.
        tauto.
        - (* and *)
          intros.
          Hlist.equiv_tac ; tauto.
        - (* impl *)
          intros.
          Hlist.equiv_tac ; tauto.
        - (* eq *)
          intros.
          Hlist.equiv_tac.
          simpl in *.
          flatten.
          rewrite H_l.
          rewrite H_r_l.
          tauto.
      * apply op_morph_u.
    Qed.


  (** [mk_eq] constructs an "equality" corresponding to the equality of types *)

    Definition mk_eq (bt:btyp) (x:positive) : Trm positive bt ->  Trm positive bt_Prop:=
      fun e => Op (inl (TrmWithProp.Prop_Eq bt))  (Hlist.scons (Var bt x) (Hlist.scons e Hlist.snil)).


    Definition acc_typ := (positive * option (Trm positive bt_Prop))%type.


    Definition fold_simplify  (F : positive -> forall (r:btyp) (e:Trm positive r), acc_typ * Trm positive r)
      (acc:acc_typ) (bt:btyp) (e:Trm positive bt) : acc_typ * Trm positive bt :=
      let (fr,eq) := acc in
        let '((fr',eqs),e') := F fr bt e in
          ((fr',mk_conj eq eqs),e').

    Fixpoint make_fresh_seq (l:list btyp) (fr :positive) : positive * Hlist.t (Trm positive) l :=
      match l with
        | nil   => (fr,Hlist.snil)
        | bt::l => 
          let (fr',res) := make_fresh_seq l (Psucc fr) in
          (fr' , Hlist.scons (Var bt (xO fr)) res)
      end.
    
    Definition simpl_op (fr:positive)  (o:op) :  Hlist.t (Trm positive) (op_args o) -> acc_typ * (Trm positive (op_ret  o)) :=
      let sp := simplify_op  o in
        match sp with
          | Id => fun args => ( (fr , None) , Op o args )
          | Unfold f => fun args => ( (fr , None) , f args)
          | Abstract l p => fun args => 
            let ret := Var (op_ret o) (xO fr) in
            let (fr', eargs) := make_fresh_seq l (Psucc fr) in
            ( fr', (Some (p ret args eargs)), ret)
        end.

    Fixpoint xsimplify  (fr: positive) (r:btyp)  (e : Trm positive r) : acc_typ *  Trm positive r  :=
      match e in Trm _ r' return acc_typ * Trm positive r'  with
        | Cst bt x => ( (fr, None) , Cst _ x)
        | Var bt x => ((fr, None) , Var bt  x)
        | Op o args => 
          let '((fr,eqs),args') := Hlist.map_acc (fold_simplify xsimplify) args (fr,None) in
            let '((fr',eqs'),e') := simpl_op fr o args' in
              ((fr',mk_conj eqs eqs'), e')
      end.


  (* [simplify] is the simplification algorithm!! *)
    Definition simplify (e: Trm positive bt_Prop) : Trm positive bt_Prop :=
      let '(_,cond, e) := xsimplify xH e in
        impl_expr (option_prop cond) e.

  (** Extensional equality of environments is an equivalence relation *)

    Definition eq_env (var:Type) (env env' : Env var) : Prop := forall v bt, env v bt  == env' v bt.

    Notation " X ≡ Y" := (eq_env  X Y) (at level 90).

    Lemma eq_env_refl : forall  (var:Type) (x: Env var), x ≡ x.
    Proof.
      unfold eq_env. reflexivity.
    Qed.

    Lemma eq_env_sym : forall (var:Type) (x y: Env var), x ≡ y -> y ≡ x.
    Proof.
      unfold eq_env.
      intros. rewrite H. reflexivity.
    Qed.

    Lemma eq_env_trans : forall (var:Type) (x y z: Env var), x ≡ y -> y ≡ z -> x ≡ z.
    Proof.
      unfold eq_env.
      intros.
      rewrite H. rewrite H0.
      reflexivity.
    Qed.

    Instance eq_env_eq (var:Type) : @Equivalence (Env var) (@eq_env var) :=
    {|
      Equivalence_Reflexive := @eq_env_refl var ;
      Equivalence_Symmetric := @eq_env_sym var ;
      Equivalence_Transitive := @eq_env_trans var 
      |}.

    (** The interpretation function [interp] is a morphism for this relation *)
    Lemma interp_morph : forall var env env' r (e: Trm var r) ,
      eq_env env env' -> 
      interp env e == interp env' e.
    Proof.
      intros.
      revert H. 
      intro.
      induction e using expr_ind ; [Case CCst  | Case CVar | Case COp]; simpl.
      reflexivity.
      apply H.
      Case COp.
      assert (Hmorph := op_morph o).
      revert Hmorph.
      inj_typ o.
      generalize (op_eval o).
      generalize (op_ret o).
      intro r.  intro f; revert f.
      revert H0.
      intros.
      revert f Hmorph H0.
      revert args.
      generalize (op_args o).
      intros.
      apply Hmorph.
      apply Hlist.in_equi.
      intros ; apply H0; auto.
    Qed.


    Add Parametric Morphism (var:Type) (bt:btyp) :(@interp _ var _ bt) with signature @eq_env  var ==>  (@eq (Trm var bt)) ==> (equi_typ  bt) as interp_eq_morph.
    Proof.
      intros.
      apply interp_morph ; auto.
    Qed.

  (** The algorithms is introducing "fresh" variables. This is handled by merging environments *)

    Definition merge_env (fr:positive) (env env' : Env positive) : Env positive :=
      fun p => 
        match p with
          | xI v => env p
          | xO v => match Pcompare v fr Eq with
                      | Lt => env p
                      | Gt | Eq => env' p
                    end
          | _   => env xH
        end.



    Add Parametric Morphism  (fr:positive) : (@merge_env  fr) with signature (@eq_env positive) ==> (@eq_env positive) ==> (@eq_env positive) as merge_env_mor.
    Proof.
      unfold eq_env , merge_env.
      intros.
      destruct v;auto. 
      destruct (Pos.compare_cont v fr Eq); auto.
    Qed.
    
    Lemma merge_env_Plt : forall v v' bt env env',
      Ple v v' ->
      merge_env v env env' (xO v') bt  = env' (xO v') bt.
    Proof.
      unfold merge_env ; intros.
      case_eq (Pos.compare_cont v' v  Eq) ; mauto.
      intros ; apply False_ind ; auto with zarith.
    Qed.

    Lemma merge_env_Ple : forall v v' bt env env',
      Plt v' v ->
      merge_env v env env' (xO v') bt  = env (xO v') bt.
    Proof.
      unfold merge_env ; intros.
      case_eq (Pos.compare_cont v' v  Eq) ; mauto.
      intros ; subst. apply False_ind ; auto with zarith.
      intros ; subst. apply False_ind ; auto with zarith.
    Qed.


    Lemma merge_env_env : forall fr env, 
      (merge_env fr env env) ≡ env.
    Proof.
      unfold merge_env. unfold eq_env.
      intros.
      destruct v ; try reflexivity.
      destruct (Pos.compare_cont v fr Eq) ; reflexivity.
    Qed.

    Lemma merge_env_assoc : forall env env1 env2 fr fr',
      Ple fr fr' -> 
      (merge_env fr' (merge_env fr env env1) env2) ≡
      (merge_env fr env (merge_env fr' env1 env2)).
    Proof.
      unfold eq_env. unfold merge_env.
      intros.
      destruct v. reflexivity.
      case_eq (Pos.compare_cont v fr' Eq) ; 
      case_eq (Pos.compare_cont v fr Eq) ; mauto ; intros ; mauto ; subst ; intuition auto with zarith;
        [reflexivity || apply False_ind ; auto with zarith].
      reflexivity.
    Qed.

    Definition update_env (bt:btyp) (env : positive -> forall bt : btyp, etyp bt) (x:positive)  (v : etyp bt) : positive -> forall bt : btyp, etyp bt :=
      fun (p : positive) =>
        if EqTest.eq_dec p x
          then
            fun ty : btyp =>
              match EqTest.eq_dec bt ty with
                | left _H0 =>
                  eq_rect bt _ v ty _H0 
                | right _ => env p ty
              end
          else env p.


    Lemma update_env_eq : forall env x bt v, (update_env bt env x  v) x bt = v.
    Proof.
      unfold update_env.
      intros.
      destruct (@EqTest.eq_dec positive positive_beq_test x x) ; try congruence.
      dest_match ; try congruence.
      assert (HH: e0  = refl_equal bt). 
      { apply  UIP_dec. intros. apply EqTest.eq_dec. }
      rewrite HH.
      unfold eq_rect_r. unfold eq_rect.
      reflexivity.
    Qed.

    Lemma update_env_neq : forall env x x' bt bt' v, x <> x' \/ bt <> bt' ->  (update_env bt env x v) x' bt' = env x' bt'.
    Proof.
      unfold update_env.
      intros.
      destruct (@EqTest.eq_dec positive positive_beq_test x' x) ; try congruence.
      dest_match ; try congruence.
      intuition congruence.
    Qed.

    Add Parametric Morphism (bt:btyp)  : (@update_env bt) with signature (@eq_env  positive)  ==>  (@eq positive) ==>  (equi_typ  bt) ==>  (@eq_env positive) as update_morph.
    Proof.
      intros.
      unfold eq_env.
      intros.
      destruct (EqTest.eq_dec  y0 v) ; try congruence.
      subst.
      destruct (EqTest.eq_dec  bt bt0) ; try congruence.
      subst.
      rewrite update_env_eq.
      rewrite H0.
      rewrite update_env_eq. reflexivity.
      rewrite update_env_neq ; try intuition congruence.
      rewrite update_env_neq ; try intuition congruence.
      apply H.
      rewrite update_env_neq ; try intuition congruence.
      rewrite update_env_neq ; try intuition congruence.
      apply H.
    Qed.


    Lemma update_merge_env : forall v v' env env'  bt vl,
      Ple v v' -> 
      update_env bt (merge_env v env env') (xO v')  vl
      ≡
      merge_env v env (update_env bt env' (xO v')  vl).
    Proof.
      unfold eq_env.
      intros.
      destruct (EqTest.eq_dec v0 (xO v')).
      subst.
      destruct (EqTest.eq_dec bt bt0).
      subst.
      rewrite update_env_eq.
    (**)
      unfold merge_env.
      case_eq (Pos.compare_cont  v' v Eq) ; mauto.
      rewrite update_env_eq.
      reflexivity.
      intros ; apply False_ind ; auto with zarith.
      rewrite update_env_eq ; reflexivity.
      rewrite update_env_neq ; [| tauto].
      rewrite merge_env_Plt; auto.
      rewrite merge_env_Plt; auto.
      rewrite (update_env_neq) ; [|tauto].
      reflexivity.
      rewrite update_env_neq ; [| intuition congruence].
    (**)
      unfold merge_env.
      destruct v0. 
      reflexivity.
      case_eq (Pos.compare_cont v0 v Eq) ; mauto.
      intros; subst. rewrite update_env_neq;[reflexivity |intuition congruence].
      reflexivity.
      intros.
      rewrite update_env_neq;[reflexivity |intuition congruence].    
      reflexivity.
    Qed.


    Lemma interp_merge_env_env : forall r (e : Trm positive r) fr env,
      interp  (merge_env fr env env)  e  ==     interp  env  e.
    Proof.
      intros.
      apply interp_morph.
      apply merge_env_env. 
    Qed.

  (** To reason about freshness, we compute the maximal variable index in expressions *)
    Definition vars_between (r:btyp) (e : Trm positive r) (l h : positive)  :=
      (l <= h)%positive /\ forall x, In (xO x) (vars e) -> (l <= x < h)%positive.

    Notation "E ∈ [ L ; R ]" := (vars_between  E L R) (at level 90).


    Lemma vars_between_le : forall r (e: Trm positive r)  l l' h h', (l' <= l -> h <= h' -> e ∈ [l ; h] -> e ∈ [l';h'])%positive.
    Proof.
      unfold vars_between.
      intuition (fo mauto).
    Qed.

    Lemma vars_between_eq : forall r (e:Trm positive r) l h, 
      e ∈ [l; h] <-> ((l <= h)%positive /\
        match e with
          | Cst _ _ => True
          | Var _ (xO x) => (l <= x < h)%positive
          | Var _ _ => True
          | Op _ e => forall x (ei:Trm positive x), Hlist.In ei e -> ei ∈ [ l ; h ]
        end).
    Proof.
      unfold vars_between. 
      split ; [Case LR |  Case RL ] ; intros; flatten ; try tauto.
      destruct e ; 
        [Case CCst 2 | Case CVar 2 | Case COp 2] 
        ;simpl; intros ; try solve [intuition mauto].
      Case CVar 2.
      split ; auto.
      destruct v; auto. 
      apply (H_r v). simpl. tauto.
      Case COp 2.
      split ; try tauto.
      intros. split ; auto.
      intros. apply H_r. simpl.
      rewrite Hlist.in_concat_map.
      exists x. exists ei. tauto.
      destruct e ; 
        [Case CCst 2 | Case CVar 2 | Case COp 2].
      Case CCst 2.
      split ; auto; simpl. tauto.
      Case CVar 2.
      destruct v ; simpl ; intuition mauto.
      Case COp 2.
      split ; auto.
      simpl.
      intros. rewrite Hlist.in_concat_map in H.
      flatten. eapply H_r ; eauto.
    Qed.

    Lemma vars_between_same_vars : forall r r' e e', @same_vars positive r r' e e' -> forall l h, (e ∈ [ l ; h]) <-> (e' ∈ [l ;h]).
    Proof.
      unfold vars_between, same_vars ; intros.
      intuition (try (fo mauto)).
    Qed.


    Ltac close :=
      match goal with
        | H : ?E ∈ [ ?X ; ?Y ] |- ?E ∈ [ ?Z ; ?T ] => apply vars_between_le with (3:= H)
        | H : forall (bt : btyp) (ei : Trm positive bt),
          Hlist.In ei (Hlist.scons ?E  ?L) -> ei ∈  [?X; ?Y] |- ?E ∈ [ ?Z ; ?T] => 
            let HH := fresh in 
              assert (HH : E ∈ [X ; Y]) by (apply H ; constructor ) ; 
                apply vars_between_le with (3:= HH)
      end ; solve [mauto].

    Lemma vars_mk_eq : forall r (e: Trm positive r) v x,  In x (vars (mk_eq v e)) ->  In x (v :: vars e).
    Proof.
      unfold mk_eq.
      destruct r ; simpl.
      *
      intros.
      rewrite in_app in H.
      simpl in H.
      intuition.
      *
        intros.
        repeat rewrite <- app_nil_end in *.
        repeat in_app.
        simpl in H.
        tauto.
    Qed.

    Lemma option_prop_None_bet : forall fr fr', (fr <= fr')%positive ->
      option_prop None ∈  [fr; fr'].
    Proof.
      simpl ; intros.
      unfold vars_between.
      split ; auto ; intros.
      simpl in H0.
      tauto.
    Qed.

    Lemma option_prop_Some_bet : 
      forall e fr fr', 
        e ∈ [ fr ; fr'] -> 
        option_prop (Some e) ∈ [fr ; fr'].
    Proof.
      simpl. auto.
    Qed.


    Lemma Prop_True_bet : forall fr fr', (fr <= fr')%positive ->
      Prop_True positive ∈  [fr; fr'].
    Proof.
      simpl ; intros.
      unfold vars_between.
      split ; auto ; intros.
      simpl in H0.
      tauto.
    Qed.


  (** Properties of [interp] *)
    Lemma interp_vars : forall var env env' r (e: Trm var r) ,
      (forall x, In x (vars   e) -> forall bt, env x bt == env' x bt) -> 
      interp env e == interp env' e.
    Proof.
      induction e using expr_ind ; simpl ; try reflexivity.
      * 
        intuition.
      *
      intros.
      assert (Hmorph := op_morph  o).
      revert Hmorph.
      inj_typ o.
      generalize (op_eval o).
      intro f ; revert f.
      unfold is_morph. intros.
      apply Hmorph.
      apply Hlist.in_equi.
      intros.
      apply H;auto.
      intros. apply H0.
      rewrite Hlist.in_concat_map.
      exists x ; exists e ; tauto.
    Qed.

    Lemma interp_merge_env : forall r (e: Trm positive r) x v env env',
      e ∈ [x ; v ] -> 
      interp env e == interp (merge_env v env env') e.
    Proof.
      intros.
      apply interp_vars.
      intros.
      unfold vars_between in H.
      unfold merge_env.
      destruct x0. reflexivity.
      apply H in H0. 
      case_eq (Pos.compare_cont x0 v Eq); [Case Eq | Case Lt | Case Gt];  mauto.
      ECase Eq by (intros ; subst ; apply False_ind ; mauto).
      ECase Lt by reflexivity.
      ECase Gt by (intros ; subst ; apply False_ind ; mauto).
      reflexivity.
    Qed.

    Lemma interp_update :  forall r (e: Trm positive r) x v v' bt vl env,
      e ∈ [ x ; v ] -> 
      Ple v v' ->
      interp env e == interp (update_env bt env (xO v')  vl) e.
    Proof.
      intros.
      apply interp_vars.
      intros.
      unfold vars_between in H.
      destruct (EqTest.eq_dec (xO v') x0).
      subst. 
      flatten.
      apply H_r in H1. apply False_ind ; mauto ; auto with zarith.
      rewrite update_env_neq.
      reflexivity. tauto.
    Qed.

    Lemma option_prop_mk_conj : forall (e1 e2: option (Trm positive bt_Prop)), 
      vars  (option_prop  (mk_conj e1 e2)) =  vars (option_prop e1) ++ vars  (option_prop e2).
    Proof.
      unfold mk_conj.
      destruct e1 ; destruct e2 ; simpl;
      try rewrite <- app_nil_end ; reflexivity.
    Qed.
    
    Lemma mk_conj_bet : forall o o' l h,
      option_prop o ∈ [ l ; h ] -> 
      option_prop o' ∈ [ l ; h ] -> 
      option_prop (mk_conj o o') ∈ [l ; h].
    Proof.
      unfold option_prop.
      destruct o ; destruct o' ; simpl ; intros ; auto.
      unfold vars_between in *.
      flatten ; split ; auto.
      intros.
      simpl in H.
      in_app. 
      destruct H; [apply H_r | apply H0_r] ; auto.
      simpl in H. tauto.
    Qed.

    Lemma mk_impl_bet : 
      forall o o' l h,
        o ∈ [ l ; h ] -> 
        o' ∈ [ l ; h ] -> 
        (impl_expr o o') ∈ [l ; h].
    Proof.
      unfold vars_between in *.
      flatten ; split ; auto.
      intuition.
      simpl.
      intros.
      in_app. 
      simpl in H1.
      destruct H1.
      apply H ; auto.
      destruct H1 ; apply H0 ; auto.
      tauto.
    Qed.



  (** Simplification functions increment the fresh counter *)


    Lemma interp_merge_equiv : forall var l (args: Hlist.t (Trm var) l) (args' :Hlist.t (Trm positive) l)   env envp env1 env2 x fr fr'', 
      (fr <= fr'')%positive -> 
      (forall bt (e: Trm positive bt), Hlist.In e args' -> e ∈ [ x ; fr'' ]) -> 
      Hlist.equiv
      (fun (bt : btyp) (ei : Trm var bt) (ei' : Trm positive bt) =>
        interp env ei == interp (merge_env fr envp env1) ei') args args' -> 
      Hlist.equiv
      (fun (bt : btyp) (ei : Trm var bt) (ei' : Trm positive bt) =>
        interp env ei ==
        interp (merge_env fr envp (merge_env fr'' env1 env2)) ei') args args'.
    Proof.
      intros until 0.
      intros Hle Hbound Hequiv.
      induction Hequiv.
      constructor.
      constructor.
      apply IHHequiv ; auto.
      intros. apply Hbound. constructor ; auto.
      rewrite <- merge_env_assoc;mauto.
      rewrite <- (@interp_merge_env _ e2);auto.
      apply Hbound. constructor.
    Qed.

    Lemma interp_merge_env_bet2  :forall bt  (e : Trm positive bt)  fro1  fro2 fr2 env env1 env2,
      e ∈ [fro2 ; fr2] -> 
      Ple fro1 fro2 -> 
      (interp (merge_env fro1 env (merge_env fro2 env1 env2)) e == interp (merge_env fro2 env env2) e).
    Proof.
      intros until 0.
      intros Hbet Hle.
      apply interp_vars; intros ; unfold merge_env.
      destruct x ; try reflexivity.
      unfold vars_between in *. flatten.
      apply Hbet_r in H.
      case_eq (Pcompare x fro1 Eq) ; mauto ; try reflexivity;
        case_eq (Pcompare x fro2 Eq) ; mauto ; try reflexivity;
          (intros; repeat subst; apply False_ind; mauto).
    Qed.

    Section FoldLe.

      Variable F : positive -> forall (r:btyp) (e:Trm positive r), acc_typ * Trm positive r.

      Definition F_le (l: list btyp) (args : Hlist.t (Trm positive) l) :=
        forall fr fr' r (e:Trm positive r) eqs' e',
          Hlist.In e args -> 
          F fr e = (fr',eqs',e') -> Ple fr fr'.

      Ltac dest_match :=
        match goal with
          | |- context[match ?X with pair _ _ =>_ end] => destruct X
        end.

      Lemma map_acc_le :  forall
        l (args: Hlist.t (Trm positive) l)  fr fr' eqs c,
        F_le args -> 
        Hlist.map_acc  (fold_simplify F) args (fr, None) =  (fr', eqs, c) ->
        Ple fr fr'.
      Proof.
        intros l args.
        induction args;simpl.
        intros ;mauto.
        intros until 0.
        inj_btyp.
        case_eq_match.
        (* Hlist.map_acc  
          (@fold_simplify  F)  args (fr, None)) . *)
        intros until 0.
        case_eq (fold_simplify F a e).
        intros. 
        inv H2. 
        destruct a.
        apply IHargs in H0.
        unfold fold_simplify in H.
        revert H ; case_eq (F p e).
        destruct a.
        intros. eapply H1 in H. inv H2. mauto.
        constructor. 
        unfold F_le in *. intros. eapply H1 ; eauto.
        constructor ; auto.
      Qed.


      Definition F_bet (l: list btyp) (args : Hlist.t (Trm positive) l) x  :=
        forall fr fr' r (e:Trm positive r) eqs' e',
          Hlist.In e args ->  (x <= fr)%positive ->  e ∈ [x ; fr] -> F fr e = (fr',eqs',e') ->     
          (option_prop eqs' ∈ [x ; fr']) /\ (e' ∈ [x ; fr']).

      Lemma map_acc_bet : forall l (args: Hlist.t (Trm positive) l)  fr fr' eqs c x,
        (forall lt (e: Trm positive lt) , Hlist.In e args -> e ∈ [ x ; fr]) -> 
        F_le args -> 
        F_bet args x  -> (x <= fr)%positive -> 
        Hlist.map_acc  (fold_simplify  F) args (fr, None) =  (fr', eqs, c) ->

        option_prop eqs ∈ [x ; fr']/\ 
        forall bt (e : Trm positive bt), Hlist.In e c -> e ∈ [x ; fr'].
      Proof.
        intros l args.
        induction args;simpl;  [Case Base | Case Induct].
        Case Base.
        intros ;mauto.
        split ; [apply option_prop_None_bet |].
        mauto. intros. Hlist.seq.
        Case Induct.
        intros until 0.
        intros Hin HF_le HF_bet Hle.
        inj_btyp.
        case_eq_match.
        (*case_eq (Hlist.map_acc  (fold_simplify F) args (fr, None) ).*)
        intros until 1.
        unfold fold_simplify.
        destruct a.
        case_eq (F p e).
        destruct a.
        intros until 0 ; intro HF ; intro HH ; inv HH.
        assert (He : e ∈ [x0 ; fr]).  apply Hin. constructor.
        assert (Hle' := HF) ; apply HF_le in Hle' ; [ | constructor].
        assert (F_le_args : F_le args)
          by 
          (unfold F_le in * ; intros;eapply HF_le ; eauto; constructor ; auto).
        assert (MapAcc_le := H).
        apply map_acc_le in MapAcc_le ; auto.
        assert (Hbet := HF). apply HF_bet in Hbet ; [| constructor| mauto | close].
        assert (FAcc_le := H).
        apply IHargs with (x:=x0) in FAcc_le ; mauto; [Case G 1| Case Harg 1 | Case Harg 1].
        Case G 1.
        split.
        apply mk_conj_bet ; try close.
        intros. destruct (Hlist.In_scons_inv H0) as [(-> & ->)|H']; clear H0. close.
        apply FAcc_le_r in H'. close.
        intros ; apply Hin. constructor ; auto.
        Case Discharge_F_bet 2.
        unfold F_bet in *.
        intros. eapply HF_bet ; eauto. constructor ; auto.
      Qed.


      Definition F_correct (l:list btyp) (args : Hlist.t (Trm positive) l) :=
        forall  y fr (fr':positive) r (e e':Trm positive r) c env,
          Hlist.In e args -> 
          e ∈ [y ; fr] -> 
          F fr e = (fr',c,e') -> 
          exists env',
            (interp  env  e  == interp (merge_env fr  env env') e')
            /\ 
            (interp  (merge_env fr   env env')  (option_prop c)).

      Lemma map_acc_correct : forall   l (args:Hlist.t (Trm positive) l)  x fr fr' eqs' args',
        Ple x fr -> 
        F_correct args -> 
        F_le args -> 
        F_bet args x  -> 
        (forall bt (e:Trm positive bt), Hlist.In e args ->  e ∈ [x ; fr]) -> 
        Hlist.map_acc  (fold_simplify F) args (fr, None) = (fr',eqs',args') -> 
        forall env, exists env',
          Hlist.equiv  (fun bt (ei: Trm positive bt) (ei': Trm positive bt) => interp env ei == interp (merge_env fr  env env') ei')  args args'
          /\
          interp (merge_env fr env env') (option_prop eqs').
      Proof.
        induction args ; simpl ; [Case Nil | Case Cons] ; intros until 0 ; intros Hle HFcorrect HFle HFbet.
        Case Nil.
        intros.  inv H0.
        exists  env.
        split. constructor.
        apply interp_None.
        Case Cons.
        inj_btyp.
        case_eq_match. 
        (*
        case_eq (Hlist.map_acc (fold_simplify F) args (fr,None)). *)
        intros until 0. 
        case_eq (fold_simplify F a e) ; intros.
        inv H2.
        rename t0 into args'.
        destruct a as [fr'' eqs''].
        unfold fold_simplify in H.
        revert H.
        case_eq (F fr'' e).
        destruct a ; intros.
        inv H2.
        rename H0 into Hacc.
        assert (IHargs' := Hacc).
        assert (F_le_args : F_le args) by 
          (unfold F_le in *;
            intros; eapply HFle ; eauto; constructor ; auto).
        apply IHargs with (env:= env) (x:=x0) in IHargs' ; auto ;clear IHargs.
        destruct IHargs' as [env1 [Hargs Heqs]].
        assert (Habst_correct := H).
        unfold F_correct in HFcorrect.
        apply HFcorrect with (env := merge_env fr env env1) (y:=x0) in Habst_correct ; try solve [constructor].
        destruct Habst_correct as [env2 [He Heq]].
        exists (merge_env fr'' env1 env2).
        assert (H0_le := Hacc). apply map_acc_le in H0_le ; auto.
        assert (H0_bet := Hacc). apply map_acc_bet with (x:=x0) in H0_bet ; auto.
        assert (H_le := H). apply HFle in H_le.
        assert (H_bet := H). unfold F_bet in HFbet.
        split ; [Case Equiv 2 | Case Eqs 2].
        constructor ; [Case EquivRec 3 | Case EquivBase 3].
        Case  EquivRec 3.
        apply interp_merge_equiv with (x:=x0) ;mauto.
        Case EquivBase 3.
        rewrite (@interp_merge_env _ _ x0 fr  env env1).
        rewrite <- merge_env_assoc;mauto.
        apply H1. constructor.
        Case Eqs 2.
        apply interp_mk_conj ; [Case MkConj1 3| Case MkConj2 3].
        Case MkConj1 3.
        rewrite <- merge_env_assoc ; mauto.
        rewrite <- (@interp_merge_env);auto.
        eauto.
        Case MkConj2 3.
        rewrite <- merge_env_assoc ; mauto.
        constructor.
        Case Cons.
        intros. apply H1. constructor ; auto.
        Case F_bet_args 1.
        unfold F_bet in *.
        intros.
        ECase F_bet_args by (eapply HFbet ; eauto; constructor ; auto).
        Case Bound 1.
        eapply map_acc_le in Hacc ; mauto.
        close.
        Case CFcorrect 1.
        unfold F_correct in * ; intros. apply HFcorrect with (env := env0) (y:=y) in H3; auto.
        constructor ; auto.
        Case CF_bet 1.
        unfold F_bet in *.
        intros.
        eapply HFbet ; eauto.
        constructor ; auto.
        intros ; apply H1 ; auto. constructor ; auto.
      Qed.


    End FoldLe.


    Lemma make_fresh_seq_le : 
      forall l fr fr' s,
        make_fresh_seq l fr = (fr', s) -> 
        Ple fr fr'.
    Proof.
      induction l ; simpl.
      * 
      intros.
      inv H.
      mauto.
      * 
        intros until 0.
        case_eq (make_fresh_seq l (Pos.succ fr)).
        intros.
        inv H0.
        apply IHl in H.
        mauto.
    Qed.

    Lemma simpl_op_le : forall o (s:Hlist.t (Trm positive) (op_args o)) fr fr' e' c,
      @simpl_op fr o s = ((fr',c), e') -> Ple fr fr'.
    Proof.
      unfold simpl_op.
      intros until 0.
      generalize (simplify_correct o ).
      destruct (@simplify_op simpl_spec  o) ; [Case Unfold | Case Abstract | Case Id].
      Case Unfold.
      intros Heq Hinv.
      inv Hinv. mauto.
      Case Abstract.
      intros Hp.
      case_eq_match.
      intros.
      inv H0.
      apply make_fresh_seq_le in H.
      mauto.
      Case Id.
      intros. mauto.
    Qed.
    
    Lemma xsimplify_le : forall r (e : Trm positive r) fr fr' e' c,
      xsimplify fr e = (fr',e',c) -> Ple fr fr'.
    Proof.
      intro.
      induction e using expr_ind ; 
        [
          Case CCst 
          | Case CVar 
          | Case COp 
        ]; intros;
        simpl in H ; try (solve [inv H; mauto]).
      Case COp.
      simpl in H0.
      revert H0.
      inj_typ o.
      case_eq_match. 
      (*case_eq (Hlist.map_acc  (fold_simplify xsimplify) args (fr,None)).*)
      intros.
      destruct a as [fr1 eqs1].
      apply map_acc_le in H0.
      revert H1.
      case_eq (simpl_op fr1 o t0).
      intros.
      destruct a.
      apply simpl_op_le in H1.
      inv H2.
      mauto.
      unfold F_le.
      intros ; eapply H ; eauto.
    Qed.


    Lemma map_acc_xsimplify_le : forall l (args: Hlist.t (Trm _) l)  fr fr' eqs' e',
      Hlist.map_acc  (fold_simplify xsimplify) args (fr, None) =
      (fr', eqs', e') -> 
      Ple fr fr'.
    Proof.
      intros.
      apply map_acc_le in H. auto.
      unfold F_le.
      intros.
      apply xsimplify_le in H1. auto.
    Qed.

  (** Fresh variables are within the desirable interval *)

    Lemma make_fresh_bet :  
      forall l fr fr' s,
        make_fresh_seq l fr = (fr',s) -> 
        forall (bt : btyp) (e:Trm positive bt), Hlist.In e s -> e ∈ [ fr ; fr'].
    Proof.
      induction l ; simpl ; intros.
      *
      inv H.
      Hlist.seq.
      *
        revert H.
        case_eq (make_fresh_seq l (Pos.succ fr)).
        intros.
        inv H1.
        apply Hlist.In_In_fix in H0.
        simpl in H0.
        destruct H0.
        + 
        destruct H0.
        subst.
        simpl in H0.
        subst.
        rewrite vars_between_eq.
        apply make_fresh_seq_le in H.
        mauto.
        + 
          apply IHl with (e := e) in H ; auto.
          unfold vars_between in *.
          destruct H.
          split.
          mauto.
          intros.
          apply H1 in H2.
          mauto.
          apply Hlist.In_In_fix' ; auto.
    Qed.

    Lemma vars_of_pred_vars_between :
      forall o lt
             (res : Trm positive (op_ret o))
             (args : Hlist.t (Trm positive) (op_args o))
             (exl  : Hlist.t (Trm positive) lt) p l h
             (LE   : (l <= h)%positive)
             (VARS : vars_of_pred o p)
             (RES : res ∈ [ l; h])
             (ARG : forall ty (e: Trm positive ty), Hlist.In e args -> e  ∈ [ l; h])
             (EX  : forall ty (e: Trm positive ty), Hlist.In e exl -> e  ∈ [ l; h]) ,
        p res args exl ∈  [l; h].
    Proof.
      intros.
      unfold vars_between.
      split; auto.
      intros.
      unfold vars_of_pred in VARS.
      rewrite VARS in H; clear VARS.
      destruct H as [H | [H| H]].
      - simpl in H.
        rewrite Hlist.in_concat_map in H.
        flatten.
        apply ARG in H_r; auto.
      - apply RES; auto.
      - rewrite Hlist.in_concat_map in H.
        flatten.
        apply EX in H_r; auto.
    Qed.
    
    Lemma vars_between_Var :
      forall ty fr l h
             (LE : (l <= fr < h)%positive),
        Var ty (xO fr) ∈  [l; h].
    Proof.
      unfold vars_between.
      split.
      - mauto.
      - simpl. intros.
        destruct H ; try tauto.
        inv H. auto.
    Qed.


    Lemma simpl_op_bet : forall o args x fr fr' e' c,
      (x <= fr)%positive -> 
      (forall bt (e: Trm positive bt), Hlist.In e args ->  e ∈ [ x ; fr ] ) -> 
      @simpl_op fr o args = (fr',c, e')  ->
      e' ∈ [ x ; fr'] /\ ((option_prop c) ∈ [x ; fr']).
    Proof.
      intros until 0.
      intros Hbe1 Hbe2.
      intro Hf.
      assert (Hle := Hf).
      apply simpl_op_le in Hle.
      unfold simpl_op in *.
      assert (Hvars := simplify_same_vars  o).
      inj_typ o.
      destruct (@simplify_op simpl_spec  o); 
        [ Case Unfold 0 | Case Abstract 0 | Case Id 0] ; intros ; mauto.
      Case Unfold.
      assert (Hvar' :=  Hvars args).
      clear Hvars.
      unfold vars_between in *.
      split;auto.
      split ; auto.
      intros.
      rewrite Hvar' in H.
      rewrite Hlist.in_concat_map in H.
      flatten.
      eapply Hbe2 ; eauto.
      apply option_prop_None_bet. mauto.
      Case Abstract.
      revert Hf.
      inj_typ o.
      case_eq_match. 
      intros fr'' s Hs Hf.
      inv Hf. 
      rewrite vars_between_eq.
      assert (Hs' := Hs).
      apply make_fresh_seq_le in Hs.
      flatten.
      split.
      mauto.
      simpl.
      assert  (Hs_bet := @make_fresh_bet _ _ _ _ Hs').
      -
        apply vars_of_pred_vars_between ; auto.
        mauto.
        apply vars_between_Var. mauto.
        intros.
        apply Hbe2 in H.
        apply vars_between_le with (l:= x) (h:=fr); mauto.
        intros.
        apply Hs_bet in H.
        apply vars_between_le with (l:= Pos.succ fr) (h:=fr'); mauto.
      - 
      split ; auto.
      unfold vars_between.
      split ; mauto.
      intros.
      simpl in H.
      rewrite Hlist.in_concat_map in H.
      flatten.
      apply Hbe2 in H_r ; auto.
      simpl.
      apply Prop_True_bet. mauto.
    Qed.
    
    Lemma xsimplify_bet : 
      forall r (e : Trm positive r) x fr fr' e' c,
        e ∈ [ x ; fr] -> 
        xsimplify fr e = (fr',c,e') ->
        e' ∈ [ x ; fr'] /\ ((option_prop c) ∈ [x ; fr']).
    Proof.
      induction e using expr_ind ; 
        [Case CCst | Case CVar | Case COp]; simpl ; intros.
      Case CCst.
      inv H0 ;simpl.
      split. auto.
      apply Prop_True_bet. rewrite vars_between_eq in H. tauto.
      Case CVar.
      inv H0.
      split. auto.
      apply option_prop_None_bet ; auto.
      rewrite vars_between_eq in H. tauto.
      Case COp.
      revert H1.
      rewrite vars_between_eq in H0.
      inj_typ o.
      case_eq_match. 
      (* case_eq (Hlist.map_acc  (fold_simplify xsimplify) args
        (fr, None)). *)
      destruct a.
      intros until 0. case_eq (simpl_op p o t0).
      destruct a.
      intros. inv H3.
      assert (Hacc := H2).
      apply map_acc_xsimplify_le in Hacc.
      apply map_acc_bet with (x:= x) in H2.
      assert (Hsimp_le := H1).
      apply simpl_op_le in Hsimp_le.
      apply simpl_op_bet with (x:= x) in H1.
      intuition. apply mk_conj_bet ; try close  ; mauto.
      mauto.
      tauto.
      tauto.
      Case Discharge1 2.
      unfold F_le. intros. 
      eapply xsimplify_le ; eauto.
      Case Discharge2 2.
      unfold F_bet. intros.
      flip. eapply H ; eauto.
      mauto.
    Qed.

    Lemma map_acc_xsimplify_bet : forall l (args:Hlist.t (Trm positive) l)  x fr fr' eqs' args',
      (forall lt (e: Trm positive lt) , Hlist.In e args -> e ∈ [ x ; fr]) -> 
      (x <= fr)%positive -> 
      Hlist.map_acc  (fold_simplify xsimplify) args (fr,None) = (fr',eqs',args') -> 
      ((option_prop eqs') ∈ [x ; fr']) /\ (forall bt (e':Trm positive bt), Hlist.In e' args' ->  e' ∈ [ x ; fr']).
    Proof.
      intros.
      eapply map_acc_bet ; eauto.
      unfold F_le ; intros.
      eapply xsimplify_le ; eauto.
      unfold F_bet. intros.
      flip.
      eapply xsimplify_bet ; eauto.
    Qed.

  (** Prove the existence of an extended environment preserving the semantics of formulae *)

(*    Lemma abstract_expr_correct : forall r (e: Trm positive r) x fr fr' e' c env,
      e ∈ [x ; fr] -> 
      abstract_expr fr e = (fr',c,e') -> 
      exists env',
        (interp  env  e  == interp (merge_env fr  env env') e')
        /\ 
        (interp  (merge_env fr  env env')  (option_prop c)).
    Proof.
      intros until 0.
      intros Hb Hf.
      unfold abstract_expr in Hf.
      revert Hf.
      case_eq (is_constant e).
      * (* This is a constant *)
        intros. inv Hf.
        exists env. split ; auto.
        rewrite merge_env_env.        
        reflexivity.
      * (* This is not a constant *)
        intros Hc Hf.
        inv Hf.
        exists (update_env r env (xO fr)  (interp env  e)).
        split.
        simpl. rewrite Pcompare_refl.
      rewrite update_env_eq. reflexivity.
    (**)
      unfold option_prop.
      set (env' := (update_env r env fr~0  (interp env e))).
      assert (Hi := @interp_merge_env _ _ _ fr env env' Hb).
      unfold env' in *.
      revert Hi.
      unfold mk_eq. intros.
      revert Hi.
      clear env'. 
      intros. 
      simpl;   rewrite Pcompare_refl;   rewrite  update_env_eq. 
      rewrite <- Hi. reflexivity.
    Qed.


    Lemma map_acc_abstract_expr_correct : forall l (args:Hlist.t (Trm positive) l)  x fr fr' eqs' args',
      Ple x fr -> 
      (forall bt (e:Trm positive bt), Hlist.In e args -> e ∈ [x ; fr]) -> 
      Hlist.map_acc  (fold_simplify abstract_expr) args (fr, None) = (fr',eqs',args') -> 
      forall env, exists env',
        @Hlist.equiv _ (Trm positive) (Trm positive) (fun bt ei ei' => interp env ei == interp (merge_env fr env env') ei') l args args'
        /\
        interp (merge_env fr env env') (option_prop eqs').
    Proof.
      intros.
      apply map_acc_correct with (x:=x) (env := env) in H1;auto;
        [Case Ccorrect | Case Cle | Case Cbet].
      Case Ccorrect.
      unfold F_correct ; intros.
      eapply abstract_expr_correct ; eauto.
      Case Cle.
      unfold F_le ; intros.
      eapply abstract_expr_le ; eauto.
      Case Cbet.
      unfold F_bet.
      intros. 
      flip.
      eapply abstract_expr_bet ; eauto.
    Qed.
 *)
    
   Lemma   simplify_morph' : forall   o ,
      match simplify_op  o with
        | Id => True
        | Unfold f => True
        | Abstract l p => True
      end.
     Proof.
       intros.
       destruct (simplify_op o) ; auto.
       Qed.

     Fixpoint fresh_env (env : Env positive) (fr : positive) (l : list btyp)  (s : Hlist.t etyp l) :  Env positive :=
       match s with
         | Hlist.snil => env
         | Hlist.scons _ e _ s => 
            (@fresh_env (@update_env _ env (xO  fr)  e) (Psucc fr) _ s) 
       end.

     Lemma fresh_env_merge_env : 
       forall l s env fr ,
         @fresh_env env fr l s ≡ merge_env  fr env (@fresh_env env fr l s).
     Proof.
       induction s.
       simpl.
       intros.
       symmetry.
       apply merge_env_env.
       simpl.
       intros.
       rewrite IHs.
       rewrite <- merge_env_assoc ; mauto.
       rewrite <- update_merge_env ; mauto.
       rewrite merge_env_env.
       reflexivity.
     Qed.

     Fixpoint add_nat (p:positive) (n:nat) : positive :=
       match n with
         | O => p
         | S n => (add_nat (Psucc  p) n)
       end.

     Lemma add_nat_succ : 
       forall n p, add_nat (Pos.succ p) n = Psucc (add_nat p n).
     Proof.
       induction n.
       simpl. reflexivity.
       intros.
       simpl.
       rewrite IHn.
       reflexivity.
     Qed.

     Lemma add_nat_ge : forall n p, (add_nat p n >= p)%positive.
     Proof.
       induction n ;simpl.
       intros; mauto.
       zify.
       mauto.
       intros.
       rewrite add_nat_succ.
       generalize (IHn p).
       zify ;mauto.
     Qed.

     Lemma make_fresh_seq_fresh : 
       forall  l fr fr' s,     
         make_fresh_seq l fr = (fr',s) -> 
         (fr' = add_nat fr (List.length l))%positive.
     Proof.
       induction l ; simpl.
       * 
         intros.
         inv H.
         reflexivity.
       * 
         intros until 0.
         case_eq_match.
         intros.
         apply IHl in H.
         subst.
         inv H0.
         reflexivity.
     Qed.

    
     Lemma fresh_env_get : 
       forall  l args' env fr x bt,
         Ple (add_nat fr (List.length l)) x -> 
         @fresh_env env fr l args' (xO x) bt = env (xO x) bt.
     Proof.
       induction args' ; simpl.
       * 
       reflexivity.
       * 
       intros.
       destruct (EqTest.eq_dec x bt).
       subst.
       destruct (EqTest.eq_dec ( fr)~0 (x0~0))%positive.
       exfalso.
       rewrite add_nat_succ in H.
       mauto.
       revert H.
       generalize (add_nat_ge  (length lx) x0).
       simpl.
       lia.
       rewrite IHargs'.
       rewrite update_env_neq.
       reflexivity.
       mauto.
       auto.
       rewrite IHargs'.
       rewrite update_env_neq.
       reflexivity.
       mauto.
       auto.
     Qed.

     Lemma fresh_env_get_low : 
       forall  l args' env fr x bt,
         Plt x fr  -> 
         @fresh_env env fr l args' (xO x) bt = env (xO x) bt.
     Proof.
       induction args' ; simpl.
       * 
       reflexivity.
       * 
       intros.
       rewrite IHargs' ; mauto.
       rewrite update_env_neq.
       reflexivity.
       left ; mauto.
       lia.
     Qed.


     Lemma make_fresh_in : 
      forall  l fr ty x ,
        Hlist.In x (snd (make_fresh_seq l fr)) -> 
        exists i, x = Var ty (xO i) /\ (fr <= i < (add_nat fr (List.length l)))%positive.
     Proof.
       induction l ; simpl.
       intros.
       inv H.
       intros.
       revert H.
       case_eq_match.
       simpl.
       intros.
       apply Hlist.In_In_fix in H0.
       simpl in H0.
       destruct H0 as [HEq HH | HH].
       destruct HEq.
       subst.
       simpl in H0.
       subst.
       exists fr.
       intuition.
       rewrite add_nat_succ.
       generalize (add_nat_ge (length l) fr).
       simpl.
       lia.
       apply Hlist.In_In_fix' in HH.
       change t0 with (snd (p,t0)) in HH.
       rewrite <- H in HH.
       apply IHl in HH.
       destruct HH ; intuition.
       subst.
       exists x0.
       intuition.
       generalize (add_nat_ge (length l) fr).       
       rewrite add_nat_succ in H3.
       lia.
     Qed.
       
    Lemma make_fresh_seq_nth : 
      forall l fr n,
        Hlist.nth n (snd (make_fresh_seq l fr)) (Var bt_Prop (xO (add_nat fr n))) = Var _ (xO (add_nat fr n)).
    Proof.
      induction l.
      *
      simpl.
      destruct n ; simpl.
      reflexivity.
      reflexivity.
      *
        simpl.
        intros.
        case_eq_match.
        intros.
        simpl.
        destruct n ; simpl.
        reflexivity.
        rewrite <- IHl.
        rewrite H.
        simpl.
        reflexivity.
    Qed.
       
     Lemma seq_equi_map_morph : 
       forall l (s:Hlist.t (Trm positive) l) s' f f',
       (forall t (x:Trm positive t), Hlist.In x s -> f _ x == f' _ x) -> 
         Hlist.equiv equi_typ (Hlist.map f s) s' -> 
         Hlist.equiv equi_typ (Hlist.map f' s) s'.
     Proof.
       induction s ; intros ;  rewrite (Hlist.make_seq_id s')  in *; simpl.
       *
       constructor.
       *
         apply Hlist.equiv_fix in H0.
         simpl in H0.
         destruct H0.
         unfold eq_rect_r in *.
         simpl in *.
         constructor.
         apply IHs with (f:=f); auto.
         intros.
         apply H.
         constructor ; auto.
         apply Hlist.equiv_fix' ; auto.
         rewrite <- H0.
         symmetry.
         apply H.
         constructor.
     Qed.

     Lemma seq_eq : 
       forall TY (f:TY -> Type) (ty : TY) (x x': f ty) ly (lx lx': Hlist.t f ly),
         x = x' -> lx = lx' -> 
         Hlist.scons x  lx = Hlist.scons x'  lx'.
     Proof.
       intros.
       subst.
       reflexivity.
     Qed.

     Lemma vars_between_map_Cst :
       forall (x : btyp) (lt : list btyp) (args : Hlist.t etyp lt)  (e: Trm positive x)  l h
              (LH : (l <= h)%positive)
              (IN : Hlist.In e (Hlist.map (fun (bt : btyp) (c : etyp bt) => Cst bt c) args)),
         e ∈  [l; h].
     Proof.
       unfold vars_between.
       split ; auto.
       intros v VARS.
       exfalso.
       apply Hlist.In_In_fix in IN.
       revert IN.
       revert VARS.
       clear.
       induction args ; simpl.
       - auto.
       - intros.
         destruct IN.
         + destruct H.
           unfold eq_rect in H.
           subst.
           subst.
           simpl in VARS. auto.
         +  eapply IHargs ; eauto.
     Qed.


     Lemma simpl_op_correct : forall o args x fr fr' e' c env,
      (x <= fr)%positive -> 
      (forall bt (ei:Trm positive bt), Hlist.In ei args -> ei ∈ [x ; fr]) -> 
      @simpl_op fr o args = (fr',c,e') -> 
      exists env',
        (interp env  (Op o args)  == interp  (merge_env fr  env env')  e')
        /\ 
        (interp  (merge_env fr  env env')  (option_prop c)).
    Proof.
      intros until 0.
      intros Hle Hargs Hsimp.
      assert (Hbet := @simpl_op_bet o  _ _ _ _ _ _ Hle Hargs Hsimp).
      unfold simpl_op in *.
      generalize (simplify_correct  o ).
      generalize (simplify_morph  o ).
      generalize (simplify_same_vars o).
      inj_typ o.
      destruct (@simplify_op simpl_spec  o) ; [Case Unfold | Case Abstract | Case Id].
      Case Unfold.
      intros Hsame Hm Heq.
      inv Hsimp.
      exists env. 
      repeat  rewrite interp_merge_env_env.
      rewrite Heq. split. reflexivity. auto.
      Case Abstract.
      intros Hsame Hm Hp.
      revert Hsimp.
      inj_typ o.
      (*case_eq_match.*)
      (*    case_eq (Hlist.map_acc (fold_simplify abstract_expr) args (fr, None)).*)
      (*destruct a as [fr2 eqs']. *)
      case_eq_match.
      intros.
      inv Hsimp.
      (*assert (H_correct := H0).
      apply map_acc_abstract_expr_correct with (x:= x) (env:= env)in H_correct ; [idtac | mauto |mauto].
      destruct H_correct as [env1 [Hargs' Heqs]]. *)
      set (envr := update_env 
                     (op_ret o)  
                     env (*(merge_env fr env env1) *)
                     (xO fr)  (interp env (Op o args))).
      assert (Hop_eq : interp env (Op o args) == interp envr (Var _ (xO fr))).
      {
        simpl.
        unfold envr.
        rewrite update_env_eq.
        simpl.
        reflexivity.
      }
      assert (H_bet : Op o args ∈ [x ; fr]).
      {
        rewrite vars_between_eq.
        auto.
      }
      assert (H_fresh_le := H).
       apply make_fresh_seq_le in H_fresh_le.
(*      assert (acc_le := H0).
      assert (acc_le' := acc_le).
      apply map_acc_abstract_expr_le in H0. *)
(*      assert (Hop_eq_t1 : interp envr (Op o args) == interp envr (Op o t1)).
      {
        unfold envr.
        rewrite <- (@interp_update _ _ x fr);[ |mauto | mauto].
        rewrite <- (@interp_update _ _ x fr2);[ |  | mauto ].
        rewrite <- interp_merge_env ; mauto.
        simpl.
        apply op_morph ; auto.
        apply Hlist.equiv_map ; auto.
        apply H_bet.
        apply map_acc_abstract_expr_bet with (x:=x) in acc_le.
        rewrite vars_between_eq.
        split. mauto.
        intuition.
        mauto.
        auto.
      } *)
      assert (Hop_eq' : interp envr (Op o args) == interp envr (Var _ (xO fr))).
      {
        (*rewrite <- Hop_eq_t1.*)
        rewrite <- Hop_eq.
        unfold envr.
        rewrite <- interp_update.
        (*rewrite <- interp_merge_env.*)
        reflexivity.
        apply H_bet.
        (*apply H_bet.*)
        mauto.
      }
      destruct (Hp _ _ _ Hop_eq') as [args' Hp']. 
      set (envr' := merge_env fr envr (@fresh_env envr (Pos.succ fr) l  args')).
      change ((TrmWithProp.op_ret syntax_arg o)) with (op_ret o).
      exists envr'.
      split. (*;[| apply interp_mk_conj].*)
      * unfold interp at 2.
      rewrite merge_env_Plt ; mauto.
      unfold envr'.
      rewrite merge_env_Plt ; mauto.
      assert (Hr := fresh_env_merge_env args' envr (Pos.succ fr) (fr~0)%positive (op_ret o)).
      rewrite Hr.
      rewrite merge_env_Ple ; mauto.
      *
(*       (* assert (Heqb : option_prop eqs' ∈  [x; fr2]).
        {
          apply map_acc_abstract_expr_bet with (x:=x) in acc_le ; auto.
          tauto.
        } *)
        +
          unfold envr'.
          rewrite <- merge_env_assoc ; mauto.
          rewrite <- interp_merge_env ; eauto.
          rewrite <- update_merge_env ; mauto.
          rewrite <- (@interp_update _ _ x fr); mauto.
          (*rewrite <- merge_env_assoc ; mauto.*)
          rewrite merge_env_env.
          unfold option_prop.
     * *)
       unfold option_prop.
       rewrite Hm with (args2 := Hlist.map (typ':= Trm positive) (fun bt v => Cst _  v) args').
         assert (
             Hp_bet : 
               p (Var (op_ret o) (fr~0)%positive) args
                 (Hlist.map (fun (bt : btyp) (c : etyp bt) => Cst bt c) args') ∈  [x; Psucc fr]).
         {
           apply vars_of_pred_vars_between; mauto.
           apply vars_between_Var. mauto.
           intros.
           apply Hargs in H0.
           apply vars_between_le with (3:= H0); mauto.
           intros.
           eapply vars_between_map_Cst ; eauto.
           mauto.
         }
         unfold envr'.
         rewrite <- merge_env_assoc; mauto.
         rewrite fresh_env_merge_env.
         rewrite <- merge_env_assoc ; mauto.
         rewrite <- interp_merge_env with (x:=x) ; auto.
         rewrite merge_env_assoc ; mauto.
         rewrite merge_env_env.
         unfold envr.
         rewrite <- update_merge_env ; mauto.
(*         rewrite <- merge_env_assoc ; mauto.*)
         rewrite merge_env_env.
         apply Hp'.
         apply Hlist.map_equiv.
         unfold envr'.
         match goal with 
           | |- ?F ?X ?Y => 
               assert (Hinterp_args' : Y = Hlist.map (fun b x => x) args')
         end.
         {
           generalize (merge_env fr env
           (merge_env fr envr (fresh_env envr (Pos.succ fr) args'))).
           clear.
           intros.
           rewrite Hlist.map_map.
           induction args' ; simpl.
           reflexivity.
           intros.
           simpl in IHargs'.
           rewrite IHargs'.
           reflexivity.
         }
         rewrite Hinterp_args'.
       clear Hinterp_args'.
       change t0 with (snd (fr',t0)).
       rewrite <- H.
       apply seq_equi_map_morph with (f:= fun bt x0 => interp (fresh_env envr (Pos.succ fr) args') x0).
       -
         intros.
         apply make_fresh_in in H0.
         destruct H0.
         destruct H0.
         subst.
         unfold interp.
         rewrite merge_env_Plt ;  mauto.
         rewrite merge_env_Plt ;  mauto.
         reflexivity.
      -
        generalize (@make_fresh_seq_nth l (Pos.succ fr)).
        generalize ((snd (make_fresh_seq l (Pos.succ fr)))).
        intros.
        assert (     
            (Hlist.map
               (fun (bt : TrmWithProp.btyp btyp_u) (x0 : Trm positive bt) =>
                  interp (fresh_env envr (Pos.succ fr) args') x0) t1)
              =
              (Hlist.map (fun (b : btyp) (x0 : etyp b) => x0) args')).
        {
          rewrite (Hlist.make_seq_id  args').
          revert H0. clear Hop_eq  Hop_eq' Hp' envr'.
          generalize envr.
          clear.
          generalize (Pos.succ fr).
          induction t1 ; simpl.
          +
            reflexivity.
          +
          intros.
          apply seq_eq.
          -
          generalize (H0 O).
          simpl.
          intro.
          subst.
          simpl.
          rewrite fresh_env_get_low ; mauto.
          rewrite update_env_eq.
          reflexivity.
          -
          rewrite IHt1.
          reflexivity.
          intro.
          generalize (H0 (S n)).
          simpl.
          auto.
        }
        rewrite <- H1.
        apply Hlist.in_equi.
        reflexivity.
        * 
      intros. inv Hsimp. exists env.
      rewrite merge_env_env. split.
      reflexivity. auto.
    Qed.

    Lemma xsimplify_correct : forall r (e : Trm positive r) x fr fr' e' c env,
      e ∈ [x ; fr ] -> 
      xsimplify fr e = (fr',c,e') -> 
      exists env',
        (interp env  e  == interp  (merge_env fr  env env') e')
        /\ 
        (interp (merge_env fr  env env')  (option_prop c)). 
    Proof.
      induction e using expr_ind ;  [Case CCst | Case CVar | Case COp].
      Case CCst.
      intros.
      exists env.
      inv H0.
      simpl. split. reflexivity.  auto.
      Case CVar.
      intros.
      simpl in H0. inv H0.
      exists env.
      rewrite merge_env_env.
      simpl.
      split. reflexivity. auto.
      Case COp.
      intros until 0.
      simpl.
      inj_typ o.
      case_eq_match. 
      (*case_eq (Hlist.map_acc  (fold_simplify xsimplify) args
        (fr, None) ). *)
      destruct a as [fr2 eqs'].
      intro args'.
      intro Hacc.
      case_eq (simpl_op fr2 o args').
      destruct a as [frr err].
      intros. inv H2.
      assert (Hargs_bet :  
        forall (bt : btyp) (e : Trm positive bt), Hlist.In e args -> e ∈  [x; fr]).
      rewrite vars_between_eq in H1. tauto.
      assert (Hacc' := Hacc).
      assert (F_le xsimplify args).
      unfold F_le ; intros.
      eapply xsimplify_le ; eauto.
      assert (Hxfr : (x <= fr)%positive).
      unfold vars_between in H1 ; tauto.
      assert (Hfrfr2 := Hacc).
      apply map_acc_le in Hfrfr2; auto.
      assert (Hcorrect :F_correct xsimplify args).
      unfold F_correct ; intros. 
      eapply H ; eauto. 
      assert (Hbt : F_bet xsimplify args x).
      unfold F_bet. 
      intros. flip.
      eapply xsimplify_bet ; eauto.
      assert (Hargs'_bet : forall (bt : btyp) (ei : Trm positive bt),
        Hlist.In ei args' -> ei ∈ [x ; fr2 ]).
      intros.
      apply map_acc_xsimplify_bet with (x:= x) in Hacc;mauto.
      apply map_acc_correct with (x:=x) (env := env) in Hacc' ; auto.
      destruct Hacc' as [env1 [Hargs Heq']].
      assert (H0_correct := H0).
      apply simpl_op_correct with (env:= merge_env fr  env env1) (x:=x) in H0_correct 
        ; [idtac |mauto|mauto].
      destruct H0_correct as [env2 [He' Herr]].
      exists (merge_env fr2 env1 env2).
      split.
      rewrite <- merge_env_assoc ; auto.
      rewrite <- He'.
      simpl.
      assert (HH :=  op_morph o).
      unfold is_morph in HH.
      inj_typ o. apply HH.
      apply Hlist.equiv_map;auto.
      Case Eqs 1.
      apply interp_mk_conj.
      rewrite <- merge_env_assoc ; auto.
      rewrite <- interp_merge_env; auto.
      apply map_acc_bet with (x:=x)  in Hacc ; auto.
      flatten. apply Hacc_l.
      rewrite <- merge_env_assoc;      mauto.
    Qed.

    Lemma inj_trm_bet : forall r (e : Trm positive r) v,  In (v~0)%positive (vars  (inj_trm e)) -> False.
    Proof.
      induction e using expr_ind ; [Case CCst | Case CVar | Case COp];
        simpl ; try tauto.
      intro. intuition congruence.
      intros.
      apply inj_vars in H0.
      revert H0.
      revert H.
      revert args.
      inj_typ o.
      generalize (op_args o).
      induction args ; simpl.
      *
      tauto.
      *
      intros. in_app.
      destruct H0.
      + eapply H ; eauto. constructor.
      + eapply IHargs ; eauto.
      intros. eapply H with (2:= H2) ; eauto.
      constructor;auto.
    Qed.
    
    Lemma collect_cst_bet : forall r (e : Trm positive r) v, In (v~0)%positive (vars  (option_prop (xcollect_cstr e))) -> False.
    Proof.
      induction e using expr_ind ; [Case CCst | Case CVar | Case COp];
        simpl ; try tauto.
      Case CVar.
      case_eq (type_cstr bt v) ; intros.
      simpl in H0.
      revert H0.  exact (inj_trm_bet t0 v0).
      simpl in H0. auto.
      Case COp.
      revert H. revert args.
      inj_typ o. generalize (op_args o).
      induction args ; simpl.
      Case CNil 1.
      intros. auto.
      Case CCons 1.
      intros.
      rewrite option_prop_mk_conj in H0.
      in_app.
      destruct H0. eapply H ; eauto. constructor.
      eapply IHargs ; eauto.
      intros ; eapply H ; eauto.
      constructor ; auto.
    Qed.

    Lemma cstr_of_var_bet : 
      forall x a e,
        cstr_of_var a = Some e -> In (x~0)%positive (vars e) -> False.
    Proof.
      intros until 0.
      destruct a.
      simpl.
      case_eq (type_cstr b p).
      simpl ; intros.
      inv H0.
      eapply inj_trm_bet with (e := t0) ; eauto.
      simpl ; discriminate.
    Qed.

    Lemma In_vars_mk_conj : 
      forall e1 e2 (x:positive),
        In x (vars (option_prop (mk_conj e1 e2)))  -> In x (vars (option_prop e1)) \/ In x (vars (option_prop e2)).
    Proof.
      destruct e1 ; destruct e2 ; simpl ; intros ; in_app ; simpl in * ; intuition.
    Qed.


    Lemma typ_cstr_of_vars_bet : forall x l, In (x~0)%positive (vars (typ_cstr_of_vars l)) -> False.
    Proof.
      unfold typ_cstr_of_vars.
      induction l ; auto.
      simpl.
      intros.
      apply IHl.
      apply In_vars_mk_conj in H.
      destruct H.
      * exfalso.
        revert H.
        case_eq (cstr_of_var a).
        simpl.
        apply cstr_of_var_bet.
        simpl. tauto.
      * auto.
    Qed.



    Lemma inj_trmt_bet : forall (e : Trm positive bt_Prop), inj_trmt bt_Prop e ∈  [1; 1].
    Proof.
      intros.
      unfold inj_trmt.
      unfold vars_between.
      split ; mauto.
      simpl.
      intros ; in_app.
      exfalso. destruct H.
      eapply collect_cst_bet ; eauto.
      simpl in H.
      destruct H;auto.
      apply inj_trm_bet with (r:= bt_Prop) (v:=x) (1:= H).
    Qed.
    

    Lemma simplify_lem : forall (e:Trm positive bt_Prop), 
      (forall env, interp env (simplify (inj_trmt bt_Prop e)))  -> forall env, interp env e.
    Proof.
      unfold simplify.
      intro.
      case_eq (xsimplify 1 (inj_trmt bt_Prop e)).
      destruct a.
      intros.
      apply inj_trmt_correct.
      intros.
      apply xsimplify_correct with (env := env0) (x:= xH) in H.
      destruct H as [env' [He Hc]].
      rewrite He.
      assert (H0' := H0 (merge_env xH  env0 env')).
      simpl in H0'.
      apply H0'; auto.
      apply inj_trmt_bet.
    Qed.
      


    Lemma simplify_sharing_lem : forall (e:Trm positive bt_Prop) (l:list (btyp * positive)), 
      (forall env, interp env (simplify (impl_expr (typ_cstr_of_vars l) (inj_trm  e))))  -> forall env, interp env e.
    Proof.
      unfold simplify.
      intros e l.
      intro.
      revert H.
      case_eq (@xsimplify xH (@None btyp_u) (impl_expr (typ_cstr_of_vars l) (inj_trm e))).
      destruct a.
      intros.
      apply inj_trmt_sharing_correct with (l:=l).
      intros env0.
      apply xsimplify_correct with (env := env0) (x:= xH) in H.
      destruct H as [env' [He Hc]].
      rewrite He.
      assert (H0' := H0 (merge_env xH  env0 env')).
      simpl in H0'.
      apply H0'; auto.
      apply mk_impl_bet.
      unfold vars_between.
      split. mauto.
      intros.
      exfalso.
      eapply typ_cstr_of_vars_bet ; eauto.
      unfold vars_between.
      split. mauto.
      intros.
      exfalso.
      apply  (@inj_trm_bet _ e x H1).
    Qed.

    Lemma simplify_sharing_lem_with_env : 
      forall (e:Trm positive bt_Prop) (l:list (btyp * positive)), 
      forall env, 
      (forall env' , 
         interp (merge_env xH (inj_env env) env') (simplify (impl_expr (typ_cstr_of_vars l) (inj_trm  e))))  -> 
      interp env e.
    Proof.
      unfold simplify.
      intros e l env.
      case_eq (@xsimplify xH (@None btyp_u) (impl_expr (typ_cstr_of_vars l) (inj_trm e))).
      destruct a.
      intros.
      apply inj_trmt_sharing_correct_keep_env with (l:=l).
      apply xsimplify_correct with (env := (inj_env env)) (x:= xH) in H.
      destruct H as [env' [He Hc]].
      rewrite He.
      simpl in H0.
      apply H0.
      auto.
      apply mk_impl_bet.
      unfold vars_between.
      split. mauto.
      intros.
      exfalso.
      eapply typ_cstr_of_vars_bet ; eauto.
      unfold vars_between.
      split. mauto.
      intros.
      exfalso.
      apply  (@inj_trm_bet _ e x H1).
    Qed.

  

  End S.

End Make.



(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
