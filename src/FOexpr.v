Require Import List.
Require Import FOLib.
Require Import FOLType.
Require Import Hlist.
Set Boolean Equality Schemes.
Set Implicit Arguments.

Module TrmSyntax.
  (** The module  [TrmSyntax] defines a class [t] 
     which fields are the necessary components to define the syntax of (typed) expressions.
   **)

  Class t := 
    Mk {
        btyp    : Type;
        etyp    : btyp -> Type;
        op      : Type;
        op_args : op -> list btyp;
        op_ret  : op -> btyp
      }.

  Module Arg.
    Section S.
      Variable btyp : Type.
      Variable op   : Type.
      
      Class t := 
        Mk {
            op_args : op -> list btyp;
            op_ret  : op -> btyp
          }.

    End S.

  End Arg.
    
  Definition Make (bt:Type) (etyp: bt -> Type) (op:Type) (args : @Arg.t bt op) : t :=
    Mk  etyp  (@Arg.op_args _ _ args) (@Arg.op_ret _ _ args).

End TrmSyntax.




Import TrmSyntax.

Module TrmSemantics.
  (* The module [TrmSemantics] defines a functor [t]
     which takes as input a class of type [TrmSyntax.t] 
     and produces an evaluation function for operators.
   *)

  Class t (Syntax : TrmSyntax.t) :=
    Mk {
        op_eval : forall (o:op) , eval_typ etyp (op_args o) (op_ret o) 
      }.
  
End TrmSemantics.
Import TrmSemantics.

Section S.



  Variable expr_synt : TrmSyntax.t.

  Variable var : Type.
  
  Inductive Trm : btyp -> Type :=
  | Cst   : forall (bt:btyp) (v:etyp bt) , Trm bt
  | Var   : forall (bt:btyp) (v:var), Trm  bt
  | Op    : forall (o:op) (args: Hlist.t  Trm (op_args o)), Trm (op_ret o).

  Fixpoint vars  (bt:btyp) (e:Trm bt) : list var :=
    match e with
      | Cst _ _ => nil
      | Var _ x => x ::nil
      | Op o a  => Hlist.concat_map_list  vars  a
    end.


  Section Induction.


    Require Import Max.

    Fixpoint depth (bt:btyp) (e:Trm bt) : nat :=
      match e with
        | Cst _ _ => O
        | Var _ _ => O
        | Op o args => S (Hlist.fold_right (fun (bt:btyp) (e:Trm bt) (n:nat) => max n (depth e)) args O)
      end.


    Require Import Wf_nat.

    Lemma depth_lt : forall (lt:list btyp) (args : Hlist.t  Trm lt) (bt : btyp) (x:Trm bt),
      Hlist.In x  args -> 
      depth x <
      S
      (Hlist.fold_right
        (fun (bt : btyp) (e : Trm bt) (n : nat) => max n (depth e)) args
        0).
    Proof.
      intros.
      induction H.
      simpl.
      generalize (        (Hlist.fold_right
           (fun (bt0 : btyp) (e : Trm bt0) (n : nat) => max n (depth e)) s 0)).
      intros. 
      generalize (max_spec n (depth e)).
      omega.
      simpl.
      revert IHIn.
      generalize (     (Hlist.fold_right
        (fun (bt0 : btyp) (e : Trm bt0) (n : nat) => max n (depth e)) s 0)).
      intros.
      generalize (max_spec n (depth e')).
      omega.
    Qed.


    Variable P : forall (bt:btyp), Trm bt -> Prop.

    Variable P_Cst : forall (bt:btyp) (v: etyp bt), P (Cst _ v).

    Variable P_Var : forall (bt:btyp) (v: var), P (Var bt v).

    Variable P_Op : forall (o : op) (args :Hlist.t Trm (op_args o)) , 
      (forall bt (x: Trm bt), Hlist.In x  args -> P x) ->  P (@Op o args).


    Lemma expr_ind : forall (bt:btyp) (e: Trm bt), P  e.
    Proof.
      intros.
      add_eq (depth e) n.
      intro.
      revert H. revert e. revert bt.
      intro.
      revert bt.
      induction n using Wf_nat.lt_wf_ind.
      destruct n ; [ Case Zero | Case Succ].
      Case Zero.
      destruct e; [Case CCst 1 | Case CVar 1 | Case COp 1] ; simpl ; intros; auto.
      discriminate.
      Case Succ.
      destruct e; [Case CCst 1 | Case CVar 1 | Case COp 1] ; simpl ; intros ; try discriminate.
      inv H0.
      apply P_Op.
      intros.
      eapply H.
      apply depth_lt; auto. apply H0.
      reflexivity.
    Qed.



  End Induction.


  Section InterpFun.

    Inductive FunTrm  : Type :=
    | FunCst : forall (bt: btyp) (v : etyp bt), FunTrm
    | FunArg : forall  (n :nat), FunTrm 
    | FunOp  : forall (o:op), FunTrm
    | FunList : forall (o:op)  (args : list FunTrm), FunTrm.
    

    Fixpoint fundepth (e:FunTrm) : nat :=
      match e with
        | FunCst _ _  => O
        | FunArg _   => O
        | FunOp o  => O
        | FunList o  argl => 
          S (List.fold_right (fun  (e:FunTrm) (n:nat) => max n (fundepth e))  O argl)
      end.

    Require Import Wf_nat.

    Lemma fundepth_lt : forall  (args : list FunTrm) (x:FunTrm),
      List.In x  args -> 
      fundepth x <
      S
      (List.fold_right
        (fun (e : FunTrm) (n : nat) => max n (fundepth e)) 0 args).
    Proof.
      intros until 0.
      induction args; simpl ; intros.
      tauto.
      revert IHargs.
      generalize ((List.fold_right (fun (e : FunTrm) (n : nat) => max n (fundepth e)) 0
         args)).        
      intros. 
      generalize (max_spec n (fundepth a)).
      destruct H.
      subst ; omega.
      apply IHargs in H. omega.
    Qed.


    Variable P : FunTrm  -> Prop.

    Variable P_Cst : forall  (bt:btyp) (v: etyp bt),    P (FunCst bt v).

    Variable P_FunArg : forall  (n:nat) , P (FunArg n ).

    Variable P_FunOp  : forall (o:op), P (FunOp o).

    Variable P_FunList : forall (o : op)  (args :list FunTrm),
      (forall  x: FunTrm, List.In x  args -> P x) ->  P (@FunList o args). 

    Lemma funexpr_ind : forall  (e: FunTrm), P  e.
    Proof.
      intros.
      add_eq (fundepth e) n.
      intro.
      revert H. revert e. 
      induction n using Wf_nat.lt_wf_ind.
      destruct n ; [ Case Zero | Case Succ].
      Case Zero.
      destruct e; [Case CCst 1 | Case CArg 1 | Case COp 1 | Case CFun 1] ; simpl ; intros; auto.
      discriminate.
      Case Succ.
      destruct e; [Case CCst 1 | Case CArg 1 | Case COp 1 | Case CFun 1] ; simpl ; intros; auto.
      inv H0.
      apply P_FunList.
      intros.
      eapply H.
      apply fundepth_lt; auto. apply H0.
      reflexivity.
    Qed.

  End InterpFun.



  Section Interp.


    Definition Env := var -> forall (bt:btyp) , etyp bt.


    Variable expr_sem : TrmSemantics.t  expr_synt.    

    Fixpoint interp (r : btyp) (env:Env) (e : Trm r) : etyp r :=
      match e with
        | Cst _ v => v
        | Var bt v => env v bt
        | Op o a =>
          interp_seq  (Hlist.map    (fun bt => @interp bt env)  a) _ (op_eval o)
    end.

    Lemma interp_rew : forall (r : btyp) (env:Env) (e : Trm r),
      interp env e =       
      match e  with
        | Cst _ v => v
        | Var bt v => env v bt
        | Op o a =>
          interp_seq  (Hlist.map    (fun bt => @interp bt env)  a) _ (op_eval o)
    end.
    Proof.
      destruct e ; reflexivity.
    Qed.

  End Interp.

  Section Map.

    Variable f : forall (r:btyp), var -> Trm r.

    Fixpoint map (r:btyp) (e:Trm r) : Trm r:=
      match e with
        | Cst _ v => Cst _ v
        | Var bt v => f bt v
        | Op o l   => Op o (Hlist.map map l)
      end.

  End  Map.


End S.

Arguments Trm [expr_synt] var btyp.
Arguments Cst  [expr_synt] [var] bt v.
Arguments Var  [expr_synt] [var] bt v.
Arguments Op   [expr_synt] [var] o args.
Arguments interp [expr_synt] [var] [expr_sem] [r] env e.

(*Implicit Arguments FunTrm [ btyp_t].
Implicit Arguments FunCst [btyp op btyp_t].
Implicit Arguments FunArg [btyp op  btyp_t].
Implicit Arguments FunOp [btyp op btyp_t].
Implicit Arguments FunList [btyp op btyp_t].
*)

Arguments interp_seq [btyp] [etyp] [l] args [r] _.


(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
