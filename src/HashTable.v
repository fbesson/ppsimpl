Require Import ZArith.
Require Import FMapPositive.
Open Scope type_scope.

Section S.
  Variable btyp : Type.
  Variable btyp_dec : forall (x y:btyp), {x = y} + {x <> y}.
  
  Variable key : btyp -> Type.
  Variable key_eq_dec : forall (bt : btyp) (x y: key bt), {x = y} +{x <> y}.

  Variable elt : btyp -> Type.


  (* We make the assumption that we have a hash function -- provided by Ltac *)

  Definition entry := {x : btyp & (key x * elt x)}.
  
  Definition entry_eqb (x y : entry) : bool:=
    match x , y with
      | existT bt (k,e) , existT bt' (k',e') => 
        match btyp_dec bt bt' with
          | left Heq  => if key_eq_dec bt k (eq_rect_r _ k'  Heq) then true else false
          | right _ => false
        end
    end.
  
  Definition entry_make (bt : btyp) (k : key bt) (e : elt bt) : entry :=
    existT _ bt (k,e).
  
  Require Import List.
  Open Scope list_scope.

  Definition bucket := list entry.
  
  Fixpoint has_entry (e : entry) (l : bucket) := List.existsb (entry_eqb e) l.

  Definition add_entry (e:entry) (l:bucket) :=
    if has_entry e l then l
    else (e::l).

  Import PositiveMap.

  Definition t := PositiveMap.t bucket.

  Definition empty : t := @PositiveMap.empty bucket.


  Definition add (i : positive) (e: entry) (m : t) : t :=
    match PositiveMap.find i m with
      | None => PositiveMap.add i (e::nil) m
      | Some l => if has_entry e l
                  then m
                  else PositiveMap.add i (e::l) m
    end.
        
  Fixpoint find_key (bt : btyp) (k : key bt) (l:list entry) : option (elt bt) :=
    match l with
      | nil => None
      | cons e l => 
        match e with
          | existT bt' (k',e') => 
            match btyp_dec bt bt' with
              | left Heq => if key_eq_dec bt k (eq_rect_r _ k' Heq ) 
                            then Some (eq_rect_r _ e' Heq)
                            else find_key bt k l
              | right _  => find_key bt k l
            end
        end
    end.
            
  Definition find (i : positive) (bt : btyp) (k : key bt) (m : t) : option (elt bt) :=
    match PositiveMap.find i m with
      | None => None
      | Some l =>  find_key bt k l
    end.




End S.

  
    

