(* A Theory of dependent lists *)
Require  List.
Set Implicit Arguments.
Require Import Utf8.
Require Import FOLib.


Section Seq.
  
  Variable A : Type.
  Variable typ : A -> Type.
  
  Inductive t : list A -> Type :=
  | snil  : t nil
  | scons : 
    forall (x : A) (e : typ x) (lx : list A) (le : t  lx),
      t (x::lx).


  Lemma list_discr : forall (x:A) lx, nil = (x :: lx)%list -> False.
  Proof.
    discriminate.
  Qed.

  Definition inj_car (x:A) (lx : list A) (e:A) (l : list A) :   (x :: lx)%list = (e :: l)%list -> x =e.
  Proof.
    intro. injection H.
    auto.
  Defined.

  Definition inj_cdr (x:A) (lx : list A) (e:A) (l : list A) :   (x :: lx)%list = (e :: l)%list -> lx =l.
  Proof.
    intro. injection H.
    auto.
  Defined.

  Definition car (x:A) (lx : list A) (s: t (x::lx)) : typ x :=
    match s in t l' with
    | snil => False
    | scons _ e _ _ => e
    end.

  Definition cdr (x:A) (lx : list A) (s: t (x::lx)) : t lx :=
    match s in t l' with
    | snil => False
    | scons _ _ _ lle => lle
    end.

  Fixpoint make_seq (l:list A)  : t l -> t l :=
      match l as l' return t l' -> t l' with
        | nil => fun s => snil
        | cons e l => fun s => scons (car s) (make_seq  (cdr s))
      end.

  Lemma make_seq_id : forall l (s: t l), s  = make_seq s.
  Proof.
    intros.
    induction s.
    * reflexivity.
    * simpl.
      rewrite  <- IHs.
      reflexivity.
  Qed.




  Definition nil_snil (s: t nil) : s = snil. 
  Proof.
    rewrite (make_seq_id s).
    compute. reflexivity.
  Qed.

  
  Definition cons_scons (x: A) (lx: list A) (l: t (x :: lx)) :
    ∃ (hd : typ x) (tl: t lx), l = scons hd tl.
  Proof.
    intros.
    rewrite (make_seq_id l).
    simpl.
    exists (car l).
    exists (cdr l).
    rewrite <- make_seq_id.
    reflexivity.
  Qed.

  Lemma scons_eq_inv (x: A) (a a': typ x) (lx: list A) (l l': t lx) :
    scons a l = scons a' l' →
    a = a' ∧ l = l'.
  Proof (λ H, conj (f_equal (@car x lx) H) (f_equal (@cdr x lx) H)).

  Definition car_cdr_typ   (x:A) (lx : list A) (s : t (x:: lx)) := { res : typ x * t lx | scons (fst res) (snd res) = s }.

  Definition car_cdr (x:A) (lx : list A) (s : t  (x::lx)) : @car_cdr_typ x lx s :=
    match s in t l' with
    | snil => False
    | scons x0 xe llx lle => exist _ (xe, lle) eq_refl
    end.

  Fixpoint nth (n:nat) : 
    forall (lt: list A), t lt ->  forall (x:A) (e:typ x), typ (List.nth n lt x) :=
  match
    n as n0 return (forall lt : list A, t lt -> forall x : A, typ x -> typ (List.nth n0 lt x))
  with
  | 0 => fun lt seq x e =>
           match seq in (t l) return typ (List.nth 0 l x) with
             | snil => e
             | scons _ e _ _ => e
           end
  | S n0 => fun (lt : list A) (seq : t lt) x e =>
              match seq in (t l) return typ (List.nth (S n0) l x) with 
                | snil => e
                | scons _ _ lx seq0 => nth n0  seq0  e
              end
  end.

  Fixpoint app (lt lt': list A) (s : t lt) :  t lt' ->  t (lt ++ lt') :=
    match s in (t l) return (t lt' -> t (l ++ lt')) with
      | snil => fun s' => s'
      | scons x e lx s0 => fun s' => scons e (app  s0 s')
    end.

  Fixpoint rev (lt : list A) (s : t lt) : t (List.rev lt) :=
    match s in (t l) return t (List.rev l) with
      | snil => snil
      | scons x e lx s' => app (rev s') (scons e snil)
    end.


(*

  Definition nth  (n:nat) :  forall (lt : list A) (seq : t  lt) (x:A) (e: typ x), typ (List.nth n lt x).
  Proof.
    induction n ; [Case N0 | Case Succ].
    intros.
    destruct seq.
    exact e.
    exact e0.
    Case Succ.
    intros.
    destruct lt.
    simpl. exact e.
    simpl. apply IHn.
    destruct (car_cdr seq).
    destruct x0 ; simpl in *.
    exact t1.
    auto.
  Defined.
(*
  Fixpoint nth  (n:nat) :  forall (lt : list A) (seq : t  lt) (x:A) (e: typ x), typ (List.nth n lt x) :=
    match n as n' return forall (lt : list A) (seq : t  lt) (x:A) (e: typ x), typ (List.nth n' lt x) with 
      | O => fun lt seq => 
        match seq as seq' in t lt' 
          return forall (x:A) (e: typ x), typ (List.nth O lt' x)with
          | snil => fun x e => e
          | scons x e lx _ => fun _ _ => e
        end
      | S n => fun lt seq => 
        match seq as seq' in t lt' 
          return forall (x:A) (e: typ x), typ (List.nth (S n) lt' x) with
          | snil => fun x e => e
          | scons x e lx les => fun x e => nth n  les  e
        end
    end.
*)
*)
(*  Definition car (x:A) (l : list A) (s : t (cons x l)) : typ x :=
    match car_cdr s with
      | exist v _ => fst v
    end.

  Definition cdr (x:A) (l : list A) (s : t (cons x l)) : t l :=
    match car_cdr s with
      | exist v _ => snd v
    end.
*)



  Lemma car_inj : 
    forall x (xe xe' : typ x) lx (s s' : t  lx), 
      scons xe s = scons xe' s' -> xe = xe'.
  Proof.
    intros.
    change (car  (scons xe s) = car  (scons xe' s')).
    rewrite H.
    simpl.
    reflexivity.
  Qed.

  Lemma cdr_inj : 
    forall x (xe xe' : typ x) lx (s s' : t  lx), 
      scons xe s = scons xe' s' -> s = s'.
  Proof.
    intros.
    change (cdr  (scons xe s) = cdr  (scons xe' s')).
    rewrite H.
    simpl.
    reflexivity.
  Qed.

  Opaque car cdr.


  Section FromList.
    Variable B : Type.
    Variable dtyp : forall (x:A), typ x.
    Variable F : B -> forall (x : A) , typ x.


    Fixpoint default (lt:list A) : t lt :=
      match lt with
        | nil => snil
        | cons e l => scons (dtyp e)  (default l)
      end.

    Fixpoint from_list (l : list B) (lt : list A) : t  lt :=
      match l with
        | nil =>  match lt as lt' return t  lt' with
                    | nil => snil
                    | cons e l => scons  (dtyp e) (default l )
      end
    | cons e' l' => 
      match lt as lt' return t  lt' with
        | nil => snil
        | cons e l => scons (F  e' e) (from_list l' l)
      end
    end.
  End FromList.






Section In.
  
  Inductive In : forall (x : A) (e: typ x) (l : list A) , t l -> Prop :=
  | InH : forall x (e: typ x) (l':list A) (s:t l'), In e (scons e s)
  | InR : forall x (e: typ x) (x':A) (e': typ x') (l':list A) (s:t l'), In e s -> In e (scons e' s).


  Fixpoint In_fix (x : A ) (e:typ x) (l:list A) (s: t l) : Prop :=
    match s with
      | snil => False
      | scons x' f lx le => (exists EQ :x = x', f = eq_rect _ _ e _ EQ)  \/ 
                           @In_fix x e lx le
    end.

  Lemma In_In_fix : forall x e l s, @In x e l s -> 
                                    @In_fix x e l s.
  Proof.
    intros.
    induction H.
    simpl.
    left.
    exists (eq_refl x).
    reflexivity.
    simpl.
    intuition.
  Defined.

  Lemma In_In_fix' : forall x e l s, @In_fix x e l s -> @In x e l s.
  Proof.
    intros.
    induction s.
    simpl in H. tauto.
    simpl in H.
    destruct H.
    destruct H.
    subst.
    constructor.
    constructor.
    auto.
  Qed.


  Lemma In_scons_inv (x: A) (e: typ x) lx (l: t lx) :
    @In x e lx l ->
    match l with
    | snil => False
    | scons y f lx' l' => (∃ EQ : x = y, f = eq_rect _ _ e _ EQ) ∨ @In x e lx' l'
    end.
  Proof.
    intros H.
    refine(
        match H in @In X E LX L return match L return Prop with
                                      | snil => False
                                      | scons Y F LY L' => (∃ EQ : X = Y, F = eq_rect _ _ E _ EQ) ∨ @In X E LY L'
                                      end : Prop with
        | InH x e l' s => or_introl (ex_intro _ eq_refl eq_refl)
        | InR x e x' e' l' s IN => or_intror IN
        end).
  Qed.

  Lemma In_scons_inv' (x y: A) (e: typ x) (f: typ y) lx (l: t lx) :
    In e (scons f l) ->
    (∃ EQ : x = y, f = eq_rect _ _ e _ EQ) ∨ In e l.
  Proof.
    intros H. exact (In_scons_inv H).
  Qed.

  Definition sub_seq (l1 : list A) (s1 : t l1) (l2 : list A) (s2 : t l2)  : Prop :=
    forall x (e:typ x), In  e s1 -> In  e s2.

  Lemma sub_seq_refl : forall l (s: t l), sub_seq s s.
  Proof.
    unfold sub_seq ; auto.
  Qed.
  
  Lemma sub_seq_cons : forall x (e: typ x) l (args': t l) l' (args : t l'), 
    sub_seq (scons e  args') args -> 
    sub_seq args' args.
  Proof.
    unfold sub_seq.
    intros.
    apply H. constructor. auto.
  Qed.

End In.

Section Forall.

  Variable p : forall (x:A), typ x -> bool.

  Fixpoint  for_all (lt : list A) (l : t  lt) : bool :=
    match l with
      | snil => true
      | scons x e lx lr => if p e then for_all lr else false
    end.

End Forall.



Section Length.

  Fixpoint length (l:list A) (s : t l) : nat :=
    match s with
      | snil => O
      | scons x e lx le' => S (length  le')
    end.

End Length.


Section Concat_Map_List.

  Variable B : Type.
  Variable f : forall (x:A), typ x -> list B.

  Fixpoint concat_map_list (lt : list A) (l : t lt) : list B :=
    match l with
      | snil => nil
      | scons x e lx lr => List.app (f e) (concat_map_list lr)
    end.

  Lemma in_concat_map1 : forall (lt:list A) (s : t lt) x,
    (List.In x (@concat_map_list _ s))
    ->
    (exists a, exists e : typ a, In e s /\ List.In x (f e)).
  Proof.
    induction s ; simpl.
    Case Base.
    intuition.
    Case Ind.
    intros.
    in_app. 
    intuition.
    exists x ; exists e ; intuition  constructor.
    destruct (IHs _ H0) as [x1 [x2 [HH1 HH2]]].
    exists x1. exists x2 ; intuition constructor. auto.
  Qed.


  Lemma in_concat_map2 : forall (lt:list A) (s : t lt) x,
    (exists a, exists e : typ a, In e s /\ List.In x (f e)) -> 
    (List.In x (@concat_map_list _ s)).
  Proof.
    intros.
    destruct H as [a [e [Hin HLin]]].
    induction Hin.
    Case Base.
    simpl.
    in_app. tauto.
    Case Ind.
    intros.
    simpl.
    in_app.
    tauto.
  Qed.


  Lemma in_concat_map : forall (lt:list A) (s : t lt) x,
    (List.In x (@concat_map_list _ s))
    <->
    (exists a, exists e : typ a, In e s /\ List.In x (f e)).
  Proof.
    split. apply in_concat_map1. apply in_concat_map2.
  Qed.
    
End Concat_Map_List.

Section Fold.
  Variable B : Type.
  Variable f : forall (x:A) (e : typ x), B -> B.

  Fixpoint fold_right (l:list A) (seq: t l) (acc : B) : B :=
    match seq in t l' return B with
      | snil  => acc
      | scons x' e' lx' le' => f  e' (fold_right  le' acc)
    end.
End Fold.


End Seq.
Implicit Arguments snil [A typ].
Implicit Arguments scons [A typ x lx].
Implicit Arguments In [A typ l x].
Implicit Arguments from_list [A B typ].
Implicit Arguments nth [A typ lt x].


Section MakeMorph.
  Variable A : Type.
  Variable etyp : A -> Type.

  Fixpoint make_seq' (A:Type)(l : list A) (etyp : A -> Type) {struct l}: t etyp l ->  t (fun x => x) (List.map etyp l):=
    match l with
      | nil => fun _  => snil 
      | cons e  l0 =>
        fun s =>  scons (typ := fun x => x)  (car s)  (@make_seq'  A l0 etyp (cdr s))
    end.

End MakeMorph.







Section MapT.
  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable ftyp : A -> A.
  Variable f : forall (x:A), typ x -> typ' (ftyp x).

  Fixpoint mapt (lt: list A) (l : t typ lt) : t typ' (List.map ftyp lt) :=
    match l in t _ lt' return t typ' (List.map ftyp lt') with
      | snil => snil
      | scons x e lx les => scons (f e) (mapt les)
    end.

End MapT.
Implicit Arguments mapt [A typ typ' ftyp lt].



Section Map.
  Variable A : Type.
  Variable typ : A -> Type.

  Variable typ' : A -> Type.

  Variable f : forall (x:A), typ x -> typ' x.

  Fixpoint map (lt : list A) (l : t  typ lt) : t  typ' lt :=
    match l in t _ lt' return t  typ' lt' with
      | snil => snil  
      | scons x e lx lr => scons  (f e) (map  lr)
    end.

  Lemma nth_map : forall lt (args:t typ lt) n x (v:typ x), 
    nth n  (map args) (f v)   = f (nth n args v  ).
  Proof.
    induction args.
    destruct n; reflexivity.
    destruct n ; simpl.
    reflexivity.
    auto.
  Qed.

  
(*  Lemma map_nil : forall (s: t typ nil), map  s = snil -> s = snil
  Proof.
    intros.
    refine (match s with
              | snil => _
              | scons x xe lx le => _
            end).
    reflexivity.
    intro. auto.
  Qed.
*)
End Map.
Implicit Arguments map [A typ typ' lt].

Section MapMorph.
  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable f : forall (x:A), typ x -> typ' x.
  Variable f' : forall (x:A), typ x -> typ' x.
  Variable ff' : forall (x:A) (e: typ x) , f e = f' e.

  Lemma map_morph : forall l (s : t typ l), map f s = map f' s.
  Proof.
    intros.
    induction s ; simpl.
    reflexivity.
    rewrite IHs.
    rewrite ff'. reflexivity.
  Qed.

End MapMorph.






Section MapTMap.

  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable f : forall (x:A), typ x -> typ' x.

  Variable ftyp : A -> A.
  Variable typ'' : A -> Type.
  
  Variable g : forall (x:A) , typ' x -> typ'' (ftyp x).
  
  Lemma mapt_map: forall  (l : list A) (s: t typ l),
    mapt  g  (map  f s) = mapt  (fun (x : A) (v: typ x) => g  (f  v))  s.
  Proof.
    intros.
    induction s.
    reflexivity.
    simpl. rewrite IHs.
    reflexivity.
  Qed.





End MapTMap.


Section MapMap.
  
  Variable A : Type.
  Variable typ : A -> Type.
  
  Variable typ' : A -> Type.
  
  Variable typ'' : A -> Type.
  
  Variable f : forall (x:A), typ x -> typ' x.
  
  Variable g : forall (x:A) , typ' x -> typ'' x.
  
  Lemma map_map: forall  (l : list A) (s: t typ l),
    map  g (map  f s) = map  (fun (x : A) (v: typ x) => g  (f  v)) s.
  Proof.
    intros.
    induction s.
    reflexivity.
    simpl. rewrite IHs.
    reflexivity.
  Qed.

End  MapMap.


Section MapAcc.

  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable B : Type.
  Variable f : B ->  forall (x:A),typ x -> B * typ' x.

  Fixpoint map_acc (l:list A) (s : t typ l) (acc : B) : B * (t typ' l) :=
    match s in t _ ll return B * t typ' ll with
      | snil => (acc,snil )
      | scons xt xe xlt xl => 
        let (r,sr) := map_acc  xl acc in
          let (r',xe') := f r  xe in
            (r',scons  xe' sr)
    end.

End MapAcc.

Implicit Arguments map_acc [A typ typ' B l].

Section Fold2.
  
  Variable A : Type.
  Variable B : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable f : forall (x:A),typ x -> typ' x -> B -> B.

  Fixpoint fold_right2 
           (l:list A)  : t typ l -> t typ' l -> B ->  B.
  refine (
      match l as l' return t typ l' -> t typ' l' -> B -> B with 
        | nil => fun _ _ acc => acc
        | cons x lx => 
          fun s  => 
            match s in t _ ll return ll = (cons x lx) -> t typ' (x::lx) -> B -> B  with
              | snil => fun Heq _ _ => False_rect _ _ 
              | scons xt xe xlt xl => 
                fun Heq s' acc => 
                  match s' in t _ ll' return (cons x lx = ll')  -> B with
                    | snil => fun Heq'  => False_rect _  _
                    | scons xt' xe' xlt' xl' => fun Heq' =>    @f xt xe _ (fold_right2 lx _ _ acc)     
                  end eq_refl

            end  eq_refl
    end).
  Proof.
    * discriminate.
    * discriminate.
    * intros.
      rewrite <- (@inj_car  _ _ _ _ _ Heq') in xe'.
      rewrite <- (@inj_car  _ _ _ _ _ Heq) in xe'.
      exact xe'.
    *
      rewrite (@inj_cdr  _ _ _ _ _ Heq) in xl. 
      exact xl.
    *       
      rewrite <- (@inj_cdr  _ _ _ _ _ Heq') in xl'. 
      exact xl'.
  Defined.



(*
  Fixpoint fold_right2 
           (l:list A) (s: t typ l) : t typ' l -> B ->  B.
  refine (
    match s in t _ ll return ll = l -> t typ' l -> B -> B  with
      | snil => (fun _ _ acc => acc)
      | scons xt xe xlt xl => 
        fun Heq s' acc => 
          (match s' in t _ ll' return ll' = (cons xt  xlt)  -> B with
            | snil => fun Heq' => _
            | scons xt' xe' xlt' xl' => 
              fun Heq  => @f xt xe _ (fold_right2 xlt xl _ acc)     
          end _)
    end eq_refl).
  Proof.
    * exfalso.
      eapply not_nil ; eauto.
    * rewrite (@inj_car  _ _ _ _ _ Heq0) in xe'.
      exact xe'.
    * rewrite (@inj_cdr  _ _ _ _ _ Heq0) in xl'. 
      exact xl'.
    * symmetry in Heq.
      exact Heq.
  Defined.
*)
End Fold2.


Section EquivT.
  Variable A : Type.
  Variable typ : A -> Type.
  Variable ftyp : A -> A.

  Variable p : forall (t:A), typ t -> typ  (ftyp t) -> Prop.

  Inductive equivt : forall (lt: list A), t typ lt ->  t typ (List.map ftyp lt) -> Prop :=
  | equivt_snil : 
    (*===========================*)
    equivt snil snil

  | equivt_scons : 
    forall lt  (l1 : t typ lt) (l2 : t typ (List.map ftyp lt)) x (e1: typ x) (e2:typ (ftyp x)), 
      equivt  l1 l2 -> 
      p e1 e2 -> 
      (*=========================*)
      equivt (scons e1  l1) (scons e2  l2).

End EquivT.
Implicit Arguments equivt [A typ ftyp lt].



Section Equiv.
  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  

  Variable p : forall (t:A), typ t -> typ' t -> Prop.

  Inductive equiv : forall (lt : list A), t typ lt ->  t typ' lt -> Prop :=
  | equiv_snil : 
    (*===========================*)
    equiv snil snil

  | equiv_scons : 
    forall lt (l1 : t typ lt) (l2 : t typ' lt) x (e1: typ x) (e2:typ' x), 
      equiv  l1 l2 -> 
      p e1 e2 -> 
      (*=========================*)
      equiv (scons e1  l1) (scons e2  l2).

  Fixpoint is_equiv (l : list A) (s:t typ l) : t typ' l ->   Prop.
  refine(
      match s in t _ ll return t typ' ll -> Prop  with
        | snil => fun s' => 
                    match s' with
                      | snil => True
                      | scons _ _ _ _ => False
                    end
        | scons xt xe xlt xl => 
                fun s'  => 
                  match s' in t _ ll' return (cons xt xlt = ll')  -> Prop with
                    | snil => fun Heq'  => False_rect _  _
                    | scons xt' xe' xlt' xl' => fun Heq' =>   @p xt xe (_ xe')  /\ (is_equiv xlt xl (_ xl'))     
                  end eq_refl
      end).
  * symmetry in Heq'.
    apply (@list_discr _ _ _ Heq').
  * rewrite (inj_car Heq').
    exact (fun x => x).
  * rewrite (inj_cdr Heq').
    exact (fun x => x).
  Defined.

  Lemma equiv_fix : forall lt (l1 : t typ lt) (l2 : t typ' lt),
    equiv l1 l2 -> is_equiv l1 l2.
  Proof.
    intros.
    induction H ; simpl ; tauto.
  Qed.

  Lemma equiv_fix' : forall lt (l1 : t typ lt) (l2 : t typ' lt),
    is_equiv l1 l2 -> equiv l1 l2.
  Proof.
    intros lt l1.
    induction l1.
    *
    intro.
    rewrite (nil_snil  l2).
    intros ; constructor.
    * 
      intros.
      destruct (car_cdr l2).
      destruct x0 ; simpl in e0.
      subst.
      constructor.
      simpl in H.
      apply IHl1 ; tauto.
      simpl in H.
      intuition.
  Qed.

End Equiv.
Implicit Arguments equiv [A typ typ' lt].




Section EquivMap.
  Variable A : Type.
  Variable B : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable typf : A -> Type.
  Variable typf' : A -> Type.


  Variable f : forall (x:A) (e:typ x), typf x.
  Variable f' : forall (x:A) (e:typ' x), typf' x.

  Variable r : forall (x:A), typf x -> typf' x -> Prop.

  Lemma equiv_map : forall (l : list A) (s : t typ l) (s' : t typ' l),
    equiv  (fun (bt:A) (x:typ bt)  (y:typ' bt) => r (f x) (f' y)) s s'
    -> equiv r   (map f s) (map  f' s').
  Proof.
    intros.
    induction H. constructor.
    simpl. constructor ; auto.
  Qed.





  Lemma map_equiv : forall (l : list A) (s : t typ l) (s' : t typ' l),
    equiv r   (map f s) (map  f' s') -> 
    equiv  (fun (bt:A) (x:typ bt)  (y:typ' bt) => r (f x) (f' y)) s s'.
  Proof.
    intros.
    revert H.
    intro.
    remember (map f s) as sm.
    remember (map f' s') as sm'.
    revert Heqsm Heqsm'.
    revert s s'.
    induction H.
    * 
    intros.
    rewrite (nil_snil s).
    rewrite (nil_snil s').
    constructor.
    * intros.
      destruct (car_cdr s).
      destruct x0 ; simpl in * ; subst.
      destruct (car_cdr s').
      destruct x0 ; simpl in * ; subst.
      constructor.
      apply IHequiv.
      apply cdr_inj in Heqsm ; auto.
      apply cdr_inj in Heqsm' ; auto.
      apply car_inj in Heqsm ; auto.
      apply car_inj in Heqsm' ; auto.
      subst.
      auto.
  Qed.

End EquivMap.

Section SeqMorph.
  Variable A : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.

  Lemma in_a : forall (a: A) (l:list A) , List.In a (a :: l).
  Proof.
    simpl ; tauto.
  Qed.

  Lemma in_IH :  forall (l:list A) (a:A),
    (forall x : A, List.In x (a :: l) -> typ x = typ' x) -> 
    forall x : A, List.In x l -> typ x = typ' x.
  Proof.
    intros. apply H. simpl. tauto.
  Qed.


  Fixpoint seq_morph (lt:list A)  (l:t typ lt) : (forall x , List.In x lt -> typ x = typ' x) -> t typ' lt :=
    match l in (t _ l1)  
      return ((forall x : A, List.In x l1 -> typ x = typ' x) -> t typ' l1)
      with
      | snil => fun _ : forall x : A, List.In x nil -> typ x = typ' x => snil
      | scons x e lx lle =>
        fun H0  =>
          @scons A typ' x
          (@eq_rect Type (typ x) (fun T : Type => T) e 
            (typ' x) (H0 x (in_a x lx))) lx
          (seq_morph  lle (@in_IH lx x H0))
    end.

End SeqMorph.


Section EquivEquiv.
  Variable A : Type.
  Variable B : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.
  Variable typf : A -> Type.
  Variable typf' : A -> Type.

  Variable ct : forall (x:A) (e: typ x) , Prop.
  Variable ct' : forall (x:A) (e: typ' x) , Prop.
  Variable p : forall (x :A) (e:typ x) (e': typ' x), Prop.
  Variable p' : forall (x :A) (e:typ x) (e': typ' x), Prop.

  Variable p_p' : forall (x:A) (e : typ x) (e' : typ' x), ct e -> ct' e' -> p e e' -> p' e e'.

  Lemma equiv_equiv : forall (l:list A) (l1 : t typ l) (l2 : t typ' l),
    (forall x (e:typ x), In e l1 -> ct e) -> 
    (forall x (e:typ' x), In e l2 -> ct' e) -> equiv p l1 l2 -> equiv p' l1 l2.
  Proof.
    intros.
    induction H1. 
    constructor.
    constructor.
    apply IHequiv;auto.
    intros. apply H ; constructor ; auto.
    intros. apply H0 ; constructor ; auto.
    apply p_p' ; auto.
    apply H ; constructor.
    apply H0 ; constructor.
  Qed.

End EquivEquiv.


Section InMapTEquiv.
  Variable A : Type.
  Variable B : Type.
  Variable typ : A -> Type.
  Variable ftyp : A -> A.
  Variable typ' : A -> Type.

  Variable f : forall (x:A) (e:typ x), typ' (ftyp x).
  Variable f' : forall (x:A) (e:typ x), typ' (ftyp x).

  Variable r : forall (x:A), typ' x -> typ' x -> Prop.

  Lemma in_mapt_equi : forall (l : list A) (s : t typ l),
    (forall x (e:typ x), In  e  s -> r  (f e) (f' e)) -> 
    equiv r (mapt f s) (mapt f' s).
  Proof.
    intros.
    induction s ; simpl ; constructor.
    apply IHs.
    intros. apply H.
    constructor ; auto.
    apply H. constructor.
  Qed.

End InMapTEquiv.



Section InEquiv.
  Variable A : Type.
  Variable B : Type.
  Variable typ : A -> Type.
  Variable typ' : A -> Type.

  Variable f : forall (x:A) (e:typ x), typ' x.
  Variable f' : forall (x:A) (e:typ x), typ' x.

  Variable r : forall (x:A), typ' x -> typ' x -> Prop.

  Lemma in_equi : forall (l : list A) (s : t typ l),
    (forall x (e:typ x), In  e  s -> r  (f e) (f' e)) -> 
    equiv r (map f s) (map f' s).
  Proof.
    intros.
    induction s ; simpl ; constructor.
    apply IHs.
    intros. apply H.
    constructor ; auto.
    apply H. constructor.
  Qed.

End InEquiv.


Lemma equiv_equal : forall (A:Type) (typ : A-> Type) (lt:list A) (l1 l2 : t  typ lt),
  equiv (fun x=> @eq (typ x)) l1 l2 -> l1 = l2.
Proof.
  intros.
  induction H; congruence.
Qed.

Lemma equiv_refl : forall (A:Type) (typ : A-> Type) (lt:list A) (l : t  typ lt),
  equiv (fun x => @eq (typ x)) l l.
Proof.
  intros.
  induction l; constructor; auto.
Qed.

Ltac seq := 
  match goal with
    | |- In ?E (scons ?E ?L) => constructor
    | H : In ?E snil |- _ => inv H
  end.

Ltac dest_seq_nil :=
  repeat
    match goal with 
      | H : t _ nil |- _ => 
        let HH := fresh in 
        assert (HH : H = snil) by apply (nil_snil H) ; 
          rewrite HH in * ; clear HH ; clear H
    end.
                             

Ltac equiv_tac := 
  repeat 
    match goal with
      | H : equiv ?P ?L1 ?L2 |- _  => 
        rewrite (make_seq_id L1) in * ; 
          rewrite (make_seq_id L2) in * ; 
          apply equiv_fix in H ; 
          simpl in H; simpl;
          unfold eq_rect_r in H ; unfold eq_rect_r
    end.



(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
