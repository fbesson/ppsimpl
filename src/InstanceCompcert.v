Require Import Psatz.
Require Import Coqlib Integers Axioms.
Require Import List.
Require Import FOReify.
Require Import FOLib.
Require Import FOCompil.
Require Import ZArith.
Open Scope Z_scope.



(** Type instances *)
Require Import InstanceZ.

(** Other types *)
Instance IntType : @TypeDecl.t Int.int.
Proof.
  apply TypeDecl.Mk with (equi := @eq Int.int) (type_cstr := Some (fun x => (-1)%Z < Int.unsigned x < Int.modulus)).
  * exact Int.zero.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
    abstract (destruct t;    simpl ; auto).
Defined.


Instance Int64Type : @TypeDecl.t Int64.int.
Proof.
  apply TypeDecl.Mk with (equi := @eq Int64.int) (type_cstr := Some (fun x => (-1)%Z < Int64.unsigned x < Int64.modulus)).
  * exact Int64.zero.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
    abstract (destruct t;    simpl ; auto).
Defined.

Instance ByteType : @TypeDecl.t Byte.int.
Proof.
  apply TypeDecl.Mk with (equi := @eq Byte.int) (type_cstr := Some (fun x => (-1)%Z < Byte.unsigned x < Byte.modulus)).
  * exact Byte.zero.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
    abstract (destruct t;    simpl ; auto).
Defined.


Instance injIntZ : DeclInj.t Int.int Z.
Proof.
  apply (DeclInj.Mk Int.int Z Int.unsigned).
  simpl ; auto.
  split.
  * congruence.
  *
    destruct x ; destruct y; simpl in *.
    intros.
    subst.
    assert (intrange0 = intrange).
    apply Axioms.proof_irr.
    rewrite H.
    reflexivity.
Defined.

Instance injInt64Z : DeclInj.t Int64.int Z.
Proof.
  apply (DeclInj.Mk Int64.int Z Int64.unsigned).
  simpl ; auto.
  split.
  * congruence.
  *
    destruct x ; destruct y; simpl in *.
    intros.
    subst.
    assert (intrange0 = intrange).
    apply Axioms.proof_irr.
    rewrite H.
    reflexivity.
Defined.


Instance injByteZ : DeclInj.t Byte.int Z.
Proof.
  apply (DeclInj.Mk Byte.int Z Byte.unsigned).
  simpl ; auto.
  split.
  * congruence.
  *
    destruct x ; destruct y; simpl in *.
    intros.
    subst.
    assert (intrange0 = intrange).
    apply Axioms.proof_irr.
    rewrite H.
    reflexivity.
Defined.


(** Injection of operators *)

Instance Injzero : Inj.t _ Int.zero.
Proof.
  make_inj Z0. 
Defined.

Instance Injone : Inj.t _ Int.one.
Proof.
  make_inj 1%Z. 
Defined.


Instance Injmone : Inj.t _ Int.mone.
Proof.
  make_inj (Int.modulus -1)%Z. 
Defined.

Instance Injunsigned : Inj.t _ Int.unsigned.
Proof.
  make_inj (fun x:Z => x)%Z. 
Defined.

Instance InjIntRepr : Inj.t _ Int.repr.
Proof.
  make_inj (fun x:Z => x mod Int.modulus)%Z. 
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.


Instance Inj64zero : Inj.t _ Int64.zero.
Proof.
  make_inj Z0. 
Defined.

Instance Inj64one : Inj.t _ Int64.one.
Proof.
  make_inj 1%Z. 
Defined.

Instance Inj64mone : Inj.t _ Int64.mone.
Proof.
  make_inj (Int64.modulus -1)%Z. 
Defined.

Instance Inj64unsigned : Inj.t _ Int64.unsigned.
Proof.
  make_inj (fun x:Z => x)%Z. 
Defined.

Instance InjInt64Repr : Inj.t _ Int64.repr.
Proof.
  make_inj (fun x:Z => x mod Int64.modulus)%Z. 
  rewrite Int64.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjByteRepr : Inj.t _ Byte.repr.
Proof.
  make_inj (fun x:Z => x mod Byte.modulus)%Z. 
  rewrite Byte.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjByteunsigned : Inj.t _ Byte.unsigned.
Proof.
  make_inj (fun x:Z => x)%Z. 
Defined.

Definition zsigned (x:Z) : Z := 
  if zlt x Int.half_modulus then x else x - Int.modulus.

Instance Injsigned : Inj.t _ Int.signed.
Proof.
  make_inj (zsigned).
Defined.
  
Instance InjInteq : Inj.t _ Int.eq.
Proof.
  make_inj (@eq Z).
  unfold Is_true.
  unfold Int.eq.
  destruct (zeq (Int.unsigned a0) (Int.unsigned a1)).
  tauto.
  tauto.
Defined.

Instance InjIntltu : Inj.t _ Int.ltu.
Proof.
  make_inj Zlt.
  unfold Is_true, Int.ltu.
  destruct (zlt (Int.unsigned a0) (Int.unsigned a1)).
  tauto.
  intuition.
Defined.

Definition zltsigned (x y : Z) : Prop :=
  Zlt (zsigned x) (zsigned y).

Instance InjIntlt : Inj.t _ Int.lt.
Proof.
  make_inj zltsigned.
  unfold Is_true, Int.lt, zltsigned.
  destruct (zlt (Int.signed a0) (Int.signed a1)).
  intuition.
  intuition.
Defined.


Definition ZleZ (n:Z) : Z :=
  if Zle_bool n 0 then 0 else 1.

Lemma int_zero_ext_rew:
 forall n x,
   Int.Zzero_ext n x =  (Zmod x (two_p n) * (ZleZ n)).
Proof.
 intros.
 unfold ZleZ.
 case_eq (n <=? 0).
 intros.
 rewrite Z.leb_le in H.
 unfold Int.Zzero_ext.
 rewrite Int.Ziter_base;auto.
 ring.
 intros.
 apply Int.equal_same_bits. intros.
 rewrite Z.mul_1_r.
 rewrite Int.Ztestbit_mod_two_p; auto.
 rewrite Int.Zzero_ext_spec;auto.
 apply Z.leb_nle in H.
 lia.
Qed.

Instance InjIntzero_ext : Inj.t _ Int.zero_ext.
Proof.
  make_inj (fun n x => Zmod ((Zmod x (two_p n) * (ZleZ n))) Int.modulus).
  unfold Int.zero_ext.
  rewrite Int.unsigned_repr_eq.  
  rewrite int_zero_ext_rew.
  reflexivity.
Defined.

Instance InjIntadd : Inj.t _ Int.add.
Proof.
  make_inj (fun x y => Zmod (x + y) Int.modulus).
  unfold Int.add.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntsub : Inj.t _ Int.sub.
Proof.
  make_inj (fun x y => Zmod (x - y) Int.modulus).
  unfold Int.sub.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntmul : Inj.t _ Int.mul.
Proof.
  make_inj (fun x y => Zmod (x * y) Int.modulus).
  unfold Int.mul.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntdivu : Inj.t _ Int.divu.
Proof.
  make_inj (fun x y => Zmod (x / y) Int.modulus).
  unfold Int.divu.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntmodu : Inj.t _ Int.modu.
Proof.
  make_inj (fun x y => Zmod (x mod y) Int.modulus).
  unfold Int.modu.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntdivs : Inj.t _ Int.divs.
Proof.
  make_inj (fun x y => Zmod (zsigned x ÷ zsigned y) Int.modulus).
  unfold Int.divs.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntmods : Inj.t _ Int.mods.
Proof.
  make_inj (fun x y => Zmod (Z.rem (zsigned x) (zsigned y)) Int.modulus).
  unfold Int.mods.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjIntand : Inj.t _ Int.and.
Proof.
  make_inj (fun x y => Zmod (Z.land x y) Int.modulus).
  unfold Int.and.
  rewrite Int.unsigned_repr_eq.
  reflexivity.
Defined.

Instance InjInt64and : Inj.t _ Int64.and.
Proof.
  make_inj (fun x y => Zmod (Z.land x y) Int64.modulus).
  unfold Int64.and.
  rewrite Int64.unsigned_repr_eq.
  reflexivity.
Defined.


Notation Int_half_modulus := 2147483648.
Notation Int_modulus := 4294967296.

(** simplification *)
Instance Abstzsigned : Abstract.t _ zsigned.
Proof.
  mk_abstract ((fun r x => 
                  (x <  Int_half_modulus) /\ r = x \/ (Int_half_modulus <= x) /\ r = x - Int_modulus)).
  unfold zsigned.
  change Int.half_modulus with Int_half_modulus.
  destruct (zlt a0 Int_half_modulus) ; intuition.
Defined.

Instance Abszltsigned : Abstract.t _ zltsigned.
Proof.
  mk_abstract (fun r x y => 
                 (
                   x < Int_half_modulus /\
                   y < Int_half_modulus /\ (r <-> x < y))
                   \/
                   ( x < Int_half_modulus /\
                     y >= Int_half_modulus /\
                     (r <-> x < y - Int_modulus)
                   )
                   \/
                   ( x >= Int_half_modulus /\
                     y >= Int_half_modulus /\
                     (r <-> x < y)
                   ) 
                   \/
                   ( x >= Int_half_modulus /\
                     y < Int_half_modulus /\
                     (r <-> x - Int_modulus < y)
                   )).
  Proof.
    unfold zltsigned.
    unfold zsigned.
    change Int.modulus with Int_modulus.
    change Int.half_modulus with Int_half_modulus.
    destruct (zlt a0 2147483648) ; destruct (zlt a1 2147483648); intuition try lia.
    right.  right. left.
    intuition try lia.
    apply H0. lia.
  Defined.
                           

Instance AbstZabs : Abstract.t _ Z.abs.
Proof.
  mk_abstract ((fun r x => (0 <= x)%Z /\ r = x \/ (0 > x)%Z /\ r = (- x)%Z)).
  generalize (Zabs_spec a0).
  intros.
  rewrite <- H0 ; auto.
Defined.

Instance AbstZleZ : Abstract.t _ ZleZ.
Proof.
  mk_abstract ((fun r n => (n<= 0)%Z /\ r = 0 \/ (n > 0)%Z /\ r = 1%Z)).
  unfold ZleZ.
  case_eq (a0 <=? 0).
  intros.
  apply Zle_bool_imp_le in H.
  intuition.
  intros.
  apply Z.leb_nle in H.
  intuition.
Defined.

Instance AbstZsgn : Abstract.t _ Z.sgn.
Proof.
  mk_abstract ((fun r x => 0 < x /\ r = 1 \/ 0 = x /\ r = 0 \/ x < 0 /\ r = -1))%Z.
  intros <-.
  apply Z.sgn_spec.
Defined.

Instance AbstZdiv : Abstract.t _ Zdiv.
Proof.
  mk_abstract 
     (fun r a b e =>   (not (b = 0%Z) -> 
                      a = b * r + e
                      /\
                      (0 <= e < b \/
                       b < e <= 0%Z)))%Z.
  (* specific proof *)
  intro.
  exists (Zmod a0 a1).
  rewrite <- H.
  generalize  (Z_div_mod_full a0 a1).
  unfold Zdiv, Zmod.
  destruct (Z.div_eucl a0 a1).
  unfold Remainder.
  intuition congruence.
Defined.


Instance InjZquot : Inj.t _ Z.quot.
Proof.
  make_inj (fun a b => Z.sgn a * Z.sgn b * (Z.abs a / Z.abs b))%Z.
  destruct (Z.eq_dec a1 0).
  subst. simpl. ring_simplify. destruct a0; reflexivity.
  apply Z.quot_div; auto.
Defined.

Instance AbstZrem : Abstract.t _ Z.rem.
Proof.
  mk_abstract 
    (fun r a b e => not (b = 0) -> 
                    a = b * e + r
                    /\
                    (0 < a /\ 0 <= r < Z.abs b
                    \/
                    0 = a /\ r = 0
                    \/
                    a < 0 /\ - Z.abs b < r <= 0
    ))%Z.
  clear.
  intros <-.
  exists (a0 ÷ a1)%Z.
  intros NZ. split.
  rewrite (Z.rem_eq a0 _ NZ). ring.
  assert (0 < a0 \/ 0 = a0 \/ a0 < 0)%Z as H. Psatz.lia.
  pose proof (Z.rem_bound_abs a0 _ NZ) as B.
  destruct H as [H|[<-|H]].
  pose proof (Z.rem_nonneg a0 _ NZ).
  rewrite Z.abs_eq in B; intuition.
  right. left. intuition.
  right. right.
  pose proof (Z.rem_nonpos a0 _ NZ).
  Psatz.lia.
Defined.


Instance UnfoldIntmodulus : Unfold.t _ Int.modulus.
Proof.
  mk_unfold 4294967296%Z. 
  reflexivity.
Defined.

Instance UnfoldInt64modulus : Unfold.t _ Int64.modulus.
Proof.
  mk_unfold 18446744073709551616%Z. 
  reflexivity.
Defined.

Instance UnfoldBytemodulus : Unfold.t _ Byte.modulus.
Proof.
  mk_unfold 256%Z. 
  reflexivity.
Defined.




(** Declared operators *)

(* Constants *)
(* Instance Tbool : DeclOperator.t := @DeclOperator.Mk bool (true :: false :: nil). *)
Instance Tnat : DeclOperator.t := @DeclOperator.Mk nat (O :: nil).

Instance Tpos  : DeclOperator.t := @DeclOperator.Mk _ (xH :: nil).

Instance TZ  : DeclOperator.t := @DeclOperator.Mk _ (Z0 :: Int.modulus :: Int.half_modulus :: Byte.modulus :: Int64.modulus :: nil).

Instance Tint : DeclOperator.t := @DeclOperator.Mk _ (Int.zero :: Int.one :: Int.mone ::nil).

Instance Tint64 : DeclOperator.t := @DeclOperator.Mk _ (Int64.zero :: Int64.one :: Int64.mone ::nil).


(* Unary *)
Instance TZ_int : DeclOperator.t := @DeclOperator.Mk _ (Int.repr :: nil).

Instance TZ_int64 : DeclOperator.t := @DeclOperator.Mk _ (Int64.repr :: nil).

Instance TZ_byte : DeclOperator.t := @DeclOperator.Mk _ (Byte.repr :: nil).

Instance Tint_Z : DeclOperator.t := @DeclOperator.Mk _ (Int.unsigned :: Int.signed :: nil).

Instance Tint64_Z : DeclOperator.t := @DeclOperator.Mk _ (Int64.unsigned :: nil).

Instance Tbyte_Z : DeclOperator.t := @DeclOperator.Mk _ (Byte.unsigned :: nil).

Instance Tnat_nat : DeclOperator.t := @DeclOperator.Mk _ (S :: NPeano.sqrt :: pred :: nil).

Instance Tpos_Z : DeclOperator.t := @DeclOperator.Mk _ (Zpos :: Zneg :: nil).

Instance Tpos_nat : DeclOperator.t := @DeclOperator.Mk _ (Pos.to_nat :: nil).

Instance Tpos_pos : DeclOperator.t := @DeclOperator.Mk _ (Pos.succ :: Pos.pred :: xI :: xO :: nil).

Instance Tpos_pos_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (Ple :: Plt :: Pge :: Pgt ::nil).

Instance Tbool_prop : DeclOperator.t := @DeclOperator.Mk _ (Bool.Is_true :: nil).

Instance Tprop_prop : DeclOperator.t := @DeclOperator.Mk _ (not :: nil).

Instance TZ_Z : DeclOperator.t := 
  @DeclOperator.Mk _ (Z.opp :: Z.succ :: Z.sqrt :: Z.sgn :: Z.abs :: 
                            Zpos_of_nat :: Z.sgn :: zsigned :: ZleZ :: two_p :: nil).
(* Binary *)
Instance TZ_int_int : DeclOperator.t :=
  @DeclOperator.Mk _ (Int.zero_ext  :: nil).
  
Instance Tint_int_bool : DeclOperator.t :=
  @DeclOperator.Mk _ (Int.eq :: Int.ltu :: Int.lt :: nil).

Instance Tint_int_int : DeclOperator.t :=  
  @DeclOperator.Mk _ 
                   (Int.add :: Int.sub :: Int.mul :: Int.divu :: Int.modu ::
                            Int.divs :: Int.mods :: nil).

Instance Tint64_int64_int64 : DeclOperator.t :=  
  @DeclOperator.Mk _ 
                   (Int64.and :: nil).


Instance Tnat_pos : DeclOperator.t := @DeclOperator.Mk _ (Pos.of_succ_nat :: Pos.of_nat :: nil).


Instance TZ_Z_Z : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Z.land:: Zmax :: Zplus :: Zminus :: Zmult :: Z.div :: Z.quot :: Zmod :: Z.rem :: nil ).

Instance T_Z_Z_Prop : DeclOperator.t :=
  @DeclOperator.Mk _ (Zlt :: Zle :: Zge :: Zgt :: zltsigned :: nil).

Instance Tnat_nat_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (ge :: le :: lt :: gt :: nil).

Instance Tprop_prop_prop : DeclOperator.t := @DeclOperator.Mk _ (or :: nil).

Instance Tbool_bool_bool : DeclOperator.t := @DeclOperator.Mk _ (andb :: orb :: nil).

Existing Instances ReifyType.MakeProp ReifyType.MakeBtype ReifyType.MakeArrow.




