Require Import Psatz Fcore_FLX Fcore Fcalc_ops QArith RMicromega.
Require Import FOLib.
Require Import FOReify.
Require Import ZArith.
Require Import Reals.
Require Import List.
Require Import FOCompil.
Open Scope R_scope.

(** Type instances *)

Instance comparisonType : @TypeDecl.t comparison.
Proof.
  apply TypeDecl.Mk with (equi := @eq comparison) (type_cstr := Some (fun x => x = Eq \/ x = Lt \/ x = Gt)).
  exact Eq.
  typeclasses eauto.
  intros.
  inv H.
  destruct t; tauto.
Defined.


Instance RType : @TypeDecl.t R.
Proof.
  apply TypeDecl.Mk with (equi := @eq R) (type_cstr := None).
  * exact R0.
  * typeclasses eauto.
  * intros ; auto.
    discriminate.
Defined.

Instance QType : @TypeDecl.t Q.
Proof.
  apply TypeDecl.Mk with (equi := Qeq) (type_cstr := None). (* There should be something. *)
  * exact (Qmake Z0 xH).
  * typeclasses eauto.
  * intros ; auto.
    discriminate.
Defined.

Lemma up_IZR : forall z, up (IZR (z-1)) = z.
Proof.
intros.
symmetry.
apply tech_up.
apply IZR_lt.
lia.
change 1 with (IZR 1)%Z.
rewrite <- plus_IZR.
apply IZR_le.
lia.
Qed.

Instance positiveType : @TypeDecl.t positive.
Proof.
  apply TypeDecl.Mk with (equi := @eq positive) (type_cstr := None).
  (* TODO Some (fun z => up (IZR (Zpos z) - 1) = (Zpos z))) *)
  * exact xH.
  * typeclasses eauto.
  * 
    congruence.
    (*intros ; auto.
    inv H.
    repeat rewrite INR_IZR_INZ.
    change 1 with (IZR 1).
    rewrite Z_R_minus.    
    rewrite up_IZR.
    apply positive_nat_Z. *)
Defined.

Instance ZType : @TypeDecl.t Z.
Proof.
  apply TypeDecl.Mk with (equi := @eq Z) (type_cstr := Some (fun z => up (IZR z - 1) = z)).
  * exact Z0.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
    change 1 with (IZR 1).
    rewrite Z_R_minus.
    apply up_IZR.
Defined.

Instance natType : @TypeDecl.t nat. (* Should also specify that this is an integer *)
apply TypeDecl.Mk with (equi := @eq nat) (type_cstr := Some (fun x => 0 <= x )%nat).
* exact O.
* typeclasses eauto.
*  intros. inv H.
   apply le_0_n.
Defined.


Lemma radix_lemma : forall t : radix,
   (2 <= IZR t)%R.
Proof.
    destruct t.
    simpl.
    replace 2%R with (IZR 2)%Z.
    apply IZR_le ; auto.
    apply Zle_bool_imp_le. auto.
    reflexivity.
Qed.

Instance radixType : @TypeDecl.t radix.
  apply TypeDecl.Mk with (equi := @eq radix) (type_cstr := Some (fun r => 2 <= IZR (@radix_val r))%R).
  * exists 2%Z.
    apply Zle_bool_true.
    lia.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
    apply radix_lemma.
Defined.


(*
Instance radixType : @TypeDecl.t radix.
  apply TypeDecl.Mk with (equi := @eq radix) (type_cstr := None).
  * exists 2%Z.
    apply Zle_bool_true.
    lia.
  * typeclasses eauto.
  * congruence.
Defined.
*)


(*
Instance natType : @TypeDecl.t nat.
apply TypeDecl.Mk with (equi := @eq nat) (type_cstr := Some (fun x => 0 <= x)).
* exact O.
* typeclasses eauto.
*  intros. inv H.
   apply le_0_n.
Defined.
*)

Instance boolType : @TypeDecl.t bool.
apply TypeDecl.Mk with (equi := @eq bool) (type_cstr := Some (fun x => Bool.Is_true x \/ ~ Bool.Is_true x)). 
* exact true.
* typeclasses eauto.
*  intros. inv H.
   unfold Bool.Is_true; destruct t ; tauto.
Defined.



(** Declared normalisation *)
(*Instance injNatR : DeclInj.t nat R. 
Proof.
apply (DeclInj.Mk nat R INR) ; simpl.
* split.
  congruence.
  apply INR_eq ; auto.
Defined.
*)

Instance inQ : DeclInj.t Q R.
Proof.
apply (DeclInj.Mk Q R (IQR)) ; simpl.
* split.
  intro.
  apply RMicromega.Qeq_true.
  apply Qeq_eq_bool ; auto.
  case_eq (Qeq_bool x y).
  intros.
  apply Qeq_bool_eq ; auto.
  intros.
  exfalso.
  revert H0.
  apply RMicromega.Qeq_false ; auto.
Defined.


Instance injcomparison : DeclInj.t comparison comparison. 
Proof.
apply (DeclInj.Mk comparison comparison (fun x => x)) ; simpl.
* split.
  congruence.
  auto.
Defined.

Instance injRR : DeclInj.t R R. 
Proof.
apply (DeclInj.Mk R R (fun x => x)) ; simpl.
tauto.
Defined.


Instance injZR : DeclInj.t Z R.
Proof.
  apply (DeclInj.Mk Z R IZR) ; simpl.
* split.
  congruence.
  apply eq_IZR ; auto.
Defined.

Definition IPosR (x:positive) : R :=
  IZR (Zpos x).

Instance injpositiveR : DeclInj.t positive R.
Proof.
  apply (DeclInj.Mk positive R IPosR) ; simpl.
* split.
  congruence.
  unfold IPosR.
  intro.
  apply eq_IZR in H.
  congruence.
Defined.

Instance injnatR : DeclInj.t nat R.
Proof.
  apply (DeclInj.Mk nat R INR) ; simpl.
* split.
  congruence.
  apply INR_eq ; auto.
Defined.


Instance injBoolProp : DeclInj.t bool Prop. 
Proof.
apply (DeclInj.Mk bool Prop Bool.Is_true) ; simpl.
* split.
  intuition congruence.
  destruct x ; destruct y ; simpl ; tauto.
Defined.


Instance injPropProp : DeclInj.t Prop Prop.
Proof.
  apply (DeclInj.Mk Prop Prop (fun x =>x)) ; simpl.
  tauto.
Defined.  

Require Import Eqdep_dec.

Instance injradixR : DeclInj.t radix R.
Proof.
  apply (DeclInj.Mk radix R (fun x => IZR (radix_val x))) ; simpl.
  * split.
  congruence.
  intro.
  destruct x ; destruct y ; simpl in H.
  apply eq_IZR in H.
  subst.
  assert (radix_prop = radix_prop0).
   apply UIP_dec.
   apply Bool.bool_dec.
  subst.
  reflexivity.
Defined.  

(** Declared operators *)

(* Constants *)
Instance Tcomparison : DeclOperator.t := @DeclOperator.Mk comparison (Eq :: Lt :: Gt :: nil).
(* Instance Tbool : DeclOperator.t := @DeclOperator.Mk bool (true :: false :: nil). *)

Instance Tnat : DeclOperator.t := @DeclOperator.Mk nat (O :: nil).

Instance Tpos : DeclOperator.t := @DeclOperator.Mk positive (xH :: nil).

(*
Instance Tprop : DeclOperator.t := @DeclOperator.Mk Prop (True :: False :: nil).
*)

Instance TZ  : DeclOperator.t := @DeclOperator.Mk _ (Z0 :: nil).

Instance TR  : DeclOperator.t := @DeclOperator.Mk _ (R0 :: R1 :: nil).

(* Unary *)
Instance Tnat_nat : DeclOperator.t := @DeclOperator.Mk _ (S :: nil).

Instance Tnat_R : DeclOperator.t := @DeclOperator.Mk _ (INR :: nil).

Definition Rinv0 (x : R) : R := if Req_bool x 0 then 0 else / x.

Instance TR_R : DeclOperator.t := @DeclOperator.Mk _ (Rinv0 :: Rinv :: Rabs :: Ropp :: sqrt :: nil).

Instance TZ_R   : DeclOperator.t := @DeclOperator.Mk _ (IZR :: Z2R :: nil).

Instance TQ_R : DeclOperator.t := @DeclOperator.Mk _ (IQR :: nil).

Instance Tpositive_R : DeclOperator.t := @DeclOperator.Mk _ (IPosR :: nil).

Instance TRZ    : DeclOperator.t := @DeclOperator.Mk _ (up :: Zceil :: Zfloor :: nil).
(*
Instance Tnat_Z   : DeclOperator.t := @DeclOperator.Mk _ (Z.of_nat :: nil).

Instance TZ_nat   : DeclOperator.t := @DeclOperator.Mk _ (Z.to_nat :: nil).

Instance Tbool_bool : DeclOperator.t := @DeclOperator.Mk _ (negb :: nil).
*)

Instance TQ_Q : DeclOperator.t := @DeclOperator.Mk _ (Qinv :: nil).

Instance Tbool_prop : DeclOperator.t := @DeclOperator.Mk _ (Bool.Is_true :: nil).

Instance Tprop_prop : DeclOperator.t := @DeclOperator.Mk _ (not :: nil).

Instance Tradix_Z : DeclOperator.t := @DeclOperator.Mk _ (radix_val :: nil).

Definition Rpower_compat (x y:R) : R := 
  IZR (Zpower (up (x-1)) (up (y-1))).

Definition Rpowerc (x y : R) :=
  if Req_bool y 0
  then 1
  else if Rgt_dec y 0 
       then if Req_bool x 0 then 0 else 
              if Rgt_dec x 0 then Rpower x y
              else  Rpower_compat x y
       else 0.

Definition Rdiv0 (x y : R) : R :=
  if Req_bool y 0 then 0 else Rdiv x y.



Instance TR_R_R : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Rdiv0 :: Rpower_compat :: Rpowerc :: Rpower :: Rplus :: Rminus :: Rmult :: Rmax :: Rmin :: Rdiv ::nil ).

Instance TR_R_prop : DeclOperator.t := 
  @DeclOperator.Mk _ (Rle   :: Rgt :: Rlt :: Rge :: nil ).

Instance TR_R_comparison : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Rcompare  :: nil ).

Instance TZ_positive_Q : DeclOperator.t :=
  @DeclOperator.Mk _  (Qmake :: nil).

Instance TQ_Q_Q : DeclOperator.t :=
  @DeclOperator.Mk _  (Qplus :: Qdiv :: Qmult :: nil).

Instance TQ_Q_Prop : DeclOperator.t :=
  @DeclOperator.Mk _  (Qlt :: nil).

Add Parametric Morphism : Qlt with signature Qeq ==>  Qeq ==> iff as Qlt_eq.
Proof.
  intros.
  rewrite H. rewrite H0.
  tauto.
Qed.

Add Parametric Morphism : Qplus with signature Qeq ==>  Qeq ==> Qeq as Qplus_eq.
Proof.
  intros.
  rewrite H. rewrite H0.
  reflexivity.
Qed.

Add Parametric Morphism : Qmult with signature Qeq ==>  Qeq ==> Qeq as Qmult_eq.
Proof.
  intros.
  rewrite H. rewrite H0.
  reflexivity.
Qed.

Add Parametric Morphism : Qinv with signature Qeq ==>Qeq as Qinv_eq.
Proof.
  intros.
  rewrite H. 
  reflexivity.
Qed.



Add Parametric Morphism : IQR with signature Qeq ==>  @eq R as IQR_eq.
Proof.
  intros.
  apply Qeq_true.
  apply Qeq_eq_bool ; auto.
Qed.

(*
Definition Zpos_of_nat (z:Z) : Z :=
  match z with
    | Z0 => 1%Z
    | _  => z
  end.
*)

Instance TZ_Z : DeclOperator.t := 
  @DeclOperator.Mk _ (Z.abs :: Z.opp :: nil ).

(* Binary *)
Instance Tradix_Z_R : DeclOperator.t := 
  @DeclOperator.Mk _ (bpow :: nil).

Instance Tnat_nat_nat : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (plus :: minus  :: mult :: nil).

Instance TZ_Z_Z : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Z.pow :: Zplus :: Zminus :: Zmult(* :: Zmax :: Zmin :: Zdiv :: Zmod*) :: nil ).

(*
Instance Tbool_bool_bool : DeclOperator.t := @DeclOperator.Mk _ (andb :: orb :: nil).

*)

Instance TZ_Z_prop : DeclOperator.t :=  @DeclOperator.Mk _ (Zle :: Zlt :: nil).

(*
Instance TZ_Z_bool : DeclOperator.t :=  @DeclOperator.Mk _ (Zeq_bool :: Z.leb :: Z.ltb :: Z.geb :: Z.gtb :: nil).

Instance Tnat_nat_bool : DeclOperator.t :=  
  @DeclOperator.Mk _ 
                   (beq_nat :: NPeano.leb :: NPeano.ltb :: nil).

Instance Tnat_nat_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (ge :: le :: lt :: gt :: nil).
*)
Instance Tnat_nat_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (le :: nil).


Instance Tprop_prop_prop : DeclOperator.t := @DeclOperator.Mk _ (or  :: nil).


Existing Instances ReifyType.MakeProp ReifyType.MakeBtype ReifyType.MakeArrow.

(** Injection of operators *)

Instance InjxH : Inj.t _ xH.
Proof.
  make_inj  R1. 
Defined.

Instance InjZ0 : Inj.t _ Z0.
Proof.
  make_inj  R0. 
Defined.


Instance InjMake : Inj.t _ Qmake.
Proof.
  make_inj  (fun x y:R => Rdiv x y). 
Defined.

Instance InjIPosR : Inj.t _ IPosR.
Proof.
  make_inj  (fun x :R => x). 
Defined.

Instance Injup : Inj.t _ up.
Proof.
  make_inj  (fun x:R => IZR (up x)). (* No Gain -- reintroduce IZR*)
Defined.



Instance InjZfloor : Inj.t _ Zfloor.
Proof.
  make_inj  (fun x:R => IZR (up x) - 1). 
  unfold Zfloor.
  unfold Zminus.
  rewrite plus_IZR.
  simpl. ring.
Defined.

Instance InjZceil : Inj.t _ Zceil.
Proof.
  make_inj  (fun x:R => - IZR (up (-x)) + 1 ). 
  unfold Zceil.
  unfold Zfloor.
  unfold Zminus.
  ring_simplify ((- (up (- a0) + - (1))))%Z.
  rewrite plus_IZR.
  rewrite opp_IZR.
  simpl. ring.
Defined.




Instance InjZabs : Inj.t _ Zabs.
Proof.
  make_inj  Rabs.
  rewrite Rabs_Zabs.
  reflexivity.
Defined.


Instance InjZopp : Inj.t _ Zopp.
Proof.
  make_inj  Ropp.
  rewrite opp_IZR.
  reflexivity.
Defined.


Instance InjZadd : Inj.t _ Zplus.
Proof.
  make_inj  Rplus.
  apply plus_IZR.
Defined.

Instance InjZmul : Inj.t _ Zmult.
Proof.
  make_inj  Rmult.
  apply mult_IZR.
Defined.

Instance InjZminus : Inj.t _ Zminus.
Proof.
  make_inj  Rminus.
  apply minus_IZR.
Defined.

Instance InjQminus : Inj.t _ Qminus.
Proof.
  make_inj  Rminus.
  apply RMicromega.IQR_minus.
Defined.

Instance InjQplus : Inj.t _ Qplus.
Proof.
  make_inj  Rplus.
  apply RMicromega.IQR_plus.
Defined.

Instance InjQmult : Inj.t _ Qmult.
Proof.
  make_inj  Rmult.
  apply RMicromega.IQR_mult.
Defined.


(*
Instance InjZ_to_nat : Inj.t _ Z.to_nat. 
Proof.
  make_inj  (fun x => Rmax 0 x).
  unfold Rmax. 
  destruct (Rle_dec 0 (IZR a0)).
  rewrite INR_IZR_INZ.
  destruct a0.
  reflexivity.
  apply IZR_eq.
  apply Z2Nat.id. compute; intuition congruence.
  exfalso.
  change 0 with (IZR Z0) in r.
  apply le_IZR in r.
  lia.
  destruct a0 ; simpl.
  reflexivity.
  simpl in n.
  generalize (Pos2Nat.is_pos p).
  intro.
  apply lt_INR in H.
  simpl in H.
  lra.
  reflexivity.
Defined.
*)
(*
Instance InjZof_nat : Inj.t _ Z.of_nat.
Proof.
  make_inj (fun x:R => x).
  rewrite INR_IZR_INZ.
  reflexivity.
Defined.
*)

(*
Instance InjGt : Inj.t _ gt. 
Proof.
  make_inj Rgt.
  split ; intros.
  assert (a1 < a0)%nat by auto with zarith.
  generalize (lt_INR a1 a0 H0).
  lra.
  assert (INR a1 < INR a0) by lra.
  apply INR_lt in H0.
  auto with zarith.
Defined.
*)


Instance  InjO : Inj.t _ O.
Proof.
  make_inj R0.
Defined.

Instance  InjS : Inj.t _ S.
Proof.
  make_inj (fun x => x + 1).
  destruct a0.
  simpl. lra.
  reflexivity.
Defined.

Instance  InjINR : Inj.t _ INR.
Proof.
  make_inj (fun x:R => x).
Defined.


Instance InjAdd  : Inj.t  _ plus. 
Proof.
  make_inj Rplus.
  apply plus_INR.
Defined.

Instance InjMult  : Inj.t  _ mult. 
Proof.
  make_inj Rmult.
  apply mult_INR.
Defined.

(*
Instance InjMax  : Inj.t  _ max. 
Proof.
  make_inj Rmax.
  unfold Rmax.
  generalize (Max.max_spec a0 a1).
  destruct (Rle_dec (INR a0) (INR a1)) ; intuition.
Defined.
*)

Instance InjMinus  : Inj.t  _ minus.
Proof.
  make_inj (fun x y => Rmax 0 (Rminus x y)).
  unfold Rmax.
  destruct (Rle_dec 0 (INR a0 - INR a1)).
  apply (minus_INR a0 a1).
  apply INR_le.
  lra.
  replace (a0 - a1)%nat with 0%nat.
  reflexivity.
  assert (INR a0 < INR a1) by lra.
  apply INR_lt in H.
  lia.
Defined.


Instance InjLe : Inj.t _ Zle. 
Proof.
  make_inj Rle.
  split.
  apply IZR_le ; auto.
  apply le_IZR ; auto.
Defined.

Instance InjLt : Inj.t _ Zlt. 
Proof.
  make_inj Rlt.
  split.
  apply IZR_lt ; auto.
  apply lt_IZR ; auto.
Defined.

Lemma Qlt_true : forall x y : Q, Qlt x y  <-> IQR x < IQR y.
Proof.
  intros.
  unfold Qlt.
  unfold IQR.
  simpl in *.
  assert (((Qnum x * ' Qden y < Qnum y * ' Qden x)%Z) <-> 
  (IZR (Qnum x * ' Qden y) <  IZR (Qnum y * ' Qden x))).
  *
    split.
    intros.
    apply IZR_lt ; auto.
    apply lt_IZR ; auto.
  *
    rewrite H.
    clear H.
  repeat rewrite mult_IZR.
  simpl.
  repeat INR_nat_of_P; intros.
  assert (Hr := Rlt_neq r H).
  assert (Hr0 := Rlt_neq r0 H0).
  split ; intros.
  + 
    replace (IZR (Qnum x) * / r) with  ((IZR (Qnum x) * r0) * (/r * /r0)) by
        (field ; intuition).
    replace  (IZR (Qnum y) * / r0) with ((IZR (Qnum y) * r) * (/r * /r0)) by
        (field ; intuition).
  apply Rmult_lt_compat_r ; auto.
  replace 0%R with (0* 0)%R.
  apply Rmult_le_0_lt_compat ; auto with real.
  apply Rmult_0_r.
  +
    replace (IZR (Qnum x) * r0) with  ((IZR (Qnum x) * /r) * (r0 * r)) by
        (field ; intuition).
    replace  (IZR (Qnum y) * r) with ((IZR (Qnum y) * /r0) * (r0 * r)) by
        (field ; intuition).
  apply Rmult_lt_compat_r ; auto.
  replace 0%R with (0* 0)%R.
  apply Rmult_le_0_lt_compat ; auto with real.
  apply Rmult_0_r.
Qed.


Instance InjQLt : Inj.t _ Qlt. 
Proof.
  make_inj Rlt.
  apply Qlt_true.
Defined.

   
Lemma IQR_Rdiv0 : forall x y, IQR (x / y) = Rdiv0 (IQR x) (IQR y).
Proof.
  intros.
  unfold Rdiv0.
  unfold Rdiv.
  unfold Qdiv.
  rewrite IQR_mult.
  destruct (Req_bool_spec (IQR y) 0).
  *
    apply IQR_x_0 in H.
    unfold Qeq in H.
    simpl in *.
    rewrite <- Zred_factor0 in H.
    destruct y.
    simpl in *.
    rewrite H.
    unfold Qinv.
    simpl.
    rewrite IQR_0.
    ring.
  * 
    rewrite RMicromega.IQR_inv.
    reflexivity.
    intro.
    apply H.
    clear H.
    unfold IQR.
    unfold Qeq in H0.
    simpl in H0.
    rewrite <- Zred_factor0 in H0.  
    rewrite H0.
    simpl.
    ring.
Qed.

Instance InjQdiv : Inj.t _ Qdiv.
Proof.
  make_inj Rdiv0.
  apply IQR_Rdiv0.
Defined.

Instance InjQinv : Inj.t _ Qinv.
Proof.
  make_inj Rinv0.
  unfold Rinv0.
  rewrite IQR_inv_ext.
  destruct (Req_bool_spec (IQR a0) 0).
  apply IQR_x_0  in H.
  rewrite Qeq_eq_bool ; auto.
  case_eq (Qeq_bool a0 0).
  intros.
  apply Qeq_bool_eq in H0.
  rewrite H0 in H.
  rewrite IQR_0 in H.
  congruence.
  reflexivity.
Defined.


Instance Injle     : Inj.t _ le.     
Proof.
  make_inj Rle.
  split.
  apply le_INR ; auto.
  apply INR_le ; auto.
Defined.

Instance Injbpow : Inj.t _ bpow.
Proof.
  make_inj (fun (rd:R) (exp:R) => Rpower rd exp).
  rewrite bpow_exp.
  repeat rewrite Z2R_IZR.
  reflexivity.
Defined.


Lemma Zpower_nat_0 : 
  forall n, (n > 0 -> (Zpower_nat 0 n = 0%Z))%nat.
 Proof.
   intro.
   unfold Zpower_nat.
   destruct n.
   lia.
   simpl.
   reflexivity.
 Qed.

  
Instance InjZpow : Inj.t _ Z.pow.
Proof.
  make_inj  Rpowerc.
  unfold Rpowerc.
  destruct (Req_bool_spec (IZR a1) 0).
  *
  change 0 with (IZR 0)%Z in H.
  apply eq_IZR in H.
  subst.
  reflexivity.
  *
    destruct (Rgt_dec (IZR a1) 0).
    destruct (Req_bool_spec (IZR a0) 0).
    change 0 with (IZR 0)%Z in H0.
    apply eq_IZR in H0.
    subst.
    destruct a1 ; simpl.
    simpl in r. lra.
    rewrite Zpower_pos_nat.
    rewrite Zpower_nat_0.
    reflexivity.
    lia.
    reflexivity.
      destruct (Rgt_dec (IZR a0) 0).
+
      clear H.
      destruct a1.
      simpl in r.
      lra.
      simpl.
      rewrite Zpower_pos_nat.
      change 0 with (IZR 0)%Z in H0.
      rewrite Rpower_pow.
      unfold Zpower_nat.
      induction (Pos.to_nat p).
      simpl ; reflexivity.
      simpl.
      rewrite mult_IZR.
      rewrite IHn.
      reflexivity.
      lra.
      exfalso.
      simpl in r.
      generalize (pos_INR_nat_of_P p).
      lra.
+
  unfold Rpower_compat.
  change 1 with (IZR 1)%Z.
  repeat rewrite <- minus_IZR.
  repeat rewrite up_IZR.
  reflexivity.
+
  destruct a1 ; simpl in * ; try reflexivity.
  lra.
  generalize (pos_INR_nat_of_P p).  
  lra.
Defined.

(*Instance InjZpow : Inj.t _ Z.pow.
Proof.
  make_inj  Rpower_compat.
  unfold Rpower_compat.
  change 1 with (IZR 1)%Z.
  repeat rewrite <- minus_IZR.
  repeat rewrite up_IZR.
  reflexivity.
Defined.
*)
Instance InjIZR : Inj.t _ IZR.
Proof.
  make_inj (fun (x:R) => x).
Defined.

Instance InjIQR : Inj.t _ IQR.
Proof.
  make_inj (fun (x:R) => x).
Defined.


Instance InjZ2R : Inj.t _ Z2R.
Proof.
  make_inj (fun (x:R) => x).
  apply Z2R_IZR.
Defined.


Instance InjIs_true  : Inj.t _ Bool.Is_true.   
Proof.
  make_inj (fun (x:Prop) => x).
Defined.

Instance InjNot  : Inj.t _ not.   
Proof.
  make_inj not.
Defined.

(*
Instance InjAndb  : Inj.t _ andb.   
Proof.
  make_inj and.
  generalize (Bool.andb_prop_elim a0 a1).
  generalize (Bool.andb_prop_intro a0 a1).
  tauto.
Defined.

Instance InjOrb  : Inj.t _ orb.   
Proof.
  make_inj or.
  generalize (Bool.orb_prop_elim a0 a1).
  generalize (Bool.orb_prop_intro a0 a1).
  tauto.
Defined.
*)

Instance InjOr  : Inj.t _ or.   
Proof.
  make_inj or.
Defined.

(*
Instance InjBTrue : Inj.t _ true. 
Proof.
  make_inj True.
Defined.

Instance InjBFalse : Inj.t _ false. 
Proof.
  make_inj False.
Defined.
*)

Instance InjAnd  : Inj.t _ and.   
Proof.
  make_inj and.
Defined.

Instance InjTrue  : Inj.t _ True.   
Proof.
  make_inj True.
Defined.

Instance InjFalse  : Inj.t _ False.   
Proof.
  make_inj False.
Defined.

Instance Injradix_val : Inj.t _ radix_val.   
Proof.
  make_inj (fun x:R => x).
Defined.

(** simplification *)

Instance AbstRabs : Abstract.t _ Rabs.
Proof.
  mk_abstract ((fun r x => (0 <= x)%R /\ r = x \/ (0 > x)%R /\ r = (- x)%R)).
  unfold Rabs.
  destruct (Rcase_abs a0) ; lra.
Defined.

Instance Rsqrt : Abstract.t _ sqrt.
Proof. 
  mk_abstract (fun r x =>  0 <= x -> 0 <= r /\ r * r = x).
  intros.
  subst.
  split.
  apply sqrt_pos.
  apply sqrt_def; auto.
Defined.


Instance RinvMul : Abstract.t _ Rinv.
Proof. (* Should only be done if denumerator is NOT a constant... *)
  mk_abstract (fun r x => x <> 0 -> x * r = 1).
  intros.
  subst.
  apply Rinv_r; auto.
Defined.


Instance Rinv0Mul : Abstract.t _ Rinv0.
Proof. 
  mk_abstract (fun r x => (x <> 0 -> x * r = 1)).
  unfold Rinv0.
  destruct (Req_bool_spec a0 0).
  intros.
  intuition.
  intros.
  subst.
  apply Rinv_r; auto.
Defined.


(*
Instance Rinv0Mul : Abstract.t _ Rinv0.
Proof. 
  mk_abstract (fun r x => (x <> 0 -> x * r = 1) /\ (x = 0 -> r = 0)).
  unfold Rinv0.
  destruct (Req_bool_spec a0 0).
  intros.
  intuition.
  split.
  subst.
  apply Rinv_r; auto.
  congruence.
Defined.
*)

Lemma Rdiv_eq : forall r x y,  x / y = r -> y <> 0 -> r * y = x.
Proof.
  intros.
  subst.
  unfold Rdiv.
  rewrite Rmult_assoc.
  rewrite (Rmult_comm (/ y)).
  rewrite Rinv_r; auto.
  rewrite Rmult_1_r.
  reflexivity.
Qed.

Instance RdivMul : Abstract.t _ Rdiv.
Proof.
  mk_abstract (fun r x y => y <> 0 -> r * y = x).
  apply Rdiv_eq.
Defined.


Instance AbsRdiv0 : Abstract.t _ Rdiv0.
Proof.
  mk_abstract (fun r x y =>  y <> 0 -> r * y = x).
  unfold Rdiv0.
  destruct (Req_bool_spec a1 0).
  *
    intuition.
  *
    intuition.
    apply Rdiv_eq ; auto.
Defined.

(*
Instance AbsRdiv0 : Abstract.t _ Rdiv0.
Proof.
  mk_abstract (fun r x y => ((y = 0 -> r = 0)
                               /\
                             (y <> 0 -> r * y = x))).
  unfold Rdiv0.
  destruct (Req_bool_spec a1 0).
  *
    intuition.
  *
    intuition.
    apply Rdiv_eq ; auto.
Defined.
*)

Lemma le_up : forall x y, x <= y -> (up x <= up y)%Z.
Proof.
  intros.
  apply le_IZR.
  generalize (archimed x).
  generalize (archimed y).
  intros.
  intuition.
  assert (up x = up y \/ up x + 1 <= up y \/ up y + 1 <= up x)%Z by lia.
  intuition.
  rewrite H5. lra.
  apply IZR_le in H1.
  rewrite plus_IZR in H1.
  simpl in H1.
  lra.
  apply IZR_le in H1.
  rewrite plus_IZR in H1.
  simpl in H1.
  lra.
Qed.


Instance AbstRcompare : Abstract.t _ Rcompare.
Proof.
  mk_abstract ((fun r x y  => (x = y /\ r = Eq) \/ (x < y /\ r = Lt) \/ (x > y /\ r = Gt))%R).
  destruct (Rcompare_spec a0 a1); intuition.
Defined.


Instance AbstUp : Abstract.t _ up.
Proof.
  mk_abstract (fun r x => IZR r > x /\ IZR r - x <= 1).
  intros.
  subst.
  apply archimed.
Defined.


Instance AbsRpowerc : Abstract.t _ Rpowerc.
Proof.  
  mk_abstract (fun r x y => (y = 0 /\ r = 1)
                              \/
                             ( y > 0 /\ x = 0 /\ r = 0)
                              \/
                              ( y > 0 /\ x > 0 /\ r = Rpower x y)
                              \/
                              ( y > 0 /\ x < 0 /\ r = Rpower_compat x y)
                              \/
                              ( y < 0 /\ r = 0)).
  unfold Rpowerc.
  destruct (Req_bool_spec a1 0) ; try lra.
  destruct (Rgt_dec a1 0) ; try lra.
  destruct (Rgt_dec a0 0) ; try lra.
  destruct (Req_bool_spec a0 0) ; lra.
  destruct (Req_bool_spec a0 0) ; lra.
Defined.


(*
  subst r.  
  rewrite <- Z2R_IZR.
  assert (Hle : (2 <=? up (a0 -1) = true)%Z).
  {
  rewrite <- Zle_is_le_bool.
  assert (H11 : 1 <= a0 -1) by lra.
  apply le_up in H11.
  change 1 with (IZR (1 + 1 -1)) in H11.
  rewrite up_IZR in H11.
  simpl in H11.
  auto.
  }
  replace (up (a0 - 1)) with (radix_val (Build_radix _ Hle)) by reflexivity.
  rewrite Z2R_Zpower.
  rewrite bpow_exp.
  repeat rewrite Z2R_IZR.
  simpl.
  rewrite H2.
  rewrite H3.
  reflexivity.
  assert (-1 <= a1 - 1) by lra.
  apply le_up in H.
  change (-1) with (IZR (0 - 1)) in H.
  rewrite up_IZR in H.
  apply H.




  unfold Rpower_compat.
  intros.
  subst r.  
  rewrite <- Z2R_IZR.
  assert (Hle : (2 <=? up (a0 -1) = true)%Z).
  {
  rewrite <- Zle_is_le_bool.
  assert (H11 : 1 <= a0 -1) by lra.
  apply le_up in H11.
  change 1 with (IZR (1 + 1 -1)) in H11.
  rewrite up_IZR in H11.
  simpl in H11.
  auto.
  }
  replace (up (a0 - 1)) with (radix_val (Build_radix _ Hle)) by reflexivity.
  rewrite Z2R_Zpower.
  rewrite bpow_exp.
  repeat rewrite Z2R_IZR.
  simpl.
  rewrite H2.
  rewrite H3.
  reflexivity.
  assert (-1 <= a1 - 1) by lra.
  apply le_up in H.
  change (-1) with (IZR (0 - 1)) in H.
  rewrite up_IZR in H.
  apply H.
Defined.
*)





