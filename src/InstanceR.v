Require Import Psatz.
Require Import FOLib.
Require Import FOReify.
Require Import ZArith.
Require Import Reals.
Require Import List.
Require Import FOCompil.

(** Type instances *)

Instance RType : @TypeDecl.t R.
Proof.
  apply TypeDecl.Mk with (equi := @eq R) (type_cstr := None).
  * exact R0.
  * typeclasses eauto.
  * intros ; auto.
    discriminate.
Defined.

Instance ZType : @TypeDecl.t Z.
Proof.
  apply TypeDecl.Mk with (equi := @eq Z) (type_cstr := None).
  * exact Z0.
  * typeclasses eauto.
  * intros ; auto.
    discriminate.
Defined.

(*
Instance natType : @TypeDecl.t nat.
apply TypeDecl.Mk with (equi := @eq nat) (type_cstr := Some (fun x => 0 <= x)).
* exact O.
* typeclasses eauto.
*  intros. inv H.
   apply le_0_n.
Defined.
*)

Instance boolType : @TypeDecl.t bool.
apply TypeDecl.Mk with (equi := @eq bool) (type_cstr := Some (fun x => Bool.Is_true x \/ ~ Bool.Is_true x)). 
* exact true.
* typeclasses eauto.
*  intros. inv H.
   unfold Bool.Is_true; destruct t ; tauto.
Defined.



(** Declared normalisation *)
(*Instance injNatR : DeclInj.t nat R. 
Proof.
apply (DeclInj.Mk nat R INR) ; simpl.
* split.
  congruence.
  apply INR_eq ; auto.
Defined.
*)

Instance injRR : DeclInj.t R R. 
Proof.
apply (DeclInj.Mk R R (fun x => x)) ; simpl.
tauto.
Defined.


Instance injZR : DeclInj.t Z R.
Proof.
  apply (DeclInj.Mk Z R IZR) ; simpl.
* split.
  congruence.
  apply eq_IZR ; auto.
Defined.

Instance injBoolProp : DeclInj.t bool Prop. 
Proof.
apply (DeclInj.Mk bool Prop Bool.Is_true) ; simpl.
* split.
  intuition congruence.
  destruct x ; destruct y ; simpl ; tauto.
Defined.


Instance injPropProp : DeclInj.t Prop Prop.
Proof.
  apply (DeclInj.Mk Prop Prop (fun x =>x)) ; simpl.
  tauto.
Defined.  

(** Declared operators *)

(* Constants *)
(* Instance Tbool : DeclOperator.t := @DeclOperator.Mk bool (true :: false :: nil).

Instance Tnat : DeclOperator.t := @DeclOperator.Mk nat (O :: nil).

Instance Tprop : DeclOperator.t := @DeclOperator.Mk Prop (True :: False :: nil).

Instance TZ  : DeclOperator.t := @DeclOperator.Mk _ (Z0 :: nil). *)

Instance TR  : DeclOperator.t := @DeclOperator.Mk _ (R0 :: R1 :: nil).

(* Unary *)
(*
Instance Tnat_nat : DeclOperator.t := @DeclOperator.Mk _ (S :: NPeano.sqrt :: pred :: nil).
*)

Instance TR_R : DeclOperator.t := @DeclOperator.Mk _ (Rinv :: nil).

(*
Instance Tnat_Z   : DeclOperator.t := @DeclOperator.Mk _ (Z.of_nat :: nil).

Instance TZ_nat   : DeclOperator.t := @DeclOperator.Mk _ (Z.to_nat :: nil).

Instance Tbool_bool : DeclOperator.t := @DeclOperator.Mk _ (negb :: nil).
*)

Instance Tbool_prop : DeclOperator.t := @DeclOperator.Mk _ (Bool.Is_true :: nil).

Instance Tprop_prop : DeclOperator.t := @DeclOperator.Mk _ (not :: nil).
(*
Definition Zpos_of_nat (z:Z) : Z :=
  match z with
    | Z0 => 1%Z
    | _  => z
  end.

Instance TZ_Z : DeclOperator.t := 
  @DeclOperator.Mk _ (Z.opp :: Z.succ :: Z.sqrt  :: Z.abs :: Zpos_of_nat :: nil ).

(* Binary *)
Instance Tnat_nat_nat : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (NPeano.pow :: plus :: minus :: max :: mult :: NPeano.div :: NPeano.modulo :: nil).
*)

Instance TR_R_R : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Rplus :: Rminus :: Rmult :: Rmax :: Rmin :: Rdiv  :: nil ).

(*
Instance TZ_Z_Z : DeclOperator.t := 
  @DeclOperator.Mk _ 
                   (Z.pow :: Zplus :: Zminus :: Zmult :: Zmax :: Zmin :: Zdiv :: Zmod :: nil ).

Instance Tbool_bool_bool : DeclOperator.t := @DeclOperator.Mk _ (andb :: orb :: nil).

Instance TZ_Z_prop : DeclOperator.t :=  @DeclOperator.Mk _ (Zle :: Zlt :: Zge :: Zgt :: nil).

Instance TZ_Z_bool : DeclOperator.t :=  @DeclOperator.Mk _ (Zeq_bool :: Z.leb :: Z.ltb :: Z.geb :: Z.gtb :: nil).

Instance Tnat_nat_bool : DeclOperator.t :=  
  @DeclOperator.Mk _ 
                   (beq_nat :: NPeano.leb :: NPeano.ltb :: nil).


Instance Tnat_nat_prop : DeclOperator.t :=  
  @DeclOperator.Mk _ (ge :: le :: lt :: gt :: nil).
*)

Instance Tprop_prop_prop : DeclOperator.t := @DeclOperator.Mk _ (or :: nil).


Existing Instances ReifyType.MakeProp ReifyType.MakeBtype ReifyType.MakeArrow.

Open Scope R_scope.

(** Injection of operators *)
(*
Instance InjZ_to_nat : Inj.t _ Z.to_nat. 
Proof.
  make_inj  (fun x => Rmax 0 x).
  unfold Rmax. 
  destruct (Rle_dec 0 (IZR a0)).
  rewrite INR_IZR_INZ.
  destruct a0.
  reflexivity.
  apply IZR_eq.
  apply Z2Nat.id. compute; intuition congruence.
  exfalso.
  change 0 with (IZR Z0) in r.
  apply le_IZR in r.
  lia.
  destruct a0 ; simpl.
  reflexivity.
  simpl in n.
  generalize (Pos2Nat.is_pos p).
  intro.
  apply lt_INR in H.
  simpl in H.
  lra.
  reflexivity.
Defined.
*)
(*
Instance InjZof_nat : Inj.t _ Z.of_nat.
Proof.
  make_inj (fun x:R => x).
  rewrite INR_IZR_INZ.
  reflexivity.
Defined.
*)

(*
Instance InjGt : Inj.t _ gt. 
Proof.
  make_inj Rgt.
  split ; intros.
  assert (a1 < a0)%nat by auto with zarith.
  generalize (lt_INR a1 a0 H0).
  lra.
  assert (INR a1 < INR a0) by lra.
  apply INR_lt in H0.
  auto with zarith.
Defined.
*)

(*
Instance InjAdd  : Inj.t  _ plus. 
Proof.
  make_inj Rplus.
  apply plus_INR.
Defined.

Instance InjMult  : Inj.t  _ mult. 
Proof.
  make_inj Rmult.
  apply mult_INR.
Defined.

Instance InjMax  : Inj.t  _ max. 
Proof.
  make_inj Rmax.
  unfold Rmax.
  generalize (Max.max_spec a0 a1).
  destruct (Rle_dec (INR a0) (INR a1)) ; intuition.
Defined.


Instance InjMinus  : Inj.t  _ minus.
Proof.
  make_inj (fun x y => Rmax 0 (Rminus x y)).
  unfold Rmax.
  destruct (Rle_dec 0 (INR a0 - INR a1)).
  apply (minus_INR a0 a1).
  apply INR_le.
  lra.
  replace (a0 - a1)%nat with 0%nat.
  reflexivity.
  assert (INR a0 < INR a1) by lra.
  apply INR_lt in H.
  lia.
Defined.

Instance InjLe : Inj.t _ le. 
Proof.
  make_inj Rle.
  split.
  apply le_INR ; auto.
  apply INR_le ; auto.
Defined.


Instance InjLt     : Inj.t _ lt.     
Proof.
  make_inj Rlt.
  split.
  apply lt_INR ; auto.
  apply INR_lt ; auto.
Defined.

Instance InjO      : Inj.t _ O.      
Proof.
  make_inj R0.
Defined.


Instance InjS      : Inj.t _ S.      
Proof.
  make_inj (fun x => x + R1).
  destruct a0.
  simpl.
  lra.
  reflexivity.
Defined.
*)

Instance InjIs_true  : Inj.t  Bool.Is_true.   
Proof.
  make_inj (fun (x:Prop) => x).
Defined.

Instance InjNot  : Inj.t not.   
Proof.
  make_inj not.
Defined.

(*
Instance InjAndb  : Inj.t _ andb.   
Proof.
  make_inj and.
  generalize (Bool.andb_prop_elim a0 a1).
  generalize (Bool.andb_prop_intro a0 a1).
  tauto.
Defined.

Instance InjOrb  : Inj.t _ orb.   
Proof.
  make_inj or.
  generalize (Bool.orb_prop_elim a0 a1).
  generalize (Bool.orb_prop_intro a0 a1).
  tauto.
Defined.
*)

Instance InjOr  : Inj.t or.   
Proof.
  make_inj or.
Defined.

(*
Instance InjBTrue : Inj.t _ true. 
Proof.
  make_inj True.
Defined.

Instance InjBFalse : Inj.t _ false. 
Proof.
  make_inj False.
Defined.
*)

Instance InjAnd  : Inj.t and.   
Proof.
  make_inj and.
Defined.

Instance InjTrue  : Inj.t True.   
Proof.
  make_inj True.
Defined.

Instance InjFalse  : Inj.t False.   
Proof.
  make_inj False.
Defined.


(** simplification *)

Instance AbstRabs : Abstract.t  Rabs.
Proof.
  mk_abstract ((fun r x => (0 <= x)%R /\ r = x \/ (0 > x)%R /\ r = (- x)%R)).
  unfold Rabs.
  destruct (Rcase_abs a0) ; lra.
Defined.

Instance RinvMul : Abstract.t  Rinv.
Proof.
  mk_abstract (fun r x => x <> 0 -> x * r = 1).
  intros.
  subst.
  apply Rinv_r; auto.
Defined.

Instance RdivMul : Abstract.t  Rdiv.
Proof.
  mk_abstract (fun r x y => y <> 0 -> r * y = x).
  intros.
  subst.
  unfold Rdiv.
  rewrite Rmult_assoc.
  rewrite (Rmult_comm (/ a1)).
  rewrite Rinv_r; auto.
  rewrite Rmult_1_r.
  reflexivity.
Defined.



(* Syntactic representation of types *)

Definition declTypes := (bool:Type):: (R:Type) :: nil.

Definition max_typ := 2.

Close Scope R_scope.

