Require Import FOLib.
Require Import FOReify.
Require Import ZArith.
Require Import Utf8.
Require Import BigZ.
Require Import List.
Require Import FOCompil.

(** Type instances *)

Instance ZType : TypeDecl.t Z := {| dtyp := Z0 ; equi := (@eq Z) ; type_cstr := None |}.
Proof.
  intros ; auto.
  discriminate.
Defined.

Instance natType : @TypeDecl.t nat :=
  {|
    dtyp := 0 ;
    equi := @eq nat ;
    type_cstr := (Some (fun x => 0 <= x))
  |}.
Proof. 
  intros. inv H.
  apply le_0_n.
Defined.

Instance comparisonType : @TypeDecl.t comparison :=
  {| dtyp := Eq ;
     equi := (@eq comparison) ;
     type_cstr := (Some (fun x => x = Eq \/ x = Lt \/ x = Gt))
  |}.
Proof.
  intros.
  inv H.
  destruct t; tauto.
Defined.

Instance boolType : @TypeDecl.t bool :=
  TypeDecl.Mk true (@eq bool) (Some (fun x => Bool.Is_true x \/ ~ Bool.Is_true x)).
Proof.
  intros. inv H.
  unfold Bool.Is_true; destruct t ; tauto.
Defined.

Instance positiveType : @TypeDecl.t positive :=
  TypeDecl.Mk xH (@eq positive) (Some (fun x => (Z0 < Z.pos x)%Z)).
Proof.
  intros. inv H.
  apply Pos2Z.is_pos.
Defined.

Instance NType : @TypeDecl.t N :=
  TypeDecl.Mk N0 (@eq N) (Some (fun x => BinNat.N0 <= x)%N).
Proof.
  intros.
  inv H.
  destruct t ; compute ; congruence.
Defined.

Instance BigZType : @TypeDecl.t bigZ :=
  {| dtyp := BigZ.zero ; equi := BigZ.eq ; type_cstr := None |}.
Proof.
  refine (λ p H, match H in _ = q return match q with None => True | _ => _ end with eq_refl => I end).
Defined.


(** Declared normalisation *)
Instance injcomparison : DeclInj.t comparison comparison. 
Proof.
apply (DeclInj.Mk comparison comparison (fun x => x)) ; simpl.
* split.
  congruence.
  auto.
Defined.


Instance injNatZ : DeclInj.t nat Z. 
Proof.
apply (DeclInj.Mk nat Z Z.of_nat) ; simpl.
* split.
  congruence.
  apply Nat2Z.inj ; auto.
Defined.

Instance injZZ : DeclInj.t Z Z.
Proof.
  apply (DeclInj.Mk Z Z (fun x => x)).
  simpl ; auto.
  tauto.
Defined.

Instance injNZ : DeclInj.t N Z. 
Proof.
  apply (DeclInj.Mk N Z Z.of_N) ; simpl.
  split.
  congruence.
  apply N2Z.inj ; auto.
Defined.

Instance injBoolProp : DeclInj.t bool Prop. 
Proof.
apply (DeclInj.Mk bool Prop Bool.Is_true) ; simpl.
* split.
  intuition congruence.
  destruct x ; destruct y ; simpl ; tauto.
Defined.

Instance injPosZ : DeclInj.t positive Z. 
Proof.
apply (DeclInj.Mk _ _ Z.pos) ; simpl.
* intuition congruence.
Defined.

Instance injbigZZ : DeclInj.t bigZ Z :=
  {| inj := BigZ.to_Z |}.
Proof.
  refine (λ x y, conj (λ H, H) (λ H, H)).
Defined.

Instance injPropProp : DeclInj.t Prop Prop.
Proof.
  apply (DeclInj.Mk Prop Prop (fun x =>x)) ; simpl.
  tauto.
Defined.  

Definition Zpos_of_nat (z:Z) : Z :=
  match z with
    | Z0 => 1%Z
    | _  => z
  end.


Existing Instances ReifyType.MakeProp ReifyType.MakeBtype ReifyType.MakeArrow.

(** Injection of operators *)

Instance InjZpow_pos : Inj.t Z.pow_pos.
Proof.
  make_inj Z.pow.
Defined.

Instance InjPosof_succ_nat : Inj.t Pos.of_succ_nat.
Proof.
  make_inj Zsucc.
  apply Zpos_P_of_succ_nat.
Defined.


Instance InjPosof_nat : Inj.t Pos.of_nat.
Proof.
  make_inj Zpos_of_nat.
  unfold Zpos_of_nat.
  unfold Z.of_nat.
  destruct a0 ; simpl.
  * reflexivity.
  * destruct a0.
    - reflexivity.
    - apply f_equal.
      rewrite Pos.of_nat_succ.
      rewrite <- Nat2Pos.inj_succ.
      reflexivity.
      congruence.
Defined.


Instance InjZ_to_nat : Inj.t Z.to_nat. 
Proof.
  make_inj  (fun x => Zmax 0 x).
  (* CompCert proof *)
  unfold Zmax. destruct a0; simpl; auto. 
  change (Z.of_nat (Z.to_nat (Zpos p)) = Zpos p).
  apply Z2Nat.id. compute; intuition congruence. 
Defined.


Instance Injpos_to_nat : Inj.t Pos.to_nat. 
Proof.
  make_inj  (fun (x:Z) =>  x).
  apply positive_nat_Z.
Defined.


Instance InjZ_to_pos : Inj.t Z.to_pos.
Proof.
  make_inj (Z.max 1).
  match goal with x : Z |- _ => destruct x as [ | p | p ]; first [ reflexivity | rewrite Z2Pos.id, Z.max_r ]; Psatz.lia end.
Defined.


Instance InjZof_nat : Inj.t  Z.of_nat.
Proof.
  make_inj (fun x:Z => x).
Defined.


Instance InjPsucc : Inj.t  Pos.succ. 
Proof.
  make_inj  Zsucc.
  rewrite Pos2Z.inj_succ.
  reflexivity.
Defined.


Instance InjPplus : Inj.t Pplus.
Proof.
  make_inj Zplus.
Defined.

Instance InjPosAddCarry : Inj.t  Pos.add_carry.
Proof.
  make_inj (fun x y => Z.succ (x + y)).
  rewrite Pos.add_carry_spec, Pos2Z.inj_succ.
  reflexivity.
Defined.

Instance InjPminus : Inj.t Pminus. 
Proof.
  make_inj (fun x y => Zmax 1 (x - y)).
  apply Pos2Z.inj_sub_max.
Defined.

Instance InjPmult : Inj.t Pmult.
Proof.
  make_inj Zmult.
Defined.

Instance InjPmax : Inj.t Pmax. 
Proof.
  make_inj Zmax.
  apply Pos2Z.inj_max.
Defined.

Instance InjPmin : Inj.t Pos.min. 
Proof.
  make_inj Z.min.
  apply Pos2Z.inj_min.
Defined.

Instance InjPpred : Inj.t Pos.pred. 
Proof.
  make_inj (fun x => Zmax 1 (x - 1))%Z.
  rewrite Ppred_minus.
  apply Pos2Z.inj_sub_max.  
Defined.

Instance InjxO : Inj.t xO.
Proof.
  make_inj (Zmult 2%Z).
  destruct (Hlist.car args);
  destruct (Hlist.car args') ; simpl ; intuition congruence.
Defined.

Instance InjxI : Inj.t xI.
Proof.
  make_inj (fun x => Zplus (Zmult 2%Z x) 1%Z).
  destruct (Hlist.car args);
  destruct (Hlist.car args') ; simpl ; intuition congruence.
Defined.

Instance InjGt : Inj.t gt. 
Proof.
  make_inj (fun x y => Zlt y x).
  split ; intros.
  auto with zarith.
  auto with zarith.
Defined.

Instance InjPgt : Inj.t Pos.gt. 
Proof.
  make_inj Zgt.
Defined.

Instance InjNateq_nat : Inj.t beq_nat.
Proof.
  make_inj (@eq Z).
  unfold Bool.Is_true.
  case_eq (beq_nat a0 a1).
  rewrite beq_nat_true_iff.
  intuition.
  rewrite beq_nat_false_iff.
  intuition.
Defined.

Instance InjNatleb : Inj.t NPeano.leb.
Proof.
  make_inj Z.le.
  unfold Bool.Is_true.
  rewrite <- Nat2Z.inj_le.
  generalize (NPeano.leb_le a0 a1).
  destruct (NPeano.leb a0 a1) ; intuition.
Defined.

Instance InjNatltb : Inj.t NPeano.ltb.
Proof.
  make_inj Z.lt.
  unfold Bool.Is_true.
  rewrite <- Nat2Z.inj_lt.
  generalize (NPeano.ltb_lt a0 a1).
  destruct (NPeano.ltb a0 a1) ; intuition.
Defined.

Instance InjZeq_bool : Inj.t Zeq_bool.
Proof.
  make_inj  (@eq Z).
  generalize (Zeq_bool_if a0 a1).
  unfold Bool.Is_true.
  destruct (Zeq_bool a0 a1).
  intuition.
  intuition.
Defined.

Lemma bool_is_true_iff (b: bool) : Bool.Is_true b <-> b = true.
Proof. intuition; destruct b; auto. Qed.

Instance InjZeqb : Inj.t Z.eqb.
Proof.
  make_inj (@eq Z).
  rewrite <- Z.eqb_eq.
  apply bool_is_true_iff.
Defined.

Instance InjZleb : Inj.t Z.leb.
Proof.
  make_inj Zle.
  generalize (Zle_cases a0 a1).
  unfold Bool.Is_true.
  destruct ((a0 <=? a1)%Z).
  tauto.
  intuition.
Defined.

Instance InjZltb : Inj.t Z.ltb. 
Proof.
  make_inj Zlt.
  generalize (Zlt_cases a0 a1).
  unfold Bool.Is_true.
  destruct ((a0 <? a1)%Z).
  tauto.
  intuition.
Defined.

Instance InjZgeb : Inj.t Z.geb. 
Proof.
  make_inj Zge.
  generalize (Zge_cases a0 a1).
  unfold Bool.Is_true.
  destruct ((a0 >=? a1)%Z).
  tauto.
  intuition.
Defined.

Instance InjZgtb : Inj.t Z.gtb.
Proof.
  make_inj Zgt.
  generalize (Zgt_cases a0 a1).
  unfold Bool.Is_true.
  destruct ((a0 >? a1)%Z).
  tauto.
  intuition.
Defined.


Instance InjPle : Inj.t Pos.le.
Proof.
  make_inj Zle.
Defined.

Instance InjPge : Inj.t Pos.ge.
Proof.
  make_inj Zge.
Defined.

Instance InjPlt : Inj.t Pos.lt. 
Proof.
  make_inj Zlt.
Defined.

Instance InjAdd  : Inj.t plus. 
Proof.
  make_inj Zplus.
  apply Nat2Z.inj_add.
Defined.

Instance InjMult  : Inj.t mult. 
Proof.
  make_inj Zmult.
  apply Nat2Z.inj_mul.
Defined.

Instance InjMax  : Inj.t max. 
Proof.
  make_inj Zmax.
  apply Nat2Z.inj_max.
Defined.

Instance InjDiv : Inj.t NPeano.div.
Proof.
  make_inj Zdiv.
  destruct a1.
  *  (* division by 0 *)
  simpl.
  unfold Zdiv. case_eq (Z.div_eucl (Z.of_nat a0) 0).
  intros.
  destruct a0 ; simpl in H ; try congruence.
  * 
    apply  div_Zdiv.
    congruence.
Defined.

Lemma npow_inj : forall x y, 
   Z.of_nat (NPeano.pow x y) = (Z.of_nat x ^ Z.of_nat y)%Z.
Proof.
  induction y.
  * reflexivity.
  *
    rewrite NPeano.pow_succ_r.
    rewrite Nat2Z.inj_mul.
    rewrite IHy.
    rewrite Nat2Z.inj_succ.
    rewrite Z.pow_succ_r.
    reflexivity.
    apply Zle_0_nat.
    auto with zarith.
Qed.

Instance InjNPow : Inj.t NPeano.pow.
Proof.
  make_inj Z.pow.
  apply npow_inj.
Defined.


Instance InjModulo : Inj.t NPeano.modulo.
Proof.
  make_inj Zmod.
  destruct a1.
  *  (* division by 0 *)
  simpl.
  unfold Zmod. case_eq (Z.div_eucl (Z.of_nat a0) 0).
  intros.
  destruct a0 ; simpl in H ; congruence.
  * 
    apply mod_Zmod.
    congruence.
Defined.

Instance InjMinus  : Inj.t minus.
Proof.
  make_inj (fun x y => Zmax Z0 (Zminus x y)).
  apply Nat2Z.inj_sub_max.
Defined.

Instance InjXH : Inj.t xH. 
Proof.
  make_inj (Zpos xH).
Defined.

Instance InjLe : Inj.t le. 
Proof.
  make_inj Zle.
  split.
  apply inj_le ; auto.
  apply inj_le_rev ; auto.
Defined.


Instance InjGe : Inj.t ge. 
Proof.
  make_inj Zge.
  split.
  apply inj_ge ; auto.
  apply inj_ge_rev ; auto.
Defined.


Instance InjLt     : Inj.t lt.     
Proof.
  make_inj Zlt.
  split.
  apply inj_lt ; auto.
  apply inj_lt_rev ; auto.
Defined.

Instance InjO      : Inj.t O.      
Proof.
  make_inj Z0.
Defined.

Instance InjS      : Inj.t S.      
Proof.
  make_inj Zsucc.
  apply Zpos_P_of_succ_nat.
Defined.

Instance Injpred      : Inj.t pred.      
Proof.
  make_inj (fun x => Zmax 0 (x - 1)).
  rewrite Nat2Z.inj_pred_max.
  reflexivity.
Defined.

Lemma nat2Z_sqrt_inj : forall x,
   Z.of_nat (NPeano.sqrt x) = Z.sqrt (Z.of_nat x).
Proof.
  intro.
  generalize (NPeano.sqrt_spec x).
  generalize (Z.sqrt_spec (Z.of_nat x)).
  simpl.
  zify.
  Psatz.nia.
Qed.

Instance InjSqrt      : Inj.t NPeano.sqrt.      
Proof.
  make_inj Z.sqrt.
  apply nat2Z_sqrt_inj.
Defined.

Instance InjN0     : Inj.t N0.     
Proof.
  make_inj Z0.
Defined.


Instance InjNle    : Inj.t N.le.   
Proof.
  make_inj Zle.
  apply N2Z.inj_le ; auto.
Defined.

Instance InjNegb  : Inj.t negb.   
Proof.
  make_inj not.
  split. apply Bool.negb_prop_elim. apply Bool.negb_prop_intro.
Defined.

Instance InjIs_true  : Inj.t Bool.Is_true.   
Proof.
  make_inj (fun (x:Prop) => x).
Defined.

Instance InjNot  : Inj.t not.   
Proof.
  make_inj not.
Defined.

Instance InjAndb  : Inj.t andb.   
Proof.
  make_inj and.
  generalize (Bool.andb_prop_elim a0 a1).
  generalize (Bool.andb_prop_intro a0 a1).
  tauto.
Defined.

Instance InjOrb  : Inj.t orb.   
Proof.
  make_inj or.
  generalize (Bool.orb_prop_elim a0 a1).
  generalize (Bool.orb_prop_intro a0 a1).
  tauto.
Defined.

Instance InjOr  : Inj.t or.   
Proof.
  make_inj or.
Defined.

Instance InjBTrue : Inj.t true. 
Proof.
  make_inj True.
Defined.

Instance InjBFalse : Inj.t false. 
Proof.
  make_inj False.
Defined.

Instance InjAnd  : Inj.t and.   
Proof.
  make_inj and.
Defined.

Instance InjTrue  : Inj.t True.   
Proof.
  make_inj True.
Defined.

Instance InjFalse  : Inj.t False.   
Proof.
  make_inj False.
Defined.

Instance InjZpos : Inj.t Z.pos. 
Proof.
  make_inj (fun x:Z => x).
Defined.

Instance InjZneg : Inj.t Z.neg. 
Proof.
  make_inj (fun x:Z => - x)%Z.
Defined.

Instance InjNpos : Inj.t N.pos. 
Proof.
  make_inj (fun x:Z => x).
Defined.

Instance InjNsucc : Inj.t N.succ. 
Proof.
  make_inj Z.succ.
  apply N2Z.inj_succ.
Defined.

Instance InjNpow : Inj.t N.pow.
Proof.
  make_inj Z.pow.
  apply N2Z.inj_pow.
Defined.

Instance InjNadd : Inj.t N.add.
Proof.
  make_inj Z.add.
  apply N2Z.inj_add.
Defined.

Instance InjNsub : Inj.t N.sub.
Proof.
  make_inj (fun x y => Z.max 0 (Z.sub x y)).
  apply N2Z.inj_sub_max.
Defined.

Instance InjNmul : Inj.t N.mul.
Proof.
  make_inj Z.mul.
  apply N2Z.inj_mul.
Defined.

Instance InjbigZ_zero : Inj.t BigZ.zero.
Proof. make_inj Z0. Defined.

Instance InjbigZ_to_Z : Inj.t BigZ.to_Z.
Proof. make_inj (fun x:Z => x). Defined.
Instance InjbigZ_of_Z : Inj.t BigZ.of_Z.
Proof. make_inj (fun x:Z => x). apply BigZ.spec_of_Z. Defined.

Instance InjbigZ_eq : Inj.t BigZ.eq.
Proof. make_inj (@eq Z). Defined.

Instance InjbigZ_opp : Inj.t BigZ.opp.
Proof. make_inj (fun (z: Z) => 0 - z)%Z. apply BigZ.spec_opp. Defined.

Instance InjbigZ_add : Inj.t BigZ.add.
Proof. make_inj Z.add. apply BigZ.spec_add. Defined.

Instance InjbigZ_sub : Inj.t BigZ.sub.
Proof. make_inj Z.sub. apply BigZ.spec_sub. Defined.

Instance InjbigZ_mul : Inj.t BigZ.mul.
Proof. make_inj Z.mul. apply BigZ.spec_mul. Defined.

Instance InjbigZ_div : Inj.t BigZ.div.
Proof. make_inj Z.div. apply BigZ.spec_div. Defined.

Instance InjbigZeqb : Inj.t BigZ.eqb.
Proof.
  make_inj (@eq Z).
  unfold BigZ.eqb.
  rewrite BigZ.spec_compare, <- Z.compare_eq_iff.
  destruct (Z.compare _ _); simpl; intuition congruence.
Defined.

Instance InjbigZleb : Inj.t BigZ.leb.
Proof.
  make_inj Zle.
  unfold BigZ.leb.
  rewrite BigZ.spec_compare, <- Z.compare_le_iff.
  destruct (Z.compare _ _); simpl; intuition congruence.
Defined.

Instance InjbigZltb : Inj.t BigZ.ltb.
Proof.
  make_inj Zlt.
  unfold BigZ.ltb.
  rewrite BigZ.spec_compare, <- Z.compare_lt_iff.
  destruct (Z.compare _ _); simpl; intuition congruence.
Defined.

Instance InjbigZlt : Inj.t BigZ.lt.
Proof.
  make_inj Zlt.
Defined.

Instance InjbigZmin : Inj.t BigZ.min.
Proof. make_inj Zmin. apply BigZ.spec_min. Defined.
Instance InjbigZmax : Inj.t BigZ.max.
Proof. make_inj Zmax. apply BigZ.spec_max. Defined.

Instance InjNdiv : Inj.t N.div.
Proof.
  make_inj Z.div.
  apply N2Z.inj_div.
Defined.


(** simplification *)

Instance AbstZcompare : Abstract.t Zcompare.
Proof.
  mk_abstract ((fun r x y  => (x = y /\ r = Eq) \/ (x < y /\ r = Lt) \/ (x > y /\ r = Gt))%Z).
  destruct (Z.compare_spec a0 a1); intuition.
Defined.

Instance AbstZabs : Abstract.t Z.abs.
Proof.
  mk_abstract ((fun r x => (0 <= x)%Z /\ r = x \/ (0 > x)%Z /\ r = (- x)%Z)).
  generalize (Zabs_spec a0).
  intros.
  rewrite <- H0 ; auto.
Defined.

Instance AbstZsgn : Abstract.t Z.sgn.
Proof.
  mk_abstract ((fun r x => 0 < x /\ r = 1 \/ 0 = x /\ r = 0 \/ x < 0 /\ r = -1))%Z.
  intros <-.
  apply Z.sgn_spec.
Defined.

Instance AbstZdiv : Abstract.t  Zdiv.
Proof.
  mk_abstract 
     (fun r a b e =>   (not (b = 0%Z) -> 
                      a = b * r + e
                      /\
                      (0 <= e < b \/
                       b < e <= 0%Z)))%Z.
  (* specific proof *)
  intro.
  exists (Zmod a0 a1).
  rewrite <- H.
  generalize  (Z_div_mod_full a0 a1).
  unfold Zdiv, Zmod.
  destruct (Z.div_eucl a0 a1).
  unfold Remainder.
  intuition congruence.
Defined.

(** This is incomplete -- x mod 0 = 0 ? *)
Lemma mod_0 : forall x, (Zmod x 0 = 0)%Z.
Proof.
  intros.
  destruct x ; reflexivity.
Qed.

Lemma AbsZmod_lemma : 
  forall a0 a1 r,
    (a0 mod a1)%Z = r ->
   exists x : Z,
     a1 = 0%Z /\ r = 0%Z \/
                 a1 <> 0%Z /\ a0 = (a1 * x + r)%Z /\ ((0 <= r < a1)%Z \/ (a1 < r <= 0)%Z).
Proof.
  intros.
  exists (Zdiv a0 a1).
  rewrite <- H.
  destruct (Z_zerop a1).
  left.
  rewrite e.
  rewrite mod_0.
  intuition.
  generalize  (Z_div_mod_full a0 a1).
  unfold Zdiv, Zmod.
  destruct (Z.div_eucl a0 a1).
  unfold Remainder.
  intuition.
Qed.

Instance AbstZmod : Abstract.t  Zmod.
Proof.
  mk_abstract 
    (fun r a b e =>   ( 
                        (b = 0 /\ r = 0)%Z
                        \/
                        (
                          (not (b = 0%Z) /\ a = b * e + r
                           /\
                           (0 <= r < b \/
                            b < r <= 0%Z)))))%Z.
  apply AbsZmod_lemma.
Defined.

(* weaker version --
Instance AbstZmod : Abstract.t _ Zmod.
Proof.
  mk_abstract 
     (fun r a b e =>   (not (b = 0%Z) -> 
                      a = b * e + r
                      /\
                      (0 <= r < b \/
                       b < r <= 0%Z)))%Z.
  (* specific proof *)
  intro.
  exists (Zdiv a0 a1).
  rewrite <- H.
  generalize  (Z_div_mod_full a0 a1).
  unfold Zdiv, Zmod.
  destruct (Z.div_eucl a0 a1).
  unfold Remainder.
  intuition congruence.
Defined.
*)

Instance InjZquot : Inj.t Z.quot.
Proof.
  make_inj (fun a b => Z.sgn a * Z.sgn b * (Z.abs a / Z.abs b))%Z.
  destruct (Z.eq_dec a1 0).
  subst. simpl. ring_simplify. destruct a0; reflexivity.
  apply Z.quot_div; auto.
Defined.

Instance AbstZrem : Abstract.t Z.rem.
Proof.
  mk_abstract 
    (fun r a b e => not (b = 0) -> 
                    a = b * e + r
                    /\
                    (0 < a /\ 0 <= r /\ ( b > 0 /\ r < b \/ b < 0 /\ r < -b)
                    \/
                    0 = a /\ r = 0
                    \/
                    a < 0 /\ (b > 0 /\ - b < r \/ b < 0 /\ b < r) /\ r <= 0
    ))%Z.
  clear.
  intros <-.
  exists (a0 ÷ a1)%Z.
  intros NZ. split.
  rewrite (Z.rem_eq a0 _ NZ). ring.
  assert (0 < a0 \/ 0 = a0 \/ a0 < 0)%Z as H. Psatz.lia.
  pose proof (Z.rem_bound_abs a0 _ NZ) as B.
  destruct H as [H|[<-|H]].
  pose proof (Z.rem_nonneg a0 _ NZ).
  left.
  rewrite Z.abs_eq in B; intuition.
  Psatz.lia.
  right. left. intuition.
  right. right.
  pose proof (Z.rem_nonpos a0 _ NZ).
  Psatz.lia.
Defined.

Instance AbstZMax : Abstract.t Zmax.
Proof.
  mk_abstract (fun r x y => not (x < y) /\ r = x \/ x < y /\ r = y)%Z.
  (generalize (Zmax_spec a0 a1);
   intuition congruence).
Defined.

Instance AbstZMin : Abstract.t Zmin.
Proof.
  mk_abstract (fun r x y => (x <= y) /\ r = x \/ x > y /\ r = y)%Z.
  (generalize (Zmin_spec a0 a1);
   intuition congruence).
Defined.



Instance AbstZsqrt : Abstract.t Z.sqrt.
Proof.
  mk_abstract (fun r n => 0 <= n -> r * r <= n < (r + 1) * (r + 1))%Z.
  rewrite Z.add_1_r.
  intro.
  rewrite <- H.
  apply (Z.sqrt_spec a0).
Defined.

Instance AbstZpos_of_nat : Abstract.t Zpos_of_nat.
Proof.
  mk_abstract (fun r n => (n = 0%Z -> r = 1%Z) /\ (n <> 0%Z -> r = n)).
  destruct a0 ; simpl ; intuition congruence.
Defined.

Instance UnfoldZsucc : Unfold.t Zsucc.
Proof.
  mk_unfold (fun x => x + 1)%Z.
  apply Z.add_1_r.
Defined.

Instance UnfoldZopp : Unfold.t Zopp.
Proof.
  mk_unfold (fun x => 0 - x)%Z.
  reflexivity.
Defined.




