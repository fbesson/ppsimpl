(** A restricted intuition -- should scale better *)
Require Import Psatz.


(** [dand] breaks /\ in the hypotheses. *)
Ltac dand :=
  repeat 
  match goal with
    |H : _ /\ _ |- _ => destruct H
  end.

(** [only_conj] only keeps atomic propositions *)
Ltac only_conj :=
  repeat 
  match goal with
    | H : context[?A\/ ?B] |- _ => clear H
    | H : context[?A ->  ?B] |- _ => clear H
  end.

(** [no_dup] remove duplicate hypotheses *)
Ltac no_dup :=
repeat 
  match goal with 
    | H : ?A , H' : ?B |- _ => 
      match constr:(A=B) with
        | ?A = ?A => clear H
      end
  end.

(** [elim_impl] given (A -> B) tries to prove B by proving A using lia  *)
Ltac elim_impl :=
  match goal with 
    | H : ?A -> ?B |- _ => 
      let f := fresh in 
      assert (f : A) by (only_conj ; lia) ;
        apply H in f ; clear H
    | H : ?A -> ?B |- _ => revert H
  end.

Ltac mintuition :=
  repeat dand ; repeat (elim_impl ; dand).