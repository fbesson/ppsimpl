Require Import ZArith.
Require Psatz.
Require Import FOLib.
Require Import List.
Require  Seq.
Set Implicit Arguments.
Require Import FOInstance.
Require Import Ppform.


Definition subst  (A:Type)
  (env : positive -> A) (x:positive) (v:A) : positive -> A :=
  fun vr => 
    if EqTest.eq_dec vr x then v else env vr.

(** We note f[[x -> v]] the function identical to f everywhere except for x for which it returns v *)
(* To use this notation, define an instance *)
Notation "E  [ X ↦ V ]" := (subst E X V) (at level 100).

Section Vector.

  Variable  A : Type.

  Inductive vector  : nat -> Type :=
  | VNil : vector  0
  | VCons : forall  (e:A) (n:nat) (v : vector  n) , vector  (S n)
  .

  Fixpoint In (x:A) (n:nat) (v : vector n) : Prop :=
    match v with
      | VNil => False
      | VCons e n v => x = e \/ (In x v)
    end.

End Vector.


Section Map.
  Variable A B : Type.
  Variable f : A -> B.
  
  Fixpoint map  (n: nat) (v : vector A n) : vector B n :=
    match v  with
      | VNil => VNil B
      | VCons e n v => VCons (f e) (map   v)
    end.
End Map.

Section Fold.
  Variable A B : Type.
  Variable f : A -> B -> B.

  Fixpoint fold_r (n : nat) (v: vector A n) (acc : B) : B :=
    match v with
      | VNil => acc
      | VCons e n v => f e (fold_r v acc)
    end.

End Fold.


Section S.
  
  Variable btyp : Type.
  Variable btyp_args : btyp -> nat.
  Variable btyp_eval : forall (o:btyp) (l : vector  Type (btyp_args o)) , Type.

  (** Polymorphic types *)

  Inductive ptyp_elem : Type :=
  | VTyp (x : positive) (* Type variable *)
  | VConstr (o:btyp) (l : vector ptyp_elem (btyp_args o)) (* Type constructor *)
  .

  Inductive ptyp : Type :=
  | Forall : forall (x : positive), ptyp -> ptyp
  | Arrow  : ptyp_elem -> ptyp -> ptyp
  | Ret    : ptyp
  .


  Section InductionP.
    Require Import Max.

    Fixpoint pdepth (ty : ptyp_elem) : nat :=
      match ty with
        | VTyp _ => 0
        | VConstr o l => S (fold_r (fun ty n => max n (pdepth ty)) l O)
      end.


    Require Import Wf_nat.

    Lemma pdepth_lt : forall (n:nat) (args : vector ptyp_elem n) (x:ptyp_elem),
      In x  args -> 
      pdepth x <
      S
      (fold_r (fun ty n => max n (pdepth ty)) args 0).
    Proof.
      intros until 0.
      induction args.
      *
      simpl. tauto.
      *
        simpl ; intros.
        destruct H ; subst.
        -
          ppsimpl. Psatz.lia.
        - 
          apply IHargs in H.
          ppsimpl.
          Psatz.lia.
    Qed.

    Variable P : ptyp_elem -> Type.

    Variable P_VTyp : forall x, P (VTyp x).

    Variable P_Op : forall (o : btyp)  (args : vector _ _),
      (forall (x: ptyp_elem), In x  args -> P x) ->  P (@VConstr o args).
    

    Definition lt_wf_rec :
      forall (n : nat) (P : nat -> Type),
       (forall n0 : nat, (forall m : nat, m < n0 -> P m) -> P n0) -> P n.
    Proof.
      intros.
      eapply well_founded_induction_type.
      apply lt_wf. auto.
    Defined.

(*    Fixpoint ptyp_elem_dind  (ty:ptyp_elem) : P ty.
    Proof.
      Check map.
      refine (
          match ty as ty' return P ty' with
            | VTyp x => P_VTyp x
            | VConstr o l => let l' := 
          end).
      induction l ; simpl.
      tauto.
      intros.
      destruct H.
      
      apply ptyp_elem_dind.
    Defined.
*)

    Lemma ptyp_elem_dind  (ty:ptyp_elem) : P ty.
    Proof.
      intros.
      add_eq (pdepth ty) n.
      revert ty. 
      induction n using lt_wf_rec.
      destruct n ; [ Case Zero | Case Succ].
      * 
        destruct ty ; simpl ; auto.
        discriminate.
      * 
        destruct ty.
        - 
          simpl.
          discriminate.
        - 
          simpl.
          intro.
          inv H.
          apply P_Op.
          intros.
          eapply X.
          apply pdepth_lt; auto. apply H.
          reflexivity.
    Defined.


  End InductionP.




  Fixpoint eptyp_elem (env : positive -> Type) (ty:ptyp_elem) : Type :=
    match ty with
      | VTyp x => env x
      | VConstr o l => @btyp_eval o (map (eptyp_elem env) l)
    end.

  Fixpoint eptyp (env : positive -> Type) (ty : ptyp) (r : ptyp_elem) : Type :=
    match ty with
      | Forall x ty' => forall (t : Type), eptyp (env [x ↦ t]) ty' r
      | Arrow ty pty => eptyp_elem env ty -> eptyp env pty r
      | Ret      => eptyp_elem env r
    end.

  (* Simple types *)

  Inductive styp_elem : Type :=
  | TConstr (o:btyp) (l : vector styp_elem (btyp_args o)) (* Type constructor *)
  | TType
  .

  Section InductionS.
    Require Import Max.

    Fixpoint depth (ty : styp_elem) : nat :=
      match ty with
          TType => 0
        | TConstr o l => S (fold_r (fun ty n => max n (depth ty)) l O)
      end.


    Require Import Wf_nat.

    Lemma depth_lt : forall (n:nat) (args : vector styp_elem n) (x:styp_elem),
      In x  args -> 
      depth x <
      S
      (fold_r (fun ty n => max n (depth ty)) args 0).
    Proof.
      intros until 0.
      induction args.
      *
      simpl. tauto.
      *
        simpl ; intros.
        destruct H ; subst.
        -
          generalize ((fold_r (fun (ty : styp_elem) (n0 : nat) => max n0 (depth ty)) args 0)).
          intros.
          ppsimpl. Psatz.lia.
        - 
          apply IHargs in H.
          ppsimpl.
          Psatz.lia.
    Qed.

    Variable P : styp_elem -> Prop.

    Variable P_TType : P TType.

    Variable P_Op : forall (o : btyp)  (args : vector _ _),
      (forall (x: styp_elem), In x  args -> P x) ->  P (@TConstr o args).
    
    Lemma styp_elem_dind : forall (ty:styp_elem) , P ty.
    Proof.
      intros.
      add_eq (depth ty) n.
      revert ty. 
      induction n using Wf_nat.lt_wf_ind.
      destruct n ; [ Case Zero | Case Succ].
      * 
        destruct ty ; simpl.
        +
        discriminate.
        + auto.
      * 
        destruct ty.
        - 
          simpl.
          intro.
          inv H0.
          apply P_Op.
          intros.
          eapply H.
          apply depth_lt; auto. apply H0.
          reflexivity.
       - simpl ; discriminate.
    Qed.


  End InductionS.

  
  Fixpoint estyp_elem  (ty:styp_elem) : Type :=
    match ty with
      | TConstr o l => @btyp_eval o (map estyp_elem  l)
      | TType  => Type
    end.

  Definition styp := (list styp_elem * styp_elem)%type.

  Section S.
    Variable  A : Type.
    CoInductive Stream : Type :=
      SCons (e:A) (l: Stream).

    Variable e : A.

    CoFixpoint all := SCons e all.

  End S.

  
  Fixpoint inst_ptyp_elem (env : positive -> styp_elem) (ty : ptyp_elem) : styp_elem :=
    match ty with
      | VTyp x =>  (env x)
      | VConstr o l => @TConstr o (map (inst_ptyp_elem env) l)
    end.

  Definition update_env (l : Stream styp_elem) (env : positive -> styp_elem) (x:positive) : Stream styp_elem * (positive -> styp_elem) :=
    let (e,r) :=  l in (l , (env[x ↦ e])).

  Fixpoint xinst_ptyp (l :  Stream styp_elem) (env : positive -> styp_elem) (ty : ptyp) (r : ptyp_elem) : list styp_elem  * styp_elem :=
    match ty with
      | Forall x ty => let (l',env') := update_env l env x in
                       let (ty,r)    := xinst_ptyp l' env' ty r in
                       (TType :: ty , r)
      | Arrow sty ty => 
        let (ty,r) := xinst_ptyp l env ty r in 
        ((inst_ptyp_elem env sty) :: ty , r)
      | Ret      => (nil , inst_ptyp_elem env r)
    end.

  Definition inst_ptyp (l :  Stream styp_elem)  (ty : ptyp) (r : ptyp_elem) : list styp_elem  * styp_elem :=
    xinst_ptyp l (fun _ => TType) ty r.
  

  Variable var : Type.
  Variable op : Type.
  Variable op_args : op -> ptyp.
  Variable op_ret  : op -> ptyp_elem.
  Variable op_eval : forall (o:op), eptyp (fun _ => Type) (op_args o) (op_ret o).


  Inductive Expr : styp_elem -> Type :=
  | Cst   : forall (bt:styp_elem) (v:estyp_elem bt) , Expr bt
  | Var   : forall (bt:styp_elem) (v:var), Expr  bt
  | Op    : forall 
              (o:op) 
              (subst : Stream styp_elem),
              let targs_tres := inst_ptyp subst (op_args o) (op_ret o) 
    in 
      forall (args: Seq.t  Expr (fst targs_tres)),
        Expr (snd targs_tres).


  Definition Env := var -> forall (bt:styp_elem) , estyp_elem bt.

  Lemma inst_correct : forall ty r targs tres l env env'
                              (Henv : forall x, estyp_elem (env x) = env' x),
                         
                         (targs,tres) = xinst_ptyp l env ty r -> 
                         eptyp env' ty r -> FOLType.eval_typ estyp_elem targs tres.
  Proof.
    induction ty.
    * (* Forall *)
      simpl.
      intros.
      revert H.
      case_eq (update_env l env x).
      intros until 0.
      case_eq (xinst_ptyp s s0 ty r).
      intros.
      inv H1.
      symmetry in H.
      simpl.
      intro.
      unfold update_env in H0.
      destruct l.
      inv H0.
      apply IHty with (2:= H) (env' := env' [x ↦ (estyp_elem e)]).      
      intro.
      unfold subst.
      destruct (EqTest.eq_dec x0 x).
      reflexivity.
      apply Henv.
      apply X.
    * (* Arrow *)
      simpl.
      intros.
      revert H.
      case_eq (xinst_ptyp l env ty r).
      intros.
      inv H0.
      simpl.
      intro.
      eapply IHty ; eauto.
      apply X.
      revert X0.
      revert Henv.
      clear.
      assert ((forall x : positive, estyp_elem (env x) = env' x) ->
      estyp_elem (inst_ptyp_elem env p) = eptyp_elem env' p).
      induction p using ptyp_elem_dind.
      +
        simpl ; intros.
        apply H.
      +
        simpl ; intros.
        apply f_equal.
        induction args.
        -
        simpl.
        reflexivity.
        -
          simpl.
          rewrite IHargs.
          rewrite H.
          reflexivity.
          simpl.
          tauto.
          auto.
          simpl.
          intros ; apply H.
          simpl ; tauto.
          auto.
     + intros.
       rewrite H in X0.
       auto.
       auto.
    * (* Ret *)
      simpl.
      intros.
      inv H.
      simpl.
      assert ((forall x : positive, estyp_elem (env x) = env' x) ->
      estyp_elem (inst_ptyp_elem env r) = eptyp_elem env' r).
      clear.
      induction r using ptyp_elem_dind.
      +
        simpl ; intros.
        apply H.
      +
        simpl ; intros.
        apply f_equal.

        induction args.
        -
        simpl.
        reflexivity.
        -
          simpl.
          rewrite IHargs.
          rewrite H.
          reflexivity.
          simpl.
          tauto.
          auto.
          simpl.
          intros ; apply H.
          simpl ; tauto.
          auto.
     + intros.
       rewrite H ; auto.
  Defined.



  Check surjective_pairing.

  Lemma surjective_pairing : forall (A B : Type) (p : A * B), p = (fst p, snd p).
  Proof.
    intros.
    destruct p.
    reflexivity.
  Defined.


  Fixpoint interp  (r : styp_elem) (env:Env) (e : Expr r) : estyp_elem r.
  refine(
  match e in (Expr r') return (estyp_elem r') with
  | Cst _ v => v
  | Var bt v => env v bt
  | Op o  s args =>
      FOLType.interp_seq (Seq.map (fun bt => @interp bt env) args) 
        (
                          _ (op_eval  o))
  end).
  apply inst_correct with (l:= s) (env := fun _ => TType).
  reflexivity.
  symmetry.
  apply surjective_pairing.
  Defined.
  

  End S.


Inductive mbtyp := bt_nat | bt_Z | bt_list.

Definition mbtyp_args (bt : mbtyp) : nat :=
  match bt with
    | bt_nat => 0
    | bt_Z   => 0
    | bt_list => 1
  end.

Definition mbtyp_eval (o:mbtyp) : forall(l : vector  Type (mbtyp_args o)) , Type.
  refine(
  match o as o' return forall(l : vector  Type (mbtyp_args o')) , Type with
    | bt_nat => fun _  => nat
    | bt_Z   => fun _  => Z
    | bt_list => 
      fun l => 
        match l in vector _ n' return 1 = n' -> Type with
          | VNil => fun _ => False_rect _ _
          | VCons e _ l => fun _ => list e
        end eq_refl
  end).
  discriminate.
Defined.

Definition nat_typ := VConstr (btyp_args := mbtyp_args) bt_nat  (VNil _).
Definition mlist (ty : ptyp_elem mbtyp_args) := VConstr (btyp_args := mbtyp_args) bt_list (VCons  ty (VNil _)).


Inductive op := MZ | MNil | MCons | TNat|TZ.

Definition op_args (o:op) : ptyp mbtyp_args :=
  match o with
    | TNat => Ret _
    | TZ   => Ret _
    | MZ => Ret _
    | MNil => Forall xH (Ret _)
    | MCons => Forall xH (Arrow (VTyp mbtyp_args xH)  (Arrow (mlist (VTyp _ xH)) (Ret mbtyp_args)))
  end.

Definition op_ret (o:op) : ptyp_elem mbtyp_args :=
  match o with
    | TNat => VTyp _ xH
    | TZ   => VTyp _ xH
    | MZ => nat_typ
    | MNil => (mlist (VTyp _ xH))
    | MCons => (mlist (VTyp _ xH))
  end.

Definition op_eval  (o:op): eptyp mbtyp_eval (fun _ => Type) (op_args o) (op_ret o).
  refine(
  match o as o' return eptyp mbtyp_eval (fun _ => Type) (op_args o') (op_ret o') with
    | TNat => nat
    | TZ   => Z
    | MZ => O
    | MNil => fun x => @nil x
    | MCons => fun x y z => @cons x y z
  end).
Defined.

(* cons O nil *)
Definition snat := TConstr (btyp_args := mbtyp_args) bt_nat (VNil _ ).

Definition list_nat := TConstr (btyp_args := mbtyp_args) bt_list (VCons snat (VNil _ )).

Definition msnil := 
(@Seq.snil (@styp_elem mbtyp mbtyp_args)
     (@Expr mbtyp mbtyp_args mbtyp_eval positive op op_args op_ret)).

Definition zero : Expr mbtyp_eval positive  op_args op_ret snat :=
  Op MZ (all snat)  msnil.

Definition tnat : Expr mbtyp_eval positive  op_args op_ret (TType _) .
  refine (Op TNat (all snat) msnil).
Defined.

Definition nil_nat : Expr mbtyp_eval positive  op_args op_ret list_nat.
  refine(Op MNil (SCons snat (all snat)) (Seq.scons tnat msnil)). 
Defined.
  

Definition cons : Expr mbtyp_eval positive  op_args op_ret list_nat :=
  Op MCons (SCons snat (all snat)) (Seq.scons tnat (Seq.scons zero (Seq.scons nil_nat msnil))).

Goal forall env, interp (btyp_args := mbtyp_args) (btyp_eval := mbtyp_eval) op_eval  env cons = List.cons 0 List.nil.
intros.
Time reflexivity.
Time Qed.

Print pair.

(* Local Variables: *)
(* coding: utf-8 *)
(* End: *)
