Require Import ZArith.
Require Import List.
Require Import FOexpr.
Require Import FOLType.
Require Import FOSimpl.
Require Import FOLib.
Require Import FOCompil.

Ltac no_dep_prop :=
  repeat match goal with
           | H : ?T   |- _ => 
             match type of T with 
               | Prop => 
                 match goal with
                   | H' : ?T' |- _ => 
                     match type of T' with
                       | Prop => 
                         match T with
                           | context[H'] => clear H
                         end
                     end
                 end
             end
         end.

(* This is a bad idea - should only reify terms that do not end up in variables *)
Ltac has_type_prop T := 
  match type of T with
    | Prop => idtac
    | _    => fail
  end.

Ltac collect_prop :=
  repeat match goal with
    | H : ?A -> ?B |- _ => has_type_prop A ; has_type_prop B ; revert H
    | H : forall (x : ?T) , ?E |- _ =>  
      let F := type of H in
      has_type_prop H ;clear dependent H
    | H : ?T |- _ => has_type_prop T ; revert H
  end.


Ltac make_impl L :=
  match L with
    | cons ?E  nil => E
    | cons ?E  ?L =>   
      let LE := make_impl L in
      constr:((FOexpr.Op
                 (inl (FOSimpl.TrmWithProp.Prop_Impl _)) 
                 (Hlist.scons E (Hlist.scons LE Hlist.snil))))
  end.

Ltac blocked T :=
  match T with
    | @FOLib.block _ _ => true
    |   _       => false
  end.

Ltac not_blocked T :=
  let b := blocked T in
  match b with
    | false => idtac
    | true => fail
  end.


Ltac convert_goal INJ _xx_ _map_ _cvars_ _env_ := 
  let btyp_u := btyp_u_of_inj INJ in
  let op   := op_of_btyp_u btyp_u in
  let etyp_u := etyp_u_of_inj INJ in
  let etyp   := etyp_of_etyp_u etyp_u in
  let dtyp   := dtyp_of_inj INJ in 
  let vnil := constr:(@nil ({x : option btyp_u & etyp x})) in

  let rec select_prop VARS E :=
      match goal with
        | H : ?A -> ?B |- _ => has_type_prop A ; has_type_prop B ; 
                               let P := constr:(A -> B) in 
                               add_prop VARS E H P
        | H : forall (x : ?T) , ?E |- _ =>  
          let F := type of H in
          has_type_prop H ;clear dependent H (* Should not clear things *)
        | H : ?T |- _ => 
          has_type_prop T ; not_blocked T ;  add_prop VARS E H T
        |   _         => 
              let E := make_impl E in
              let cvars := (eval  compute in (@collect_vars _ _ VARS xH nil)) in 
              let map := (eval unfold makeMap in (@makeMap _ etyp VARS)) in
              let smap := (eval simpl in map) in
              set (_xx_ := E) ; 
                set (_map_ := smap) ; 
                set (_cvars_ := cvars);
                set (_env_ := @mkEnv _ etyp dtyp  (@option_beq_test _ _)  _map_) ;
                try change (@interp _ positive _  _ _env_ _xx_) ; revert _cvars_ 
      end

      with

      add_prop VARS E H P :=
        let e := reify_expr btyp_u etyp op VARS P  vnil in
        match e with
          | (?NVARS , ?NE) => 
            (
              match NE with
              | @FOexpr.Var _ _ _ _ => change P with (@block Prop P) in  H ; select_prop VARS E
              |  _          => 
                 revert H ; 
                   select_prop NVARS (cons NE  E) 
            end)
        end in

  match goal with
    | |- ?G => let e := reify_expr btyp_u etyp op vnil G  vnil in
               match e with
                 | (?V, ?E) => 
                   select_prop V (cons E  nil)
               end
  end.

(*Ltac convert_goal INJ := 
  let btyp_u := btyp_u_of_inj INJ in
  let op   := op_of_btyp_u btyp_u in
  let etyp_u := etyp_u_of_inj INJ in
  let etyp   := etyp_of_etyp_u etyp_u in
  let dtyp   := dtyp_of_inj INJ in 
  let vnil := constr:(@nil ({x : option btyp_u & etyp x})) in

  match goal with
    | |- ?G => let e := reify_expr btyp_u etyp op vnil G  vnil in
               match e with 
                 | (?VARS ,  ?E) => 
                   let cvars := (eval  compute in (@collect_vars _ _ VARS xH nil)) in 
                   let map := (eval unfold makeMap in (@makeMap _ etyp VARS)) in
                   let smap := (eval simpl in map) in
                   set (_xx_ := E) ; 
                     set (_map_ := smap) ; 
                     set (_cvars_ := cvars);
                     set (_env_ := @mkEnv _ etyp dtyp  (@option_beq_test _ _)  _map_) ;
                 try change (@interp _ positive _  _ _env_ _xx_) ; revert _cvars_ 
               end
  end.
*)

Ltac ppform_do_it INJ := 

  match goal with
    | |- let _  := ?VARS in interp ?ENV ?GOAL => 
        intro;
        apply Make.inj_trmt_sharing_correct with (1:= INJ) (l:= VARS);
        intro _env'_ ; 
        match goal with
          |  |-  interp _ ?F1 -> interp _ ?F2 => 
              set (_CF1_ := F1) ; 
              set (_CF2_ := F2) ; 
              vm_compute in _CF1_; vm_compute in _CF2_; 
              unfold _CF1_ ; unfold  _CF2_ ; 
              clear _CF1_ _CF2_;
              simpl ; unfold FOLib.block
        end
  end.

Ltac ppform_with INJ :=
  let _xx_ := fresh "_xx_" in 
  let _map_ := fresh "_map_" in 
  let _cvars_ := fresh "_cvars_" in
  let _env_  := fresh "_env" in
  convert_goal INJ _xx_ _map_ _cvars_ _env_ ; 
  ppform_do_it INJ ; clear _xx_ _map_ _cvars_ _env_.

Ltac ppform :=
  match constr: (@FOSimpl.Make.inj_vars _ _ _ _ _ _ _ _) with
      | @FOSimpl.Make.inj_vars _ _ _ _ _ _ _ ?INJ => 
        ppform_with INJ
  end.


Ltac ppsimpl_do_it  INJ SIMPL  :=
  match goal with
    | |- let _ := ?VARS in FOexpr.interp ?ENV ?GOAL => 
      intro ; 
        refine (@FOSimpl.Make.simplify_sharing_lem_with_env _ _ _ _ _ _ _  INJ SIMPL _ VARS _ _) 
  end ;
  intro _env'_ ;
  match goal with
    | |- FOexpr.interp _ ?V => set (_CF1_ := V) ;
                              vm_compute in _CF1_ ;
                              unfold _CF1_ ; clear _CF1_
  end .

Ltac msimpl  INJ :=
  match   type of INJ with
    | @FOSimpl.Make.spec _ ?BTYP _ _ _ _ ?EVAL => 
      let eq_typ := (eval unfold FOLType.Btyp.equi_typ, BTYP in (@FOLType.Btyp.equi_typ _ BTYP)) in 
      match eq_typ with
          ?EQTYP => 
          match goal with
            | H := FOCompil.mkEnv ?A ?B ?C ?D ?E |- _ => unfold H
          end;
            simpl;  
            unfold FOCompil.mkEnv; simpl;
            unfold eq_rect_r,eq_rect;
            simpl;
            unfold FOLib.block

      end
  end.

Ltac gen_env :=
  try
    (
      match goal with
        | H : FOSimpl.Make.Env _ _ _ |- _ => 
          (repeat match goal with
                    | |- context[H ?X ?Y] => generalize (H X Y) ; intro
                  end) ; clear H ; 
        (repeat 
           match goal with
             | H : FOexpr.TrmSyntax.etyp _ |- _ => simpl in H
           end);
          clear H ; unfold FOLib.block in *
      end) .


Ltac ppsimpl_with INJ SIMPL := 
  let _xx_ := fresh "_xx_" in 
  let _map_ := fresh "_map_" in 
  let _cvars_ := fresh "_cvars_" in
  let _env_  := fresh "_env" in
  convert_goal INJ _xx_ _map_ _cvars_ _env_ ; 
  ppsimpl_do_it INJ SIMPL ; 
  msimpl INJ;  
  clear _xx_ _map_ _cvars_ _env_ ; gen_env.

Ltac ppsimpl :=
  match constr: (@FOSimpl.Make.inj_vars _ _ _ _ _ _ _ _) with
      | @FOSimpl.Make.inj_vars _ _ _ _ _ _ _ ?INJ => 
        match constr:(@FOSimpl.Make.op_morph_u _ _ _ _ _ _) with
          | @FOSimpl.Make.op_morph_u _ _ _ _ _ ?SIMPL => 
            ppsimpl_with INJ SIMPL
        end
  end.

