Require Import BuildPpsimpl.
Require Export Ppform.

Ltac ppsimplz :=
  let inj := make_inj in
  let simpl := simplifier in 
  ppsimpl_with make_inj simplifier.
