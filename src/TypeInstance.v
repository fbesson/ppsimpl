Require Import Psatz.
Require Import List.
Require Import FOLib.
Require Import FOReify.
Require Import FOCompil.
Require Import ZArith.
Open Scope Z_scope.

(** Type instances *)

Instance ZType : @TypeDecl.t Z.
Proof.
  apply TypeDecl.Mk with (equi := @eq Z) (type_cstr := None).
  * exact Z0.
  * typeclasses eauto.
  * intros ; auto.
    inv H.
Defined.

Instance positiveType : @TypeDecl.t positive.
apply TypeDecl.Mk with (equi := @eq positive) (type_cstr := Some (fun x => (Z0 < Z.pos x)%Z)).
* exact xH.
* typeclasses eauto.
*  intros. inv H.
   apply Pos2Z.is_pos.
Defined.

Instance boolType : @TypeDecl.t bool.
apply TypeDecl.Mk with (equi := @eq bool) (type_cstr := Some (fun x => Bool.Is_true x \/ ~ Bool.Is_true x)). 
* exact true.
* typeclasses eauto.
*  intros. inv H.
   unfold Bool.Is_true; destruct t ; tauto.
Defined.

Instance natType : @TypeDecl.t nat.
apply TypeDecl.Mk with (equi := @eq nat) (type_cstr := Some (fun x => 0 <= x)%nat).
* exact O.
* typeclasses eauto.
*  intros. inv H.
   apply le_0_n.
Defined.

Instance comparisonType : @TypeDecl.t comparison.
Proof.
  apply TypeDecl.Mk with (equi := @eq comparison) (type_cstr := Some (fun x => x = Eq \/ x = Lt \/ x = Gt)).
  exact Eq.
  typeclasses eauto.
  intros.
  inv H.
  destruct t; tauto.
Defined.

Instance NType : @TypeDecl.t N.
Proof.
  apply TypeDecl.Mk with (equi := @eq N) (type_cstr := Some (fun x => BinNat.N0 <= x)%N).
  exact N0.
  typeclasses eauto.
  intros.
  inv H.
  destruct t ; compute ; congruence.
Defined.
