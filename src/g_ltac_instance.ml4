open Util
open Sign
open Term
open Termops
open Namegen
open Environ
open Pfedit
open Tacmach
open Refiner
open Tacticals
open Decl_kinds
open Evarutil
open Tactics
(*
let start_proof id str hyps c ?init_tac ?compute_guard hook = 
  let goals = [ (Global.env_of_context hyps , c) ] in
  Proof_global.start_proof id str goals ?compute_guard hook;
  let tac = match init_tac with
   | Some tac -> Proofview.V82.tactic tac
   | None -> Proofview.tclUNIT ()
  in
  try Proof_global.run_tactic tac
  with reraise -> Proof_global.discard_current (); raise reraise
*)

let make_transparent const  = 
  {const with Entries.const_entry_opaque = false}

(*
let build_constant_by_tactic id sign typ tac =
  start_proof id (Global,Proof Theorem) sign typ (fun _ _ -> ());
  try
    by tac;
    let _,(const,_,_,_) = cook_proof (fun _ -> ()) in
    delete_current_proof ();
    const
  with reraise ->
    delete_current_proof ();
    raise reraise
*)

let interpretable_as_section_decl d1 d2 = match d1,d2 with
  | (_,Some _,_), (_,None,_) -> false
  | (_,Some b1,t1), (_,Some b2,t2) -> eq_constr b1 b2 && eq_constr t1 t2
  | (_,None,t1), (_,_,t2) -> eq_constr t1 t2


let concrete_subproof id tac gl =
  let current_sign = Global.named_context()
  and global_sign = pf_hyps gl in
  let sign,secsign =
    List.fold_right
      (fun (id,_,_ as d) (s1,s2) ->
	if mem_named_context id current_sign &&
          interpretable_as_section_decl (Sign.lookup_named id current_sign) d
        then (s1,push_named_context_val d s2)
	else (add_named_decl d s1,s2))
      global_sign (empty_named_context,empty_named_context_val) in
  let id = next_global_ident_away id (pf_ids_of_hyps gl) in
  let concl = it_mkNamedProd_or_LetIn (pf_concl gl) sign in
  let concl =
    try flush_and_check_evars (project gl) concl
    with Uninstantiated_evar _ ->
      error "\"abstract\" cannot handle existentials." in
  let const = build_constant_by_tactic id secsign concl
    (tclCOMPLETE (tclTHEN (tclDO (List.length sign) intro) tac)) in
  let cd = Entries.DefinitionEntry (make_transparent const) in
  let lem = mkConst (Declare.declare_constant ~internal:Declare.KernelSilent id (cd,IsDefinition Definition)) in
  exact_no_check
    (applist (lem,List.rev (Array.to_list (instance_from_named_context sign))))
    gl


TACTIC EXTEND CONCRETE
| [ "concrete"  tactic(tac)  "as" ident(s) ] -> 
  [ concrete_subproof s (Tacinterp.eval_tactic tac)  ]
END

TACTIC EXTEND EXISTING_INSTANCE
| [ "Existing" "Instance" ident(r)] -> 
  [
    begin
      Classes.existing_instance true (Libnames.Ident (Util.dummy_loc,r)) ; 
      Tacticals.tclIDTAC
    end
    ]
END
